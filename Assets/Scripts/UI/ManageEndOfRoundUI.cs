using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageEndOfRoundUI : MonoBehaviour
{
    [SerializeField] GameObject profitParent;
    [SerializeField] GameObject lossesParent;

    [SerializeField] GameObject profitPrefab;
    [SerializeField] GameObject lossesPrefab;

    [SerializeField] GameObject youWonText;
    [SerializeField] GameObject youLostText;

    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.Confined;
        PopulateListLosses();
        if (CarListOpenWorld.Instance.HasWon)
        {
            youWonText.SetActive(true);
            PopulateListProfits();
        }
        else
        {
            youLostText.SetActive(true);
        }

    }

    void PopulateListLosses()
    {
        for (int k = 0; k < CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli.Count; k++)
        {
            if(CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli[k].HealthPerc == 0)
            {
                GameObject item = Instantiate(lossesPrefab, lossesParent.transform);
                PartySelectionButton partyBt = item.GetComponent<PartySelectionButton>();
                VehicleData veicolo = CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli[k];

                partyBt.Icon.sprite = CarListOpenWorld.Instance.GetMaterialIndex(veicolo.VehicleType).Icona;
                partyBt.WeaponIcon.sprite = CarListOpenWorld.Instance.GetMaterialIndex(veicolo.WeaponType).Icona;
                partyBt.Nome.text = veicolo.CarName;
                partyBt.HealthPerc = veicolo.HealthPerc;
            }
        }
    }

    void PopulateListProfits()
    {
        for (int k = 0; k < CarListOpenWorld.Instance.OpenWorldInteractableObjcts.Count; k++)
        {
            if (CarListOpenWorld.Instance.OpenWorldInteractableObjcts[k].InteractableID == CarListOpenWorld.Instance.IntercaableObjectID)
            {
                for (int i = 0; i < CarListOpenWorld.Instance.OpenWorldInteractableObjcts[k].Ricompense.Count; i++)
                {
                    GameObject item = Instantiate(profitPrefab, profitParent.transform);
                    ProfitUIItem partyBt = item.GetComponent<ProfitUIItem>();

                    CraftingMaterial mat = CarListOpenWorld.Instance.GetMaterialIndex(CarListOpenWorld.Instance.OpenWorldInteractableObjcts[k].Ricompense[i].Index);

                    partyBt.ProfitIcon.sprite = mat.Icona;
                    partyBt.ProfitName.text = mat.Nome;
                    partyBt.ProfitQuantity.text = CarListOpenWorld.Instance.OpenWorldInteractableObjcts[k].Ricompense[i].Quantity.ToString();
                }
                break;
            }
        }
    }
}

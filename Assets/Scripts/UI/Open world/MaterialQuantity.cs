using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MaterialQuantity
{
    [SerializeField] int quantity, index;
    [SerializeField] string vehicleDataId;
    [SerializeField] bool isVehicle;

    public MaterialQuantity(int quantity, int index, string vehicleDataId, bool isVehicle)
    {
        this.quantity = quantity;
        this.index = index;
        this.isVehicle = isVehicle;
        this.vehicleDataId = vehicleDataId;
    }

    public int Quantity { get => quantity; set => quantity = value; }
    public int Index { get => index; set => index = value; }
    public bool IsVehicle { get => isVehicle; set => isVehicle = value; }
    public string VehicleDataId { get => vehicleDataId; set => vehicleDataId = value; }
}

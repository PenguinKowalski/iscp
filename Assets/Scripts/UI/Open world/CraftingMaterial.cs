using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CraftingMaterial
{
    [SerializeField] protected string nome, descrizione;
    [SerializeField] protected Sprite icona;
    [SerializeField] protected int tier, price;
    [SerializeField] public enum Category { arma, veicoli, armatura, motore, materiale, benzina };
    [SerializeField] Category itemCategory;
    [SerializeField] List<string> listaStatistiche = new List<string>();
    [SerializeField] int itemIndex;
    [SerializeField] GameObject itemPrefab;
    public CraftingMaterial(string nome, string descrizione, Sprite icona, int tier, int price, Category itemCategory, List<string> listaStatistiche, int itemIndex, GameObject itemPrefab)
    {
        this.nome = nome;
        this.descrizione = descrizione;
        this.icona = icona;
        this.tier = tier;
        this.price = price;
        this.itemCategory = itemCategory;
        this.listaStatistiche = listaStatistiche;
        this.itemIndex = itemIndex;
        this.itemPrefab = itemPrefab;
    }

    public string Nome { get => nome; set => nome = value; }
    public string Descrizione { get => descrizione; set => descrizione = value; }
    public Sprite Icona { get => icona; set => icona = value; }
    public int Tier { get => tier; set => tier = value; }
    public int Price { get => price; set => price = value; }
    public Category ItemCategory { get => itemCategory; set => itemCategory = value; }
    public List<string> ListaStatistiche { get => listaStatistiche; set => listaStatistiche = value; }
    public int ItemIndex { get => itemIndex; set => itemIndex = value; }
    public GameObject ItemPrefab { get => itemPrefab; set => itemPrefab = value; }

    public override bool Equals(object obj)
    {
        return Equals(obj as CraftingMaterial);
    }
    public bool Equals(CraftingMaterial other)
    {
        return nome == other.nome;
    }

    public override int GetHashCode()
    {
        int hashCode = -1308677511;
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(nome);
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(descrizione);
        hashCode = hashCode * -1521134295 + EqualityComparer<Sprite>.Default.GetHashCode(icona);
        hashCode = hashCode * -1521134295 + tier.GetHashCode();
        hashCode = hashCode * -1521134295 + price.GetHashCode();
        hashCode = hashCode * -1521134295 + itemCategory.GetHashCode();
        hashCode = hashCode * -1521134295 + EqualityComparer<List<string>>.Default.GetHashCode(listaStatistiche);
        return hashCode;
    }
}

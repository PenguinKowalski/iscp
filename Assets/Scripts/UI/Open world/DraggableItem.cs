using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class DraggableItem : InventoryItem, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    Transform originalParent = null;
    int originalIndex;
    bool returnToOriginal = false, isInDropZone = false;
    [SerializeField] bool isRubbishBin = false;
    GameObject placeholder;
    [SerializeField] GameObject placeholderPrefab, recycleIcon;
    Canvas canvas;
    VehiclePartSlot assignedSlot;
   [SerializeField] Inventory_scroll thisInventory;
    CraftingSystem crafting;
    [SerializeField] Slider qntySlider;
    [SerializeField] GameObject quantityPrefab, menu;
    QuantitySlider sliderInfo;
    RubbishBinDropZone isRubbish;
    UIMenu menuUi;
    public Inventory_scroll ThisInventory { get => thisInventory; set => thisInventory = value; }
    public GameObject RecycleIcon { get => recycleIcon; set => recycleIcon = value; }
    public CraftingSystem Crafting { get => crafting; set => crafting = value; }
    public GameObject Menu { get => menu; set => menu = value; }

    public void OnBeginDrag(PointerEventData eventData)
    {
		originalParent = this.transform.parent;
		originalIndex = this.transform.GetSiblingIndex();
		this.transform.SetParent(canvas.transform);
		placeholder.transform.SetParent(originalParent);
		placeholder.transform.SetSiblingIndex(originalIndex);	
		this.GetComponent<CanvasGroup>().blocksRaycasts = false;
        recycleIcon.SetActive(true);

    }

    public void OnDrag(PointerEventData eventData)
    {
		this.transform.position = eventData.position;
		var parent = placeholder.transform.parent;
		if (parent != null && !returnToOriginal)
		{
			int newSiblingIndex = parent.childCount;
			for (int i = 0; i < parent.childCount; i++)
			{
				if (this.transform.position.x < parent.GetChild(i).position.x)
				{
					newSiblingIndex = i;

					if (placeholder.transform.GetSiblingIndex() < newSiblingIndex)
					newSiblingIndex--;
					break;
				}
			}
			placeholder.transform.SetSiblingIndex(newSiblingIndex);
		}
	}

    public void OnEndDrag(PointerEventData eventData)
    {
		GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (isInDropZone)
        {
            CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(ThisMaterial.Index);
            MaterialQuantity materialCopy = new MaterialQuantity(1, ThisMaterial.Index, ThisMaterial.VehicleDataId, ThisMaterial.IsVehicle);
            if(assignedSlot != null)
            {
                if (assignedSlot.ThisPart == null)
                {
                    assignedSlot.ThisPart = materialCopy;
                    assignedSlot.Inventory = thisInventory;
                    assignedSlot.ChangeDetails(materialInformation);
                }
                else if (assignedSlot.ThisPart.Index != ThisMaterial.Index)
                {
                    Debug.LogWarning("found slot");
                    assignedSlot.ThisPart = materialCopy;
                    assignedSlot.Inventory = thisInventory;
                    assignedSlot.ChangeDetails(materialInformation);
                }
                else if (assignedSlot.ThisPart.Index == ThisMaterial.Index)
                {
                    this.transform.SetParent(placeholder.transform.parent);
                    this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
                    placeholder.transform.SetParent(canvas.transform);
                    placeholder.SetActive(false);
                    isRubbish = null;
                    recycleIcon.SetActive(false);
                    return;
                }
            }

            if (ThisMaterial.Quantity == 1)
            {
                Debug.LogWarning(ThisMaterial.Quantity);
                if (!isRubbishBin)
                {
                    thisInventory.RemoveMaterial(ThisMaterial, CarListOpenWorld.Instance.PlayerInventory);
                }
                else
                {
                    for (int j = 0; j < OpenWorld._instance.PossibleRecipes.Count; j++)
                    {
                        for (int k = 0; k < OpenWorld._instance.PossibleRecipes[j].Profit.Count; k++)
                        {
                            if (OpenWorld._instance.PossibleRecipes[j].Profit[k].Index == ThisMaterial.Index)
                            {
                                crafting.RecicleItem(ThisMaterial);
                                thisInventory.RemoveMaterial(ThisMaterial, CarListOpenWorld.Instance.PlayerInventory);
                            }
                        }
                    }
                    
                }
            }
            else
            {
                Debug.LogWarning("is more than one");
                TextMeshProUGUI nomeElemento = GetComponentInChildren<TextMeshProUGUI>();
                if (!isRubbishBin)
                {
                    ThisMaterial.Quantity--;
                }
                else
                {
                    for (int j = 0; j < OpenWorld._instance.PossibleRecipes.Count; j++)
                    {
                        for (int k = 0; k < OpenWorld._instance.PossibleRecipes[j].Profit.Count; k++)
                            {
                            if (OpenWorld._instance.PossibleRecipes[j].Profit[k].Index == ThisMaterial.Index)
                            {
                                MaterialQuantity itemsToRecycle = new MaterialQuantity(ThisMaterial.Quantity, ThisMaterial.Index, ThisMaterial.VehicleDataId, ThisMaterial.IsVehicle);
                                GameObject go = Instantiate(quantityPrefab, canvas.transform);

                                sliderInfo = go.GetComponent<QuantitySlider>();
                                sliderInfo.AssignValues(itemsToRecycle);
                                qntySlider = sliderInfo.QntySlider;
                                //Debug.LogWarning(itemsToRecycle.Quantity);
                                sliderInfo.Ok.onClick.AddListener(delegate { Ok_OnClick(itemsToRecycle, ThisMaterial, nomeElemento, materialInformation); });
                                sliderInfo.Annulla.onClick.AddListener(delegate { Annulla_OnClick(); });
                            }
                        }
                    }
                    isRubbish.CloseLid();
                }
                placeholder.transform.SetParent(originalParent);
                placeholder.transform.SetSiblingIndex(originalIndex);
                ChangeItemDetails(nomeElemento, materialInformation, ThisMaterial);
            }
        }
        this.transform.SetParent(placeholder.transform.parent);
        this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
        placeholder.transform.SetParent(canvas.transform);
        placeholder.SetActive(false);
        isRubbish = null;
        assignedSlot = null;
        recycleIcon.SetActive(false);
    }
    public void Ok_OnClick(MaterialQuantity item, MaterialQuantity originalItem, TextMeshProUGUI nomeElemento, CraftingMaterial materialInformation)
    {
        item.Quantity = (int)qntySlider.value;
        crafting.RecicleItem(item);
        ThisMaterial.Quantity -= item.Quantity;
        if (ThisMaterial.Quantity <= 0)
        {
            thisInventory.RemoveMaterial(ThisMaterial, CarListOpenWorld.Instance.PlayerInventory);
        }
        ChangeItemDetails(nomeElemento, materialInformation, originalItem);
        recycleIcon.SetActive(false);
        Destroy(sliderInfo.gameObject);
    }
    public void Annulla_OnClick()
    {
        recycleIcon.SetActive(false);
        Destroy(sliderInfo.gameObject);
        return;
    }

    void ChangeItemDetails(TextMeshProUGUI nomeElemento, CraftingMaterial materialInformation, MaterialQuantity Item)
    {
        if (Item.Quantity > 1)
        {
            nomeElemento.text = materialInformation.Nome + " (" + Item.Quantity + ")";
        }
        else
        {
            nomeElemento.text = materialInformation.Nome;
        }
    }

	public void EnteringDropZone(Transform dropZone)
	{
        isRubbish = dropZone.gameObject.GetComponent<RubbishBinDropZone>();
        returnToOriginal = false;
        if(isRubbish != null)
        {
            assignedSlot = null;
            isRubbishBin = true;
        }
		else
        {
            assignedSlot = dropZone.gameObject.GetComponent<VehiclePartSlot>();
            isRubbishBin = false;
        }
        isInDropZone = true;
        
    }

	public void LeavingDropZone(Transform dropZone)
	{
		placeholder.transform.SetParent(originalParent);
		placeholder.transform.SetSiblingIndex(originalIndex);
		returnToOriginal = true;
		isInDropZone = false;
		assignedSlot = null;
        isRubbishBin = false;
    }

	public override void Start()
	{
        menuUi = GetComponentInParent<UIMenu>();
        hasExited = false;
        canvas = GetComponentInParent<Canvas>();
		GameObject go = Instantiate(placeholderPrefab, gameObject.transform.parent.gameObject.transform);
		if (placeholder == null)
		{
			placeholder = go;
			placeholder.SetActive(false);
		}
    }

    public void CategorySelection(Transform dropZone)
    {
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(ThisMaterial.Index);
        assignedSlot = dropZone.gameObject.GetComponent<VehiclePartSlot>();
        if (materialInformation.ItemCategory == CraftingMaterial.Category.arma && assignedSlot.AllCateg == VehiclePartSlot.AllowedCategory.arma)
        {
            EnteringDropZone(dropZone);
        }
        if (materialInformation.ItemCategory == CraftingMaterial.Category.armatura && assignedSlot.AllCateg == VehiclePartSlot.AllowedCategory.armatura)
        {
            EnteringDropZone(dropZone);
        }
        if (materialInformation.ItemCategory == CraftingMaterial.Category.motore && assignedSlot.AllCateg == VehiclePartSlot.AllowedCategory.motore)
        {
            EnteringDropZone(dropZone);
        }
    }
    public override void Update()
    {
        if (descriptionPrefab != null)
        {
            if (descPanel != null && hasExited)
            {
                menuUi.DescriptionPanel.gameObject.SetActive(false);
            }
        }
    }
    public override void OnPointerEnter(PointerEventData pointerEventData)
    {
        // thisAudio.pitch = Random.Range(0.75f, 1.25f);
        //thisAudio.PlayOneShot(hoverSounds[Random.Range(0, hoverSounds.Count)]);
        if (descriptionPrefab != null)
        {
            if (descPanel == null)
            {
                CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(thisMaterial.Index);
                menuUi.DescriptionPanel.ChangeDetails(materialInformation, -1);
                menuUi.DescriptionPanel.gameObject.SetActive(true);

                hasExited = false;
            }
        }
    }
    public override void OnPointerExit(PointerEventData pointerEventData)
    {
        if (descriptionPrefab != null)
        {
            menuUi.DescriptionPanel.gameObject.SetActive(false);
            hasExited = true;
        }
    }

}

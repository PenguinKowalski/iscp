using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;


public class CarItem : InventoryItem
{
    [SerializeField] VehicleData thisVehicle;
    UIMenu menuUi;
    public VehicleData ThisVehicle { get => thisVehicle; set => thisVehicle = value; }
    // Start is called before the first frame update

    public override void Start()
    {
        menuUi = GetComponentInParent<UIMenu>();
        hasExited = false;
    }
    public override void Update()
    {
        if (descriptionPrefab != null)
        {
            if (descPanel != null && hasExited)
            {
                menuUi.DescriptionPanel.gameObject.SetActive(false);
            }
        }
    }
    public override void OnPointerEnter(PointerEventData pointerEventData)
    {
        // thisAudio.pitch = Random.Range(0.75f, 1.25f);
        //thisAudio.PlayOneShot(hoverSounds[Random.Range(0, hoverSounds.Count)]);
        if (descriptionPrefab != null)
        {
            if (descPanel == null)
            {
                CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(thisMaterial.Index);
                menuUi.DescriptionPanel.ChangeDetails(materialInformation, -1);
                menuUi.DescriptionPanel.gameObject.SetActive(true);

                hasExited = false;
            }
        }
    }
    public override void OnPointerExit(PointerEventData pointerEventData)
    {
        if (descriptionPrefab != null)
        {
            menuUi.DescriptionPanel.gameObject.SetActive(false);
            hasExited = true;
        }
    }
}

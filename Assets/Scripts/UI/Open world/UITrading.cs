using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
public class UITrading : MonoBehaviour
{
    [SerializeField] Inventory_scroll inventarioPlayer, inventarioVenditore;
    int totalValue;
    [SerializeField] int minMaterialQuant;
    [SerializeField] Slider qntySlider;
    [SerializeField] GameObject quantityPrefab;
    QuantitySlider sliderPopUp;
    [SerializeField] int totalPrice, categoryIndex, vendorStartingMoney, playerStartingMoney;
    [SerializeField] TextMeshProUGUI totalPrice_text, categoryName;
    [SerializeField] Image totalPriceArrow;
    [SerializeField] OpenWorld.InventarioPlayer playerInventoryCopy, vendorInventoryCopy, originalVendorInventory;
    [SerializeField] ItemDescription descriptionPanel;
    MaterialQuantity benzinaPlayer, benzinaVenditore, benzinaPlayerCopy, benzinaVenditoreCopy;

    public QuantitySlider SliderPopUp { get => sliderPopUp; set => sliderPopUp = value; }
    public ItemDescription DescriptionPanel { get => descriptionPanel; set => descriptionPanel = value; }
    public OpenWorld.InventarioPlayer OriginalVendorInventory { get => originalVendorInventory; set => originalVendorInventory = value; }


    public void OnEnable()
    {
        Open();
    }
    public void OnDisable()
    {
        inventarioPlayer.ClearInventory();
        inventarioVenditore.ClearInventory();
        totalPrice = 0;
        categoryIndex = 0;
        totalPrice_text.text = "" + totalPrice;
        totalPriceArrow.transform.rotation = Quaternion.Euler(0, 0, 0);
        ChangeCategory(inventarioPlayer);
        ChangeCategory(inventarioVenditore);
    }
    public void GetVendorInventory(OpenWorld.InventarioPlayer traderInventory)
    {
        originalVendorInventory = traderInventory;
    }
    public void Purchase_OnClick()
    {
        benzinaPlayer.Quantity = benzinaPlayerCopy.Quantity;
        benzinaVenditore.Quantity = benzinaVenditoreCopy.Quantity;

        CarListOpenWorld.Instance.PlayerInventory = new OpenWorld.InventarioPlayer();
        for (int i = 0; i < playerInventoryCopy.ListaMateriali.Count; i++)
        {
            CarListOpenWorld.Instance.PlayerInventory.ListaMateriali.Add(playerInventoryCopy.ListaMateriali[i]);
        }
        for (int i = 0; i < playerInventoryCopy.ListaDatiVeicoli.Count; i++)
        {
            CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli.Add(playerInventoryCopy.ListaDatiVeicoli[i]);
        }

        originalVendorInventory.ListaMateriali.Clear();
        originalVendorInventory.ListaDatiVeicoli.Clear();
        for (int i = 0; i < vendorInventoryCopy.ListaMateriali.Count; i++)
        {
            originalVendorInventory.ListaMateriali.Add(vendorInventoryCopy.ListaMateriali[i]);
        }
        for (int i = 0; i < vendorInventoryCopy.ListaDatiVeicoli.Count; i++)
        {
            originalVendorInventory.ListaDatiVeicoli.Add(vendorInventoryCopy.ListaDatiVeicoli[i]);
        }
        OpenWorld._instance.CloseCombatLog(true);
        //gameObject.SetActive(false);
    }


    public void Item_OnClick(MaterialQuantity material, int index)
    {
        if (material.Quantity > minMaterialQuant)
        {
            if (sliderPopUp != null)
            {
                SelectionPopUpAnnulla();
            }
            GameObject go = Instantiate(quantityPrefab, transform);
            sliderPopUp = go.GetComponent<QuantitySlider>();
            sliderPopUp.QntySlider.maxValue = material.Quantity;
            sliderPopUp.SetItemPrice(material);
            sliderPopUp.Ok.onClick.AddListener(delegate { SelectionPopUpOk(material, index, sliderPopUp.QntySlider); });
            sliderPopUp.Annulla.onClick.AddListener(SelectionPopUpAnnulla);
        }
        else
        {
            if (sliderPopUp != null)
            {
                SelectionPopUpAnnulla();
            }
            MoveMaterial(material,1 , index);
        }
    }

    void SelectionPopUpOk(MaterialQuantity material, int index, Slider slider)
    {

        MoveMaterial(material, (int)slider.value, index);
        Destroy(sliderPopUp.gameObject);
    }

    void ChangeItemDetails(Inventory_scroll inventario, MaterialQuantity material)
    {
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(material.Index);
        for (int i = 0; i < inventario.ListaElementiLista.Count; i++)
        {
            if (material.Equals(inventario.ListaElementiLista[i].ThisMaterial))
            {
                inventario.ListaElementiLista[i].ThisMaterial.Quantity -= material.Quantity;
                TextMeshProUGUI nomeElemento = inventario.ListaElementiLista[i].GetComponentInChildren<TextMeshProUGUI>();
                if (inventario.ListaElementiLista[i].ThisMaterial.Quantity > 1)
                {
                    nomeElemento.text = materialInformation.Nome + " (" + inventario.ListaElementiLista[i].ThisMaterial.Quantity + ")";
                }
                else
                {
                    nomeElemento.text = materialInformation.Nome;
                }
            }
        }
    }
    void SelectionPopUpAnnulla()
    {
        Destroy(sliderPopUp.gameObject);
    }

    void MoveMaterial(MaterialQuantity material, int quantity, int index)
    {
        MaterialQuantity materialCopy = new MaterialQuantity(quantity, material.Index, material.VehicleDataId, material.IsVehicle);
        for (int i = 0; i < playerInventoryCopy.ListaMateriali.Count; i++)
        {
            CraftingMaterial materiale = CarListOpenWorld.Instance.GetMaterialIndex(playerInventoryCopy.ListaMateriali[i].Index);
            if (materiale.ItemCategory == CraftingMaterial.Category.benzina)
            {
                benzinaPlayer = playerInventoryCopy.ListaMateriali[i];
            }
        }
       // benzinaPlayerCopy = new MaterialQuantity(benzinaPlayer.Quantity, benzinaPlayer.Index, benzinaPlayer.VehicleDataId, benzinaPlayer.IsVehicle);
        for (int i = 0; i < vendorInventoryCopy.ListaMateriali.Count; i++)
        {
            CraftingMaterial materiale = CarListOpenWorld.Instance.GetMaterialIndex(vendorInventoryCopy.ListaMateriali[i].Index);
            if (materiale.ItemCategory == CraftingMaterial.Category.benzina)
            {
                benzinaVenditore = vendorInventoryCopy.ListaMateriali[i];
            }
        }
       
        //  benzinaVenditoreCopy = new MaterialQuantity(benzinaVenditore.Quantity, benzinaVenditore.Index, benzinaVenditore.VehicleDataId, benzinaVenditore.IsVehicle);
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(materialCopy.Index);
        if (index == 0)
        {
            if (!materialCopy.IsVehicle)
            {
                if (materialCopy.Quantity * materialInformation.Price <= benzinaVenditoreCopy.Quantity)
                {
                    totalPrice += materialInformation.Price * materialCopy.Quantity;     
                    inventarioVenditore.AddMaterial(materialCopy, vendorInventoryCopy, null);
                }
                else
                {
                    totalPrice += benzinaVenditoreCopy.Quantity;
                    inventarioVenditore.AddMaterial(materialCopy, vendorInventoryCopy, null);
                }
            }
            else
            {
                VehicleData vehicleInfo = null;

                for (int i = 0; i < playerInventoryCopy.ListaDatiVeicoli.Count; i++)
                {
                    if (playerInventoryCopy.ListaDatiVeicoli[i].CarId.Equals(materialCopy.VehicleDataId))
                    {
                        vehicleInfo = playerInventoryCopy.ListaDatiVeicoli[i];

                    }
                }
                if (GetCarTotalPrice(vehicleInfo) <= benzinaVenditoreCopy.Quantity)
                {
                    totalPrice += GetCarTotalPrice(vehicleInfo);
                    inventarioVenditore.AddMaterial(materialCopy, vendorInventoryCopy, vehicleInfo);
                }
                else
                {
                    totalPrice += benzinaVenditoreCopy.Quantity;
                    inventarioVenditore.AddMaterial(materialCopy, vendorInventoryCopy, vehicleInfo);
                }
            }
            inventarioPlayer.RemoveMaterial(materialCopy, playerInventoryCopy);
        }
        else
        {
            if(!materialCopy.IsVehicle)
            {
                if (materialCopy.Quantity * materialInformation.Price <= benzinaPlayerCopy.Quantity)
                {                    
                    totalPrice -= materialInformation.Price * materialCopy.Quantity;
                    inventarioPlayer.AddMaterial(materialCopy, playerInventoryCopy, null);
                    inventarioVenditore.RemoveMaterial(materialCopy, vendorInventoryCopy);
                }
                
            }
            else
            {
                VehicleData vehicleInfo = null;
                for (int i = 0; i < vendorInventoryCopy.ListaDatiVeicoli.Count; i++)
                {
                    if (vendorInventoryCopy.ListaDatiVeicoli[i].CarId.Equals(materialCopy.VehicleDataId))
                    {
                        vehicleInfo = vendorInventoryCopy.ListaDatiVeicoli[i];
                    }
                }
                if(GetCarTotalPrice(vehicleInfo) <= benzinaPlayerCopy.Quantity)
                {
                    totalPrice -= GetCarTotalPrice(vehicleInfo);
                    inventarioPlayer.AddMaterial(materialCopy, playerInventoryCopy, vehicleInfo);
                    inventarioVenditore.RemoveMaterial(materialCopy, vendorInventoryCopy);
                }
            } 
        }

        benzinaPlayerCopy.Quantity = playerStartingMoney + totalPrice;
        benzinaVenditoreCopy.Quantity = vendorStartingMoney - totalPrice;

        if (totalPrice >= 0)
        {
            totalPrice_text.text = "" + totalPrice;
            totalPriceArrow.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            totalPrice_text.text = "" + -totalPrice;
            totalPriceArrow.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }
    public int GetCarTotalPrice(VehicleData data)
    {
        int totalPrice = 0;
        for (int i = 0; i < CarListOpenWorld.Instance.ListaMateriali.Count; i++)
        {
            if (CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex != 0)
            {
                if (CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex == data.VehicleType)
                {
                    totalPrice += CarListOpenWorld.Instance.ListaMateriali[i].Price;
                }
                if (CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex == data.WeaponType)
                {
                    totalPrice += CarListOpenWorld.Instance.ListaMateriali[i].Price;
                }
                if (CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex == data.ArmorType)
                {
                    totalPrice += CarListOpenWorld.Instance.ListaMateriali[i].Price;
                }
                if (CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex == data.MotorType)
                {
                    totalPrice += CarListOpenWorld.Instance.ListaMateriali[i].Price;
                }
            }
        }
        return totalPrice;
    }
    public void Open()
    {
        descriptionPanel.gameObject.SetActive(false);
        playerInventoryCopy = new OpenWorld.InventarioPlayer();
        for(int i = 0; i < CarListOpenWorld.Instance.PlayerInventory.ListaMateriali.Count; i++)
        {
            playerInventoryCopy.ListaMateriali.Add(CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[i]);
        }
        for (int i = 0; i < CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli.Count; i++)
        {
            playerInventoryCopy.ListaDatiVeicoli.Add(CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli[i]);
        }

        vendorInventoryCopy = new OpenWorld.InventarioPlayer();
        for (int i = 0; i < originalVendorInventory.ListaMateriali.Count; i++)
        {
            vendorInventoryCopy.ListaMateriali.Add(originalVendorInventory.ListaMateriali[i]);
        }
        for (int i = 0; i < originalVendorInventory.ListaDatiVeicoli.Count; i++)
        {
            vendorInventoryCopy.ListaDatiVeicoli.Add(originalVendorInventory.ListaDatiVeicoli[i]);
        }

        totalPrice_text.text = "" + totalPrice;
        totalPriceArrow.transform.rotation = Quaternion.Euler(0, 0, 0);

        inventarioPlayer.SpawnList(playerInventoryCopy.ListaMateriali, playerInventoryCopy);
        for (int i = 0; i < playerInventoryCopy.ListaMateriali.Count; i++)
        {
            CraftingMaterial materiale = CarListOpenWorld.Instance.GetMaterialIndex(playerInventoryCopy.ListaMateriali[i].Index);
            if (materiale.ItemCategory == CraftingMaterial.Category.benzina)
            {
                benzinaPlayer = playerInventoryCopy.ListaMateriali[i];
                playerStartingMoney = benzinaPlayer.Quantity;
            }
        }
        benzinaPlayerCopy = new MaterialQuantity(benzinaPlayer.Quantity, benzinaPlayer.Index, benzinaPlayer.VehicleDataId, benzinaPlayer.IsVehicle);
        inventarioPlayer.BenzinaDisponibile.text = "" + benzinaPlayer.Quantity;
        inventarioVenditore.SpawnList(vendorInventoryCopy.ListaMateriali, vendorInventoryCopy);
        for (int i = 0; i < vendorInventoryCopy.ListaMateriali.Count; i++)
        {
            CraftingMaterial materiale = CarListOpenWorld.Instance.GetMaterialIndex(vendorInventoryCopy.ListaMateriali[i].Index);
            if (materiale.ItemCategory == CraftingMaterial.Category.benzina)
            {
                benzinaVenditore = vendorInventoryCopy.ListaMateriali[i];
                vendorStartingMoney = benzinaVenditore.Quantity;
            }
        }
        inventarioVenditore.BenzinaDisponibile.text = "" + benzinaVenditore.Quantity;
        benzinaVenditoreCopy = new MaterialQuantity(benzinaVenditore.Quantity, benzinaVenditore.Index, benzinaVenditore.VehicleDataId, benzinaVenditore.IsVehicle);
    }
    void Close()
    {

    }

    void ChangeCategory(Inventory_scroll inventoryScroll)
    {
        CraftingMaterial.Category category = CraftingMaterial.Category.arma;
        switch(categoryIndex)
        {
            case 1:
            categoryName.text = "Weapons";
            category = CraftingMaterial.Category.arma;
                break;
            case 2:
                categoryName.text = "Vehicles";
                category = CraftingMaterial.Category.veicoli;
                break;
            case 3:
                categoryName.text = "Armour";
                category = CraftingMaterial.Category.armatura;
                break;
            case 4:
                categoryName.text = "Engines";
                category = CraftingMaterial.Category.motore;
                break;
            case 5:
                categoryName.text = "Materials";
                category = CraftingMaterial.Category.materiale;
                break;
            default:
                categoryName.text = "All";
                break;
        }
        for (int i = 0; i < inventoryScroll.ListaElementiLista.Count; i++)
        {
            CategoryCheck(inventoryScroll.ListaElementiLista[i], category);
        }
    }


    void CategoryCheck(InventoryItem item, CraftingMaterial.Category category)
    {
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(item.ThisMaterial.Index);
        if (materialInformation.ItemCategory == category || categoryIndex == 0)
        {
            item.gameObject.SetActive(true);
        }
        else
        {
            item.gameObject.SetActive(false);
        }
    }
    
    public void CategoryUp_onClick()
    {
        if(categoryIndex < 5)
        {
            categoryIndex++;
        }
        else
        {
            categoryIndex = 0;
        }
        ChangeCategory(inventarioPlayer);
        ChangeCategory(inventarioVenditore);
    }

    public void CategoryDown_onClick()
    {
        if (categoryIndex > 0)
        {
            categoryIndex--;
        }
        else
        {
            categoryIndex = 5;
        }
        ChangeCategory(inventarioPlayer);
        ChangeCategory(inventarioVenditore);
    }
    [System.Serializable]
    class listaInventario
    {
        [SerializeField]List<MaterialQuantity> listaMateriali = new List<MaterialQuantity>();

        public List<MaterialQuantity> ListaMateriali { get => listaMateriali; set => listaMateriali = value; }
    }
}
 
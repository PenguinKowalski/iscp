using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorFollower : MonoBehaviour
{
    [SerializeField] Vector3 offset;

    private void Update()
    {
        this.gameObject.transform.position = Input.mousePosition + offset;
    }
}

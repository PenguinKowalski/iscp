using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScrollInventarioMenu : Inventory_scroll
{
    [SerializeField]UIMenu uIMenu;
    [SerializeField] GameObject rubbishBin, menu;
    [SerializeField] CraftingSystem crafting;

    private void OnEnable()
    {
        uIMenu.Open();
    }
    public void OnDisable()
    {
        uIMenu.DisableMenu();
    }
    public override void Start()
    {
        down.onClick.AddListener(delegate { ButtonDown_OnClick(scrollView, ListaElementiLista.Count - minElementsPerScroll); });
        up.onClick.AddListener(delegate { ButtonUp_OnClick(scrollView, ListaElementiLista.Count - minElementsPerScroll); });
        uIMenu = GetComponentInParent<UIMenu>();
    }
    public override void itemClickFunctionality(MaterialQuantity material, int? index, UITrading trading)
    {
        
    }
    public override void SpawnObjectInList(MaterialQuantity material, UITrading trading, Recipe recipe, VehicleData data)
    {
        DraggableItem item;
        GameObject go = Instantiate(elementoLista, content.transform);
        item = go.GetComponent<DraggableItem>();
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(material.Index);
        item.ThisMaterial = material;
        item.ThisInventory = this;
        item.RecycleIcon = rubbishBin;
        item.Crafting = crafting;
        item.Menu = menu;
        item.ChangeDetails(materialInformation, material);
        listaElementiLista.Add(item);
    }
    public override void SpawnList(List<MaterialQuantity> material, OpenWorld.InventarioPlayer inventory)
    {
        for (int i = 0; i < material.Count; i++)
        {
            CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(material[i].Index);
            if (materialInformation.ItemCategory != CraftingMaterial.Category.materiale && materialInformation.ItemCategory != CraftingMaterial.Category.veicoli && materialInformation.ItemCategory != CraftingMaterial.Category.benzina)
            {
                SpawnObjectInList(material[i], null, null, null);
            }          
        }
    }
    public override void RemoveMaterial(MaterialQuantity objectDetails, OpenWorld.InventarioPlayer inventario)
    {
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(objectDetails.Index);
        MaterialQuantity materialExists = OpenWorld._instance.RemoveItem(objectDetails, inventario);
        InventoryItem item = FindInventoryItem(materialInformation);
        if (materialExists != null)
        {
            item.ChangeDetails(materialInformation, materialExists);
        }
        else
        {
            if (item.DescPanel != null)
            {
                Destroy(item.DescPanel.gameObject);
            }
            Destroy(item.gameObject);
            listaElementiLista.Remove(item);
        }
    }
    public override void AddMaterial(MaterialQuantity objectDetails, OpenWorld.InventarioPlayer inventario, VehicleData data)
    {
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(objectDetails.Index);
        MaterialQuantity materialExists = OpenWorld._instance.AddItem(objectDetails, inventario, data);
        InventoryItem item = FindInventoryItem(materialInformation);
        if (materialExists != null)
        {
            if (item == null)
            {
                SpawnObjectInList(objectDetails, null, null, data);
            }
            else
            {
                item.ChangeDetails(materialInformation, materialExists);
            }
        }
    }

   
}

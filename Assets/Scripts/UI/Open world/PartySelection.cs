using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartySelection : MonoBehaviour
{
    [SerializeField] GameObject partyButton;
    [SerializeField] GameObject parentList;

    // Start is called before the first frame update
    void Start()
    {
        PopulateList();
    }

    void PopulateList()
    {
        for(int k = 0; k < CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli.Count; k++)
        {
            GameObject item = Instantiate(partyButton, parentList.transform);
            PartySelectionButton partyBt = item.GetComponent<PartySelectionButton>();
            VehicleData veicolo = CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli[k];
            partyBt.Bt.onClick.AddListener(delegate { AddVehicleToParty(veicolo, partyBt); });

            partyBt.Icon.sprite = CarListOpenWorld.Instance.GetMaterialIndex(veicolo.VehicleType).Icona;
            partyBt.WeaponIcon.sprite = CarListOpenWorld.Instance.GetMaterialIndex(veicolo.WeaponType).Icona;
            partyBt.Nome.text = veicolo.CarName;
            partyBt.HealthPerc = veicolo.HealthPerc;
        }
    }

    void AddVehicleToParty(VehicleData data, PartySelectionButton bt)
    {
        if (bt.CanAdd)
        {
            //Debug.Log("aggiungo");
            OpenWorld._instance.PlayerScript.InteractableData.CarInfo.Add(data);
            bt.CanAdd = false;
        }
        else
        {
            //Debug.Log("rimuovo");
            OpenWorld._instance.PlayerScript.InteractableData.CarInfo.Remove(data);
            bt.CanAdd = true;
        }
    }

}

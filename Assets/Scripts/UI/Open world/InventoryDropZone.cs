using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryDropZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("OnPointerEnter");
        if (eventData.pointerDrag == null)
        {
            return;
        }

        VehiclePartIcon d = eventData.pointerDrag.GetComponent<VehiclePartIcon>();
        if (d != null)
        {
            d.EnteringDropZone(this.transform);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("OnPointerExit");
        if (eventData.pointerDrag == null)
        {
            return;
        }

        VehiclePartIcon d = eventData.pointerDrag.GetComponent<VehiclePartIcon>();
        if (d != null)
        {
            d.LeavingDropZone(this.transform);
        }
    }

}

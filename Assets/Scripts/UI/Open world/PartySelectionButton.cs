using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PartySelectionButton : MonoBehaviour
{
    bool canAdd = true;
    [SerializeField] Image icon;
    [SerializeField] TMP_Text nome;
    [SerializeField] Button bt;
    [SerializeField] Image btImage;
    [SerializeField] Image weaponIcon;
    [SerializeField] Slider healthSlider;
    [SerializeField] Color avaiable, selected;
    float healthPerc;
    
    public bool CanAdd { get => canAdd; set => canAdd = value; }
    public Image Icon { get => icon; set => icon = value; }
    public TMP_Text Nome { get => nome; set => nome = value; }
    public Button Bt { get => bt; set => bt = value; }
    public Image WeaponIcon { get => weaponIcon; set => weaponIcon = value; }
    public float HealthPerc { get => healthPerc; set => healthPerc = value; }

    private void Update()
    {
        healthSlider.value = healthPerc;
        if (canAdd)
        {
            btImage.color = selected;
        }
        else
        {
            btImage.color = avaiable;
        }
    }
}

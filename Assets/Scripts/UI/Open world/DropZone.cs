using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public void OnPointerEnter(PointerEventData eventData)
	{
		if (eventData.pointerDrag == null)
		{
			return;
		}

		DraggableItem d = eventData.pointerDrag.GetComponent<DraggableItem>();
		if (d != null)
		{
			d.EnteringDropZone(this.transform);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{

		if (eventData.pointerDrag == null)
		{
			return;
		}

		DraggableItem d = eventData.pointerDrag.GetComponent<DraggableItem>();
		if (d != null)
		{
			d.LeavingDropZone(this.transform);
		}
	}

}

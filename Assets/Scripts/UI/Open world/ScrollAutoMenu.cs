using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScrollAutoMenu : Inventory_scroll
{
    UIMenu uIMenu;
    public override void Start()
    {
        down.onClick.AddListener(delegate { ButtonDown_OnClick(scrollView, ListaElementiLista.Count - minElementsPerScroll); });
        up.onClick.AddListener(delegate { ButtonUp_OnClick(scrollView, ListaElementiLista.Count - minElementsPerScroll); });
        uIMenu = GetComponentInParent<UIMenu>();
    }
    public override void SpawnObjectInList(MaterialQuantity material, UITrading trading, Recipe recipe, VehicleData data)
    {
        CraftingMaterial materialInformation = null;
        CarItem item;
        GameObject go = Instantiate(elementoLista, content.transform);
        item = go.GetComponent<CarItem>();
        item.ThisVehicle = data;
        materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(material.Index);
        item.ThisMaterial = material;
        item.Material = materialInformation;
        listaElementiLista.Add(item);
        item.Index = listaElementiLista.Count - 1;
        item.ChangeDetails(materialInformation, material);
        item.Bt.onClick.AddListener(delegate { carClickFunctionality(material, data); });
    }
    public void carClickFunctionality(MaterialQuantity material, VehicleData data)
    {
        uIMenu.CarOnClick(material, data);
    }
    public override void SpawnList(List<MaterialQuantity> material, OpenWorld.InventarioPlayer inventory)
    {
        for (int i = 0; i < material.Count; i++)
        {
            CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(material[i].Index);
            VehicleData data = null;
            if (material[i].IsVehicle)
            {
                //Debug.LogError(inventory.ListaDatiVeicoli.Count);
                for (int j = 0; j < inventory.ListaDatiVeicoli.Count; j++)
                {
                    if (inventory.ListaDatiVeicoli[j].CarId.Equals(material[i].VehicleDataId))
                    {
                        data = inventory.ListaDatiVeicoli[j];
                    }
                }
            }
            if (materialInformation.ItemCategory == CraftingMaterial.Category.veicoli)
            {
                SpawnObjectInList(material[i], null, null, data);
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CarSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    Image Icon;
    CraftingMaterial thisPart;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ChangePartInformation()
    {
        Icon.sprite = thisPart.Icona;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null)
        {
            return;
        }

        DraggableItem d = eventData.pointerDrag.GetComponent<DraggableItem>();
        if (d != null)
        {
            d.EnteringDropZone(this.transform);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("OnPointerExit");
        if (eventData.pointerDrag == null)
        {
            return;
        }

        DraggableItem d = eventData.pointerDrag.GetComponent<DraggableItem>();
        if (d != null)
        {
            d.LeavingDropZone(this.transform);
        }
    }
}

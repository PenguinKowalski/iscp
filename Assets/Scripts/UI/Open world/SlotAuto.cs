using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class SlotAuto : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI name_txt;
    [SerializeField] List<TextMeshProUGUI> listaStatistiche = new List<TextMeshProUGUI>();
    [SerializeField] Image carImage;
    [SerializeField] GameObject statistichePrefab, panelStatistiche;
    bool prevListDeleted;
    [SerializeField] OpenWorld openWorld;
    VehicleData vehicleData;
    [SerializeField] TMP_InputField carName;

    [SerializeField] List<VehiclePartSlot> listaSlot = new List<VehiclePartSlot>();

    private void Start()
    {     
        for (int i = 0; i < listaSlot.Count; i++)
        {
            listaSlot[i].gameObject.SetActive(false);
        }
        this.gameObject.SetActive(false);
        carName.onEndEdit.AddListener(delegate { SetCarName(carName.text); });
    }
    private void Update()
    {
        if (carName.isFocused)
        {
            OpenWorld._instance.IsWritingName = true;
        }
        else
        {
            OpenWorld._instance.IsWritingName = false;
        }
    }

    public void SetCarName(string carName_)
    {
        vehicleData.CarName = carName_;
    }
    public void ChangeInformation(MaterialQuantity vehicle, VehicleData data)
    {
        vehicleData = data;
        VehicleInfo vehicleInfo = OpenWorld._instance.GetCarIndex(data.VehicleType);
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(vehicle.Index);
        for (int i = 0; i < listaSlot.Count; i++)
        {       
            if(vehicleInfo.ListaSlot[i])
            {
                listaSlot[i].gameObject.SetActive(true);
            }
            else
            {
                listaSlot[i].gameObject.SetActive(false);
            }
            listaSlot[i].SetInfo(data);
        }
        prevListDeleted = false;
        if (!prevListDeleted)
        {
            for (int i = 0; i < listaStatistiche.Count; i++)
            {
                Destroy(listaStatistiche[i].gameObject);
            }
            listaStatistiche.Clear();
            prevListDeleted = true;
        }
        name_txt.text = materialInformation.Nome;
        if(data.CarName == "")
        {
            carName.text = materialInformation.Nome;
        }
        else
        {
            carName.text = data.CarName;
        }
        for (int i = 0; i < materialInformation.ListaStatistiche.Count; i++)
        {
            GameObject go = Instantiate(statistichePrefab, panelStatistiche.transform);
            TextMeshProUGUI statistica = go.GetComponent<TextMeshProUGUI>();
            statistica.text = materialInformation.ListaStatistiche[i];
            listaStatistiche.Add(statistica);
        }
        carImage.sprite = materialInformation.Icona;
    }
}

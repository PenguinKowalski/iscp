using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemDescription : MonoBehaviour
{
    [SerializeField] Image Icon;
    [SerializeField] TextMeshProUGUI name_txt, description_txt, tier_txt, price_txt;
    public void Start()
    {
        this.gameObject.SetActive(false);
    }
    public void ChangeDetails(CraftingMaterial materialDetails, int price)
    {
        Icon.sprite = materialDetails.Icona;
        name_txt.text = materialDetails.Nome;
        description_txt.text = materialDetails.Descrizione;
        tier_txt.text = "Tier " + materialDetails.Tier;
        if(price > -1)
        {
            price_txt.text = price + " gasoline";
            if (!price_txt.gameObject.activeSelf)
            {
                price_txt.gameObject.SetActive(true);
            }
        }
        else
        {
            if (price_txt.gameObject.activeSelf)
            {
                price_txt.gameObject.SetActive(false);
            }
        }
        
    }
}

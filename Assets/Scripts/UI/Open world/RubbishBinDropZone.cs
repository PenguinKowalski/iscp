using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RubbishBinDropZone : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]Sprite rubbishOpen, rubbishClosed;
    Image rubbishIcon;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(false);
        rubbishIcon = GetComponent<Image>();
        rubbishIcon.sprite = rubbishClosed;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("OnPointerEnter");
        if (eventData.pointerDrag == null)
        {
            return;
        }
        rubbishIcon.sprite = rubbishOpen;
        DraggableItem d = eventData.pointerDrag.GetComponent<DraggableItem>();
        if (d != null)
        {
            d.EnteringDropZone(this.transform);
        }
        VehiclePartIcon v = eventData.pointerDrag.GetComponent<VehiclePartIcon>();
        if (v != null)
        {
            v.EnteringDropZone(this.transform);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
        //Debug.Log("OnPointerExit");
        if (eventData.pointerDrag == null)
        {
            return;
        }
        rubbishIcon.sprite = rubbishClosed;
        DraggableItem d = eventData.pointerDrag.GetComponent<DraggableItem>();
        if (d != null)
        {
            d.LeavingDropZone(this.transform);
        }
        VehiclePartIcon v = eventData.pointerDrag.GetComponent<VehiclePartIcon>();
        if (v != null)
        {
            v.EnteringDropZone(this.transform);
        }
    }
    public void CloseLid()
    {
        rubbishIcon.sprite = rubbishClosed;
    }
}

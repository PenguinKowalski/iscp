using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CraftingRecipeUI : MonoBehaviour
{
    [SerializeField] List<TMP_Text> costi = new List<TMP_Text>();
    [SerializeField] Image icon;
    [SerializeField] TMP_Text nome;
    [SerializeField] Button bt;

    public List<TMP_Text> Costi { get => costi; set => costi = value; }
    public Image Icon { get => icon; set => icon = value; }
    public TMP_Text Nome { get => nome; set => nome = value; }
    public Button Bt { get => bt; set => bt = value; }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VehicleInfo
{
    [SerializeField] int index;
    [SerializeField] List<bool> listaSlot = new List<bool>();

    public VehicleInfo(int index, List<bool> listaSlot)
    {
        this.index = index;
        this.listaSlot = listaSlot;
    }
    public int Index { get => index; set => index = value; }
    public List<bool> ListaSlot { get => listaSlot; set => listaSlot = value; }
}

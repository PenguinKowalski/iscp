using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class EventPopUp : MonoBehaviour
{
    [SerializeField]GameObject buttonPrefab, buttonSpace;
    [SerializeField] TMP_Text descriptionEvent;

    List<Button> eventButtons = new List<Button>();
    

    public void SpawnButtons(string description, List<string> buttonNames, List<UnityAction> unityActions)
    {
        for(int i = 0; i < eventButtons.Count; i++)
        {
            Destroy(eventButtons[i].gameObject);
        }
        eventButtons.Clear();

        for (int i = 0; i < buttonNames.Count; i++)
        {
            GameObject go = Instantiate(buttonPrefab, buttonSpace.gameObject.transform);
            eventButtons.Add(go.gameObject.GetComponent<Button>());
            eventButtons[i].onClick.AddListener(unityActions[i]);

            eventButtons[i].GetComponentInChildren<TMP_Text>().text = buttonNames[i];
        }
        descriptionEvent.text = description;
    }

}

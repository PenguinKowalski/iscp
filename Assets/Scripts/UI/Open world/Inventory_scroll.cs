using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Inventory_scroll : MonoBehaviour
{
    [SerializeField] protected ScrollRect scrollView;
    [SerializeField] protected Button up, down;
    [SerializeField] protected GameObject content;
    [SerializeField] protected List<InventoryItem> listaElementiLista = new List<InventoryItem>();
    [SerializeField] protected int minElementsPerScroll;
    [SerializeField] protected GameObject elementoLista;
    [SerializeField] protected int index;
    [SerializeField]UITrading tradingUi;
    [SerializeField] TextMeshProUGUI benzinaDisponibile;
    public List<InventoryItem> ListaElementiLista { get => listaElementiLista; set => listaElementiLista = value; }
    public int Index { get => index; set => index = value; }
    public TextMeshProUGUI BenzinaDisponibile { get => benzinaDisponibile; set => benzinaDisponibile = value; }

    // Start is called before the first frame update
    public virtual void Start()
    {
        down.onClick.AddListener(delegate { ButtonDown_OnClick(scrollView, listaElementiLista.Count - minElementsPerScroll); });
        up.onClick.AddListener(delegate { ButtonUp_OnClick(scrollView, listaElementiLista.Count - minElementsPerScroll); });
        tradingUi = GetComponentInParent<UITrading>();
    }
    public virtual void SpawnObjectInList(MaterialQuantity material, UITrading trading, Recipe recipe, VehicleData data)
    {
        CraftingMaterial materialInformation = null;
        InventoryItem item;
        GameObject go = Instantiate(elementoLista, content.transform);
        item = go.GetComponent<InventoryItem>();
        materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(material.Index);
        item.ThisMaterial = material;
        item.Material = materialInformation;
        listaElementiLista.Add(item);
        item.Index = listaElementiLista.Count - 1;
        item.ChangeDetails(materialInformation, material);
        if (data != null)
        {
            item.Price = tradingUi.GetCarTotalPrice(data);
        }
        else
        {
            item.Price = materialInformation.Price;
        }
        item.Bt.onClick.AddListener(delegate { itemClickFunctionality(material, index, trading); });
    }

    public virtual void itemClickFunctionality(MaterialQuantity material, int? index, UITrading trading)
    {
        trading.Item_OnClick(material, index.Value);
    }
    public virtual void SpawnList(List<MaterialQuantity> material, OpenWorld.InventarioPlayer inventory)
    {
        for (int i = 0; i < material.Count; i++)
        {
            CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(material[i].Index);
            VehicleData data = null;
            if(material[i].IsVehicle)
            {
                for (int j = 0; j < inventory.ListaDatiVeicoli.Count; j++)
                {
                    if (inventory.ListaDatiVeicoli[j].CarId.Equals(material[i].VehicleDataId))
                    {
                        data = inventory.ListaDatiVeicoli[j];
                    }
                }
            }
            if ( materialInformation.ItemCategory != CraftingMaterial.Category.benzina)
            {
                SpawnObjectInList(material[i], tradingUi, null, data);
            }           
        }
    }

    public void ClearInventory()
    {
        if(listaElementiLista != null)
        {
            for (int i = 0; i < listaElementiLista.Count; i++)
            {
                Destroy(listaElementiLista[i].gameObject);
            }
            listaElementiLista.Clear();
        }
    }
    void Update()
    {
        if (listaElementiLista.Count > minElementsPerScroll)
        {
            if (scrollView.verticalNormalizedPosition >= 0.985f)
            {
                up.gameObject.SetActive(false);
                down.gameObject.SetActive(true);
            }
            else if (scrollView.verticalNormalizedPosition <= 0.015f)
            {
                down.gameObject.SetActive(false);
            }
            else
            {
                down.gameObject.SetActive(true);
                up.gameObject.SetActive(true);
            }
        }
        else
        {
            down.gameObject.SetActive(false);
            up.gameObject.SetActive(false);
        }
    }

    protected void ButtonDown_OnClick(ScrollRect scrollRect, float listLength)
    {
        float calcolo = 1f / (float)listLength;
        scrollRect.verticalNormalizedPosition -= calcolo;
        Canvas.ForceUpdateCanvases();
    }
    protected void ButtonUp_OnClick(ScrollRect scrollRect, float listLength)
    {
        float calcolo = 1f / (float)listLength;
        scrollRect.verticalNormalizedPosition += calcolo;
        Canvas.ForceUpdateCanvases();
    }
    public virtual void AddMaterial(MaterialQuantity objectDetails, OpenWorld.InventarioPlayer inventoryCopy, VehicleData carData)
    {
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(objectDetails.Index);
        MaterialQuantity materialExists = OpenWorld._instance.AddItem(objectDetails, inventoryCopy, carData);
        InventoryItem item = FindInventoryItem(materialInformation);
        if (materialExists != null)
        {
            if (item == null || objectDetails.IsVehicle)
            {
                SpawnObjectInList(objectDetails, tradingUi, null, carData);
            }
            else
            {
                item.ChangeDetails(materialInformation, materialExists);
            }
        }
    }

    public virtual void RemoveMaterial(MaterialQuantity objectDetails, OpenWorld.InventarioPlayer inventoryCopy)
    {
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(objectDetails.Index);
        MaterialQuantity materialExists = OpenWorld._instance.RemoveItem(objectDetails, inventoryCopy);
        InventoryItem item = FindInventoryItem(materialInformation);
        if(materialExists != null)
        {
            item.ChangeDetails(materialInformation, materialExists);
        }
        else
        {
            if (item.DescPanel != null)
            {
                Destroy(item.DescPanel.gameObject);
            }
            Destroy(item.gameObject);
            listaElementiLista.Remove(item);
        }
    }
    protected InventoryItem FindInventoryItem(CraftingMaterial materialInformation)
    {
        for (int i = 0; i < listaElementiLista.Count; i++)
        {
            if (materialInformation.ItemIndex == listaElementiLista[i].ThisMaterial.Index)
            {
                return listaElementiLista[i];
            }
        }
        return null;
    }
}

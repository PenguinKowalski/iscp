using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class VehiclePartSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField]VehiclePartIcon icon;
	[SerializeField]MaterialQuantity thisPart, previousPart;
	[SerializeField]bool hasItemAssigned = false;
    [SerializeField] Inventory_scroll inventory;
	VehicleData data;
	string prevDataId;
    public enum AllowedCategory { arma, armatura, motore }
    [SerializeField]AllowedCategory allCateg;
    public MaterialQuantity ThisPart { get => thisPart; set => thisPart = value; }
    public Inventory_scroll Inventory { get => inventory; set => inventory = value; }
    public AllowedCategory AllCateg { get => allCateg; set => allCateg = value; }
    public bool HasItemAssigned { get => hasItemAssigned; set => hasItemAssigned = value; }
    public MaterialQuantity PreviousPart { get => previousPart; set => previousPart = value; }

    public void OnPointerEnter(PointerEventData eventData)
	{
		if (eventData.pointerDrag == null)
		{
			return;
		}
		DraggableItem d = eventData.pointerDrag.GetComponent<DraggableItem>();
		if (d != null)
		{
			d.CategorySelection(this.transform);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (eventData.pointerDrag == null)
		{
			return;
		}

		DraggableItem d = eventData.pointerDrag.GetComponent<DraggableItem>();
		if (d != null)
		{
			d.LeavingDropZone(this.transform);
		}
	}
	public void ChangeDetails(CraftingMaterial part)
    {
		if(part == null)
        {
			if(icon.gameObject.activeSelf)
            {
				icon.gameObject.SetActive(false);
				hasItemAssigned =false;
				previousPart = null;
			}
			return;
        }
        else
        {
			thisPart = new MaterialQuantity(1, part.ItemIndex, null, false);
        }
		if (previousPart != null )
		{
			inventory.AddMaterial(previousPart, CarListOpenWorld.Instance.PlayerInventory, null);
        }
        previousPart = thisPart;
        prevDataId = data.CarId;
        CheckCategory(part.ItemIndex);
        icon.gameObject.SetActive(true);
	    icon.PartInformation = thisPart;
		icon.Icon.sprite = part.Icona;
        //icon.InventoryToAddTo = inventory;
    }
	public void SetInfo(VehicleData vehicle)
    {
		data = vehicle;
		previousPart = null;
        /*if(data != null)
        {
			prevDataId = data.CarId;
            data = vehicle;
            if (data.CarId != prevDataId)
            {
                previousPart = null;
            }
        }
        else
        {
            prevDataId = null;
            data = vehicle;
            previousPart = icon.PartInformation;
        }*/

        CraftingMaterial materialInformation = null;
		if (AllCateg == AllowedCategory.arma)
		{
			if(data.WeaponType != 0)
			materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(data.WeaponType);
		}
		if (AllCateg == AllowedCategory.armatura)
		{
			if (data.ArmorType != 0)
				materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(data.ArmorType);
		}
		if (AllCateg == AllowedCategory.motore)
		{
			if (data.MotorType != 0)
				materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(data.MotorType);
		}
		ChangeDetails(materialInformation);
		hasItemAssigned = true;
	}
    public void CheckCategory(int index)
    {
        if (AllCateg == AllowedCategory.arma)
        {
            data.WeaponType = index;
        }
        if (AllCateg == AllowedCategory.armatura)
        {
            data.ArmorType = index;
        }
        if (AllCateg == AllowedCategory.motore)
        {
            data.MotorType = index;
        }
    }
}

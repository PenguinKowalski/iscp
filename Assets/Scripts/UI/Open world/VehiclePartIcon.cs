using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VehiclePartIcon : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]MaterialQuantity partInformation;
    [SerializeField]Image icon;
    [SerializeField]Sprite originalIcon;
    [SerializeField] Inventory_scroll inventoryToAddTo;
    Transform originalParent = null;
    int originalIndex;
    bool returnToOriginal = false, isInDropZone = false, isRubbishBin = false;
    GameObject placeholder;
    [SerializeField] GameObject placeholderPrefab, recycleIcon;
    Canvas canvas;
    InventoryDropZone assignedSlot;
    VehiclePartSlot thisSlot;
    VehicleData carData;
    [SerializeField] CraftingSystem crafting;
    public MaterialQuantity PartInformation { get => partInformation; set => partInformation = value; }
    public Image Icon { get => icon; set => icon = value; }
    public Inventory_scroll InventoryToAddTo { get => inventoryToAddTo; set => inventoryToAddTo = value; }

    // Start is called before the first frame update
    void Start()
    {
        thisSlot = GetComponentInParent<VehiclePartSlot>();
        canvas = GetComponentInParent<Canvas>();
        GameObject go = Instantiate(placeholderPrefab, gameObject.transform.parent.gameObject.transform);
        if (placeholder == null)
        {
            placeholder = go;
            placeholder.SetActive(false);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(partInformation != null)
        {
            originalParent = this.transform.parent;
            originalIndex = this.transform.GetSiblingIndex();
            this.transform.SetParent(canvas.transform);
            placeholder.transform.SetParent(originalParent);
            placeholder.transform.SetSiblingIndex(originalIndex);
            this.GetComponent<CanvasGroup>().blocksRaycasts = false;
            recycleIcon.SetActive(true);
        }  
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(partInformation != null)
        {
            this.transform.position = eventData.position;
            var parent = placeholder.transform.parent;
            if (parent != null && !returnToOriginal)
            {
                int newSiblingIndex = parent.childCount;

                for (int i = 0; i < parent.childCount; i++)
                {
                    if (this.transform.position.x < parent.GetChild(i).position.x)
                    {

                        newSiblingIndex = i;

                        if (placeholder.transform.GetSiblingIndex() < newSiblingIndex)
                            newSiblingIndex--;

                        break;
                    }
                }
                placeholder.transform.SetSiblingIndex(newSiblingIndex);
            }
        }
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if(partInformation != null)
        {
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            if (isInDropZone)
            {
                if(assignedSlot != null)
                {
                    inventoryToAddTo.AddMaterial(partInformation, CarListOpenWorld.Instance.PlayerInventory, null);
                    thisSlot.ThisPart = null;
                    icon.sprite = originalIcon;
                    partInformation = null;
                    thisSlot.HasItemAssigned = false;
                    thisSlot.PreviousPart = null;
                    this.gameObject.SetActive(false);
                }
                if (isRubbishBin)
                {
                    for (int j = 0; j < OpenWorld._instance.PossibleRecipes.Count; j++)
                    {
                        for (int k = 0; k < OpenWorld._instance.PossibleRecipes[j].Profit.Count; k++)
                        {
                            if (OpenWorld._instance.PossibleRecipes[j].Profit[k].Index == partInformation.Index)
                            {
                                crafting.RecicleItem(partInformation);
                                thisSlot.ThisPart = null;
                                icon.sprite = originalIcon;
                                partInformation = null;
                                this.gameObject.SetActive(false);
                            }
                        }
                    }
                }
                thisSlot.CheckCategory(0);
            }

            this.transform.SetParent(placeholder.transform.parent);
            this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
            placeholder.transform.SetParent(canvas.transform);
            placeholder.SetActive(false);
            recycleIcon.SetActive(false);
        }
    }

    public void EnteringDropZone(Transform dropZone)
    {
      
        returnToOriginal = false;
        isInDropZone = true;
        placeholder.transform.SetParent(originalParent);
        placeholder.transform.SetSiblingIndex(originalIndex);
        assignedSlot = dropZone.gameObject.GetComponent<InventoryDropZone>();
        RubbishBinDropZone isRubbish = dropZone.gameObject.GetComponent<RubbishBinDropZone>();
        if (isRubbish != null)
        {
            isRubbishBin = true;
        }
    }

    public void LeavingDropZone(Transform dropZone)
    {
        placeholder.transform.SetParent(originalParent);
        placeholder.transform.SetSiblingIndex(originalIndex);
        returnToOriginal = true;
        isInDropZone = false;
        assignedSlot = null;
        isRubbishBin = false;
    }
}

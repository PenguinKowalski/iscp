using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class QuantitySlider : MonoBehaviour
{
    [SerializeField] Slider qntySlider;
    float sliderCurrentValue;
    [SerializeField] Button ok, annulla;
    [SerializeField] TextMeshProUGUI price_txt, quantity_txt;
    int itemPrice;
    public float SliderCurrentValue { get => sliderCurrentValue; set => sliderCurrentValue = value; }
    public Button Ok { get => ok; set => ok = value; }
    public Button Annulla { get => annulla; set => annulla = value; }
    public Slider QntySlider { get => qntySlider; set => qntySlider = value; }


    // Start is called before the first frame update
    void Start()
    {
        qntySlider.value = qntySlider.maxValue;
    }

    // Update is called once per frame
    void Update()
    {
        quantity_txt.text = "" + qntySlider.value;
        price_txt.text = "" + (qntySlider.value * itemPrice);
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Destroy(this.gameObject);
        }
    }

    public void SetItemPrice(MaterialQuantity material)
    {
        CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(material.Index);
        itemPrice = materialInformation.Price;
    }
    public void AssignValues(MaterialQuantity quantity)
    {
        qntySlider.maxValue = quantity.Quantity;
    }
}

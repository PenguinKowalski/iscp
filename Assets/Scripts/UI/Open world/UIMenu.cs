using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class UIMenu : MonoBehaviour
{
    [SerializeField] List<MaterialQuantity> inventarioGiocatore = new List<MaterialQuantity>();
    [SerializeField] List<Recipe> listaRecipe = new List<Recipe>();
    [SerializeField] Inventory_scroll inventarioAuto, inventarioGenerale;
    [SerializeField] SlotAuto slotAuto;
    bool carsSet = false;
    [SerializeField] ItemDescription descriptionPanel;

    public ItemDescription DescriptionPanel { get => descriptionPanel; set => descriptionPanel = value; }

    // Start is called before the first frame update
    /*void Start()
    {
        Open();
    }*/
    public void DisableMenu()
    {
        for(int i = 0; i < inventarioAuto.ListaElementiLista.Count; i++)
        {
            Destroy(inventarioAuto.ListaElementiLista[i].gameObject);
        }
        for (int i = 0; i < inventarioGenerale.ListaElementiLista.Count; i++)
        {
            Destroy(inventarioGenerale.ListaElementiLista[i].gameObject);
        }
        inventarioAuto.ListaElementiLista.Clear();
        inventarioGenerale.ListaElementiLista.Clear();
        descriptionPanel.gameObject.SetActive(false);
        slotAuto.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void CarOnClick(MaterialQuantity car, VehicleData data)
    {
        slotAuto.gameObject.SetActive(true);
        slotAuto.ChangeInformation(car, data);
    }
    public void ItemOnClick()
    {
        
    }
    public void Open()
    {
        inventarioAuto.SpawnList(CarListOpenWorld.Instance.PlayerInventory.ListaMateriali, CarListOpenWorld.Instance.PlayerInventory);
        inventarioGenerale.SpawnList(CarListOpenWorld.Instance.PlayerInventory.ListaMateriali, CarListOpenWorld.Instance.PlayerInventory);
    }
}


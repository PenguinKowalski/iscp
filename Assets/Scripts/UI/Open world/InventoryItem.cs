using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class InventoryItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    protected int index, price;
    [SerializeField]protected MaterialQuantity thisMaterial;
    protected CraftingMaterial material;
    [SerializeField] protected Button bt;
    [SerializeField] protected ItemDescription descPanel;
    [SerializeField] protected GameObject descriptionPrefab;
    protected UITrading thisUi;
    protected bool hasExited;
    [SerializeField] protected Image icon;
    [SerializeField] protected TextMeshProUGUI itemName;
    protected AudioSource thisAudio;
    [SerializeField] protected List<AudioClip> hoverSounds = new List<AudioClip>();
    public int Index { get => index; set => index = value; }
    public MaterialQuantity ThisMaterial { get => thisMaterial; set => thisMaterial = value; }
    public Button Bt { get => bt; set => bt = value; }
    public ItemDescription DescPanel { get => descPanel; set => descPanel = value; }
    public CraftingMaterial Material { get => material; set => material = value; }
    public int Price { get => price; set => price = value; }
    public AudioSource ThisAudio { get => thisAudio; set => thisAudio = value; }

    public virtual void Start()
    {
        thisUi = GetComponentInParent<UITrading>();
        hasExited = false;
    }

    public void ChangeDetails(CraftingMaterial material, MaterialQuantity quantity)
    {
        icon.sprite = material.Icona;
        if (quantity != null)
        {
            if (thisMaterial.Quantity > 1)
            {
                itemName.text = material.Nome + " (" + quantity.Quantity + ")";
            }
            else
            {
                itemName.text = material.Nome;
            }
        }
        else
        {
            itemName.text = material.Nome;
        }
    }

    public virtual void Update()
    {
        if (descriptionPrefab != null)
        {
            if (thisUi.SliderPopUp == null && descPanel != null && hasExited)
            {
                thisUi.DescriptionPanel.gameObject.SetActive(false);
            }
        }
    }
    public virtual void OnPointerEnter(PointerEventData pointerEventData)
    {
       // thisAudio.pitch = Random.Range(0.75f, 1.25f);
//thisAudio.PlayOneShot(hoverSounds[Random.Range(0, hoverSounds.Count)]);
        if (descriptionPrefab != null)
        {
            if (thisUi.SliderPopUp == null && descPanel == null)
            {
                CraftingMaterial materialInformation = CarListOpenWorld.Instance.GetMaterialIndex(thisMaterial.Index);
                thisUi.DescriptionPanel.ChangeDetails(materialInformation, price);
                thisUi.DescriptionPanel.gameObject.SetActive(true);

                hasExited = false;
            }
        }
    }
    public virtual void OnPointerExit(PointerEventData pointerEventData)
    {
        if(descriptionPrefab != null)
        {
            if (thisUi.SliderPopUp == null)
            {
                thisUi.DescriptionPanel.gameObject.SetActive(false);
            }
            hasExited = true;
        }          
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UpdateCraftingUI : MonoBehaviour
{
    [SerializeField] List<TMP_Text> craftingMaterialQuantity = new List<TMP_Text>();
    [SerializeField] Image imageInfo;
    [SerializeField] TMP_Text Info1, info2, description;

    [SerializeField] CraftingSystem craftingSystem;
    [SerializeField] Image craftingButton;

    // Update is called once per frame
    void Update()
    {
        if(craftingSystem.MaterialToCraft != null)
        {
            CraftingMaterial mat = CarListOpenWorld.Instance.GetMaterialIndex(craftingSystem.MaterialToCraft.Profit[0].Index);

            imageInfo.sprite = mat.Icona;
            description.text = mat.Descrizione;

            //Info1.text = mat.ListaStatistiche[0];
            //info2.text = mat.ListaStatistiche[1];
        }

        if(craftingSystem.CanCraft == true)
        {
            craftingButton.color = Color.green;
        }
        else
        {
            craftingButton.color = Color.red;
        }

        bool b = false;
        bool a = false;
        bool o = false;

        for (int i = 0; i < CarListOpenWorld.Instance.PlayerInventory.ListaMateriali.Count; i++)
        {
            switch (CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[i].Index)
            {
                case 1:
                    craftingMaterialQuantity[0].text = CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[i].Quantity.ToString();
                    b = true;
                    break;
                case 2:
                    craftingMaterialQuantity[1].text = CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[i].Quantity.ToString();
                    a = true;
                    break;
                case 3:
                    craftingMaterialQuantity[2].text = CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[i].Quantity.ToString();
                    o = true;
                    break;
            }
        }
        if(b == false)
        {
            craftingMaterialQuantity[0].text = "0";
        }
        if (a == false)
        {
            craftingMaterialQuantity[1].text = "0";
        }
        if (o == false)
        {
            craftingMaterialQuantity[2].text = "0";
        }
    }
}

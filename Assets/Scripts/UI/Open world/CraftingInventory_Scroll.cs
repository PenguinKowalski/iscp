using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CraftingInventory_Scroll : Inventory_scroll
{
    [SerializeField] CraftingSystem crafting;

    public override void Start()
    {
        base.Start();
        for(int i = 0; i < OpenWorld._instance.PossibleRecipes.Count; i++)
        {
            SpawnListRecipe(OpenWorld._instance.PossibleRecipes[i]);
        }

    }

    public void SpawnListRecipe(Recipe recipe)
    {
        SpawnObjectInList(null, null, recipe, null);
    }

    public override void SpawnObjectInList(MaterialQuantity material, UITrading trading, Recipe recipe, VehicleData data)
    {
        CraftingRecipeUI item;
        GameObject go = Instantiate(elementoLista, content.transform);        
        item = go.GetComponent<CraftingRecipeUI>();
        
        if(recipe.Profit.Count > 0)
        {
            if (CarListOpenWorld.Instance.GetMaterialIndex(recipe.Profit[0].Index).Icona != null)
            {
                item.Icon.sprite = CarListOpenWorld.Instance.GetMaterialIndex(recipe.Profit[0].Index).Icona;
            }
            if (CarListOpenWorld.Instance.GetMaterialIndex(recipe.Profit[0].Index).Nome != null)
            {
                item.Nome.text = CarListOpenWorld.Instance.GetMaterialIndex(recipe.Profit[0].Index).Nome;
            }
            item.Bt.onClick.AddListener(delegate { itemClickFunctionality(recipe); });
        }

        if(recipe.Cost.Count > 0)
        {
            for (int i = 0; i < recipe.Cost.Count; i++)
            {
                item.Costi[i].text = recipe.Cost[i].Quantity.ToString();
            } 
        }      
    }

    public void itemClickFunctionality(Recipe material)
    {
        crafting.SelectObjectToCraft(material);
        crafting.UpdateCraftingbutton();
    }
}

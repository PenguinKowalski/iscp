using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Marker : MonoBehaviour
{
    Car markedCar;

    public Car MarkedCar { get => markedCar; set => markedCar = value; }

}

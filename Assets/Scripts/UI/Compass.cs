﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour
{
    public GameObject marker;
    public RawImage compassImage;
    GameObject compassReference;
    [SerializeField] List<GameObject> listMarker = new List<GameObject>();
    [SerializeField] List<Marker> listMarkerScript = new List<Marker>();
    // Start is called before the first frame update
    private void Start()
    {
        for(int i = 0; i < Arena._instance.Team2.Count; i++)
        {
            GameObject newMarker = Instantiate(marker, this.transform);
            Marker newMarkerScript = newMarker.GetComponent<Marker>();
            newMarkerScript.MarkedCar = Arena._instance.Team2[i];

            listMarkerScript.Add(newMarkerScript);
            listMarker.Add(newMarker);
        }
        compassReference = Arena._instance.PlayerCamera.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        compassImage.uvRect = new Rect(compassReference.transform.eulerAngles.y / 360f, 0f, 1f, 1f);


        for(int i = 0; i < listMarker.Count; i++)
        {
            listMarker[i].transform.localPosition = new Vector3(400 * GetAngle(Arena._instance.Team2[i].gameObject.transform.position) / -90f, -10f, 0f);
        }

    }

    protected float GetAngle(Vector3 target)
    {
        Vector3 dir = target - compassReference.transform.position;

        float angle = Vector3.SignedAngle(dir, compassReference.transform.forward, new Vector3(0, 1, 0));

        return angle;
    }

    public void UpdatePlayerCarCompass(GameObject _player)
    {
        compassReference = _player;
    }

    public void RemoveMarker(Car car)
    {
        for(int i = 0; i < listMarkerScript.Count; i++)
        {
            if(listMarkerScript[i].MarkedCar == car)
            {
                Destroy(listMarker[i]);
                listMarker.Remove(listMarker[i]);
                listMarkerScript.Remove(listMarkerScript[i]);
                break;
            }
        }
    }

}

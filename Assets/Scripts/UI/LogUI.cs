using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LogUI : MonoBehaviour
{
    [SerializeField] GameObject logPanelPrefab;
    [SerializeField] GameObject logPanelParent;
    [SerializeField] Scrollbar scrollbar;
    ScrollRect scrollRect;

    [SerializeField] List<GameObject> logList = new List<GameObject>();

    private void Start()
    {
        scrollRect = GetComponent<ScrollRect>();
    }

    public void AddLog(string textLog)
    {
        GameObject newLog = Instantiate(logPanelPrefab, logPanelParent.transform);
        TMP_Text logText = newLog.GetComponentInChildren<TMP_Text>();
        logText.text = textLog;
        logList.Add(newLog);
        if(logList.Count >= 5)
        {
            Destroy(logList[0]);
            logList.Remove(logList[0]);
        }

    }
}

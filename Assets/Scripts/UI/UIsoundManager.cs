using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIsoundManager : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler
{
    AudioSource thisAudio;
    [SerializeField] List<AudioClip> hoverSounds = new List<AudioClip>();
    [SerializeField] List<AudioClip> clickSounds = new List<AudioClip>();

    public void OnPointerClick(PointerEventData eventData)
    {
        thisAudio.pitch = Random.Range(0.5f, 1.5f);
        thisAudio.PlayOneShot(clickSounds[Random.Range(0, clickSounds.Count)]);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        thisAudio.pitch = Random.Range(0.5f, 1.5f);
        thisAudio.PlayOneShot(hoverSounds[Random.Range(0, hoverSounds.Count)]);
    }

    // Start is called before the first frame update
    void Start()
    {
        thisAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

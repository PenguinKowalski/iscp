using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsCanvasManager : MonoBehaviour
{
    public static ControlsCanvasManager _controls;
    // Start is called before the first frame update
    private void Awake()
    {
        if(_controls == null)
        {
            _controls = this;
        }
        DontDestroyOnLoad(this);
    }
    void Start()
    {
        gameObject.SetActive(false);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHud : MonoBehaviour
{
    [SerializeField] Slider healtSlider;  
    [SerializeField] TMP_Text playerAmmo; 
    [SerializeField] TMP_Text playerSpeed;
    [SerializeField] GameObject image;
    Image imageScript;
                                          

    // Start is called before the first frame update
    void Start()
    {
        imageScript = image.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Arena._instance.PlayerWeapon.CurrentAmmo > 0)
        {
            playerAmmo.text = Arena._instance.PlayerWeapon.CurrentAmmo.ToString();
            image.SetActive(false);
        }
        else if(Arena._instance.PlayerWeapon.CurrentAmmo <= 0)
        {
            image.SetActive(true);
            imageScript.sprite = Arena._instance.PlayerCar.WeaponImage;
            playerAmmo.text = "";
        }
        playerSpeed.text = (Arena._instance.PlayerCar.Speed * 3.6f).ToString("f0");

        healtSlider.value = Arena._instance.PlayerCar.HealthPerc;
    }

    
}

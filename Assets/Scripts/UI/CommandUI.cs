using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommandUI : MonoBehaviour
{
    [SerializeField] List<Image> buttonGroup = new List<Image>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Arena._instance.PlayerCar.IsSelectingGroup == true)
        {
            switch (Arena._instance.PlayerCar.SelectedGroup)
            {
                case Weapon.WeaponType._long:
                    buttonGroup[0].color = Color.red;
                    break;
                case Weapon.WeaponType._medium:
                    buttonGroup[1].color = Color.red;
                    break;
                case Weapon.WeaponType._melee:
                    buttonGroup[2].color = Color.red;
                    break;
                case Weapon.WeaponType._short:
                    buttonGroup[3].color = Color.red;
                    break;
            }    
        }
        else if(Arena._instance.PlayerCar.IsSelectingGroup == false)
        {
            switch (Arena._instance.PlayerCar.SelectedGroup)
            {
                case Weapon.WeaponType._long:
                    buttonGroup[0].color = Color.white;
                    break;
                case Weapon.WeaponType._medium:
                    buttonGroup[1].color = Color.white;
                    break;
                case Weapon.WeaponType._melee:
                    buttonGroup[2].color = Color.white;
                    break;
                case Weapon.WeaponType._short:
                    buttonGroup[3].color = Color.white;
                    break;
            }
        }
    }
}

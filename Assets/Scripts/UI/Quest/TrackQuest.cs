using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackQuest : MonoBehaviour
{
    QuestData thisQuest;
    [SerializeField] QuestScroll questScroll;

    public QuestData ThisQuest { get => thisQuest; set => thisQuest = value; }

    public void ReOrderList()
    {
        CarListOpenWorld.Instance.TrakNewQuest(thisQuest.QuestID);
         for(int i = 0; i < questScroll.QuestList.Count; i++)
         {
             if(questScroll.QuestList[i].ThisQuestData != null)
             {
                questScroll.QuestList[i].ChangeGraphics(false);
             }   
         }
    }
}

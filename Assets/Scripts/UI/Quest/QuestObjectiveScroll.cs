using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestObjectiveScroll : MonoBehaviour
{
    [SerializeField] protected ScrollRect scrollView;
    [SerializeField] protected Button up, down;
    [SerializeField] protected GameObject content;
    [SerializeField] protected List<TextMeshProUGUI> objectiveList = new List<TextMeshProUGUI>();
    [SerializeField] protected int minElementsPerScroll;
    [SerializeField] protected GameObject detailText;
    [SerializeField] protected int index;
    public List<TextMeshProUGUI> ObjectiveList { get => objectiveList; set => objectiveList = value; }
    public int Index { get => index; set => index = value; }

    // Start is called before the first frame update
    public virtual void Start()
    {
        this.gameObject.SetActive(false);
        down.onClick.AddListener(delegate { ButtonDown_OnClick(scrollView, objectiveList.Count - minElementsPerScroll); });
        up.onClick.AddListener(delegate { ButtonUp_OnClick(scrollView, objectiveList.Count - minElementsPerScroll); });
    }
    public virtual void SpawnObjectInList(string stepId, bool isLatest)
    {
        TextMeshProUGUI objectiveText;
        GameObject go = Instantiate(detailText, content.transform);
        objectiveText = go.GetComponent<TextMeshProUGUI>();
        objectiveText.text = stepId;
        if (!isLatest)
        {
            objectiveText.color = Color.gray;
        }
        else
        {
            objectiveText.color = Color.white;
        }
        objectiveList.Add(objectiveText);
    }
    public virtual void SpawnList(QuestData quest)
    {
        for (int i = 0; i < quest.OldSteps.Count; i++)
        {
            SpawnObjectInList(quest.OldSteps[i].StepID, false);  
        }
        SpawnObjectInList(quest.Step.StepID, true);
    }
    public void ClearList()
    {
        for (int i = 0; i < objectiveList.Count; i++)
        {
            Destroy(objectiveList[i].gameObject);
        }
        objectiveList.Clear();
    }

    void Update()
    {
        if (objectiveList.Count > minElementsPerScroll)
        {
            if (scrollView.verticalNormalizedPosition >= 0.985f)
            {
                up.gameObject.SetActive(false);
                down.gameObject.SetActive(true);
            }
            else if (scrollView.verticalNormalizedPosition <= 0.015f)
            {
                down.gameObject.SetActive(false);
            }
            else
            {
                down.gameObject.SetActive(true);
                up.gameObject.SetActive(true);
            }
        }
        else
        {
            down.gameObject.SetActive(false);
            up.gameObject.SetActive(false);
        }
    }

    protected void ButtonDown_OnClick(ScrollRect scrollRect, float listLength)
    {
        float calcolo = 1f / (float)listLength;
        scrollRect.verticalNormalizedPosition -= calcolo;
        Canvas.ForceUpdateCanvases();
    }
    protected void ButtonUp_OnClick(ScrollRect scrollRect, float listLength)
    {
        float calcolo = 1f / (float)listLength;
        scrollRect.verticalNormalizedPosition += calcolo;
        Canvas.ForceUpdateCanvases();
    }

}

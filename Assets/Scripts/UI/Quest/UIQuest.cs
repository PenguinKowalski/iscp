using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIQuest : MonoBehaviour
{

    [SerializeField] QuestScroll questScroll;

    private void OnEnable()
    {
        SwitchUIOn();
    }
    public void SwitchUIOn()
    {
        questScroll.SpawnList(CarListOpenWorld.Instance.ActiveQuests);
    }
}

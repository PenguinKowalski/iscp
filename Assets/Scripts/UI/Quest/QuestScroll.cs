using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestScroll : MonoBehaviour
{
    [SerializeField] protected ScrollRect scrollView;
    [SerializeField] protected Button up, down;
    [SerializeField] protected GameObject content;
    [SerializeField] protected List<QuestButton> questList = new List<QuestButton>();
    [SerializeField] protected int minElementsPerScroll;
    [SerializeField] protected GameObject questButtonPrefab;
    [SerializeField] protected int index;
    [SerializeField] QuestDetails questDetails;
    DialogueManager dialogueManager;
    public List<QuestButton> QuestList { get => questList; set => questList = value; }
    public int Index { get => index; set => index = value; }

    // Start is called before the first frame update
    private void OnDisable()
    {
        ClearList();
    }
    private void OnEnable()
    {
        questDetails.gameObject.SetActive(false);
        for (int i = 0; i < questList.Count; i++)
        {
            if(questList[i].ThisQuestData != null)
            {
                if (questList[i].ThisQuestData.IsActive)
                {
                    questDetails.gameObject.SetActive(true);
                    return;
                }
            }
        }
    }

    public virtual void Start()
    {
        dialogueManager = DialogueManager._manager;
        down.onClick.AddListener(delegate { ButtonDown_OnClick(scrollView, questList.Count - minElementsPerScroll); });
        up.onClick.AddListener(delegate { ButtonUp_OnClick(scrollView, questList.Count - minElementsPerScroll); });
    }
    public virtual void SpawnObjectInList(Quest quest, QuestData questData, bool failedQuest)
    {
        QuestButton questButton;
        GameObject go = Instantiate(questButtonPrefab, content.transform);
        questButton = go.GetComponent<QuestButton>();
        questButton.AssignButtonData(questData, quest, failedQuest);
        questList.Add(questButton);
        questButton.Bt.onClick.AddListener(delegate { ItemClickFunctionality(quest.ActorImage, quest.TitleID, quest.ActorName, quest.QuestDescription, questData, quest); });
    }
    public virtual void SpawnList(List<QuestData> questDataList)
    {
        for (int i = 0; i < questDataList.Count; i++)
        {
            for (int j = 0; j < CarListOpenWorld.Instance.PossibleQuests.Count; j++)
            {
                if (questDataList[i].QuestID == CarListOpenWorld.Instance.PossibleQuests[j].QuestID && questDataList[i].IsActive)
                {
                    SpawnObjectInList(CarListOpenWorld.Instance.PossibleQuests[j], questDataList[i], false);
                }
            }
        }
        for (int i = 0; i < CarListOpenWorld.Instance.CompletedQuests.Count; i++)
        {
            SpawnObjectInList(CarListOpenWorld.Instance.CompletedQuests[i], null, false);
        }
        for (int i = 0; i < CarListOpenWorld.Instance.FailedQuests.Count; i++)
        {
            SpawnObjectInList(CarListOpenWorld.Instance.FailedQuests[i], null, true);
        }
    }

    public virtual void ClearList()
    {
        for(int i = 0; i < questList.Count; i++)
        {
            Destroy(questList[i].gameObject);
        }
        questList.Clear();
    }

    void ItemClickFunctionality(Sprite actorImage, string questNameText, string questGiverNameText, string questDescriptionText, QuestData questData, Quest quest )
    {
        questDetails.AssignDetailsDetails(actorImage,  questNameText, questGiverNameText, questDescriptionText, questData, quest);
    }

    void Update()
    {
        if (questList.Count > minElementsPerScroll)
        {
            if (scrollView.verticalNormalizedPosition >= 0.985f)
            {
                up.gameObject.SetActive(false);
                down.gameObject.SetActive(true);
            }
            else if (scrollView.verticalNormalizedPosition <= 0.015f)
            {
                down.gameObject.SetActive(false);
            }
            else
            {
                down.gameObject.SetActive(true);
                up.gameObject.SetActive(true);
            }
        }
        else
        {
            down.gameObject.SetActive(false);
            up.gameObject.SetActive(false);
        }
    }

    protected void ButtonDown_OnClick(ScrollRect scrollRect, float listLength)
    {
        float calcolo = 1f / (float)listLength;
        scrollRect.verticalNormalizedPosition -= calcolo;
        Canvas.ForceUpdateCanvases();
    }
    protected void ButtonUp_OnClick(ScrollRect scrollRect, float listLength)
    {
        float calcolo = 1f / (float)listLength;
        scrollRect.verticalNormalizedPosition += calcolo;
        Canvas.ForceUpdateCanvases();
    }
}

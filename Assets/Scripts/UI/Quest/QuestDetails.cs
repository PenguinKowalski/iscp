using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestDetails : MonoBehaviour
{
    [SerializeField] Image actorIcon;
    [SerializeField] TextMeshProUGUI questName, questDescription, questGiverName;
    [SerializeField] QuestObjectiveScroll objectiveScroll;
    [SerializeField] TrackQuest questTracker;

    public void AssignDetailsDetails(Sprite actorImage, string questNameText, string questGiverNameText, string questDescriptionText, QuestData questData, Quest quest)
    {
        objectiveScroll.ClearList();
        if (questData == null)
        {
            objectiveScroll.gameObject.SetActive(false);
        }
        else
        {
            objectiveScroll.gameObject.SetActive(true);
            questTracker.ThisQuest = questData;
            objectiveScroll.SpawnList(questData);
        }
        actorIcon.sprite = actorImage;
        questName.text = questNameText;
        questGiverName.text = questGiverNameText;
        questDescription.text = questDescriptionText;
        if (!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
    }
}

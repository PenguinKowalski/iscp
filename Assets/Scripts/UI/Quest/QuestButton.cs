using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestButton : MonoBehaviour
{
    QuestData thisQuestData;
    Quest thisQuest;
    [SerializeField] TextMeshProUGUI questName;
    [SerializeField] Image progressIcon, thisBackground;
    [SerializeField] Sprite active, failed, completed;
    [SerializeField] Button bt;


    public QuestData ThisQuestData { get => thisQuestData; set => thisQuestData = value; }
    public Button Bt { get => bt; set => bt = value; }

    public void AssignButtonData(QuestData questData, Quest quest, bool failedQuest)
    {
        thisQuest = quest;
        thisQuestData = questData;
        questName.text = quest.TitleID;
        ChangeGraphics(failedQuest);
    }
    public void ChangeGraphics(bool failedQuest)
    {

        if (thisQuestData != null)
        {
            if (thisQuestData.QuestID.Equals(CarListOpenWorld.Instance.TrakedQuestID))
            {
                gameObject.transform.SetSiblingIndex(0);
                thisBackground.color = Color.green;
            }
            else
            {
                thisBackground.color = Color.yellow;
            }
        }
        else
        {
            thisBackground.color = Color.gray;
            if (failedQuest)
            {
                progressIcon.sprite = failed;
            }
            else
            {
                progressIcon.sprite = completed;
            }
        }

    }
}

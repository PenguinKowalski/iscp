using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProfitUIItem : MonoBehaviour
{
    [SerializeField] Image profitIcon;
    [SerializeField] TMP_Text profitName;
    [SerializeField] TMP_Text profitQuantity;

    public Image ProfitIcon { get => profitIcon; set => profitIcon = value; }
    public TMP_Text ProfitName { get => profitName; set => profitName = value; }
    public TMP_Text ProfitQuantity { get => profitQuantity; set => profitQuantity = value; }
}

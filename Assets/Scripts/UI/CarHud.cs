using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CarHud : MonoBehaviour
{
    [SerializeField] Slider healthSlider;
    [SerializeField] Image statusIconImage;
    [SerializeField] Image groupIconImage;
    [SerializeField] TMP_Text carNameText;

    [SerializeField] Image BackGroundIconImage;
    Car car;

    [SerializeField] List<Sprite> statusIcon = new List<Sprite>();
    [SerializeField] List<Sprite> groupIcon = new List<Sprite>();

    public Car Car { get => car; set => car = value; }

    // Update is called once per frame
    void Update()
    {
        UpdatecarHud();
    }


    public void UpdatecarHud()
    {
        healthSlider.value = car.HealthPerc;
        carNameText.text = car.CarName;

        if(car.CarID == Arena._instance.PlayerCar.CarID)
        {
            BackGroundIconImage.color = Color.green;
        }
        else
        {
            BackGroundIconImage.color = Color.white;
        }
        
        statusIconImage.sprite = statusIcon[car.Status];


        switch(car.Weapon.WpnType1)
        {
            case Weapon.WeaponType._long:
                groupIconImage.sprite = groupIcon[0];
                break;
            case Weapon.WeaponType._medium:
                groupIconImage.sprite = groupIcon[1];
                break;
            case Weapon.WeaponType._melee:
                groupIconImage.sprite = groupIcon[2];
                break;
            case Weapon.WeaponType._short:
                groupIconImage.sprite = groupIcon[3];
                break;
        }    
    }
}

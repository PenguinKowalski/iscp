using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class OpenWorldUIManager : MonoBehaviour
{
    [SerializeField] List<GameObject> subMenus = new List<GameObject>();
    [SerializeField] EventPopUp eventPopUp;
    [SerializeField] Image bt;

    [SerializeField] TMP_Text estimatedConsumptionTarget;
    [SerializeField] TMP_Text estimatedConsumptionCursor;

    [SerializeField] TMP_Text gasLiters;

    public List<GameObject> SubMenus { get => subMenus; set => subMenus = value; }

    private void Update()
    {
        if(OpenWorld._instance.PlayerScript.InteractableData.CarInfo.Count > 0)
        {
            bt.color = Color.green;
        }
        else
        {
            bt.color = Color.red;
        }
        gasLiters.text = OpenWorld._instance.Gas.ToString() + " L";
        ShowEstimatedGasConsumption();
    }


    public void ActivateUI(InteractableObject enemy, int index)
    {
        for (int i=0; i< subMenus.Count; i++)
        {
            if (i==index)
                subMenus[i].SetActive(true);
            else
                subMenus[i].SetActive(false);
        }

        if(index == 5 && enemy!=null)
        {
            //EventPopUp(enemy);
            DialogueManager._manager.AssignDialogue(enemy.InteractableData.Description, null,enemy);            
        }
    }
    /*
    public void carSelection()
    {
        subMenus[3].SetActive(true);

    }
    */
    public void DeactivateUI(int i)
    {
        subMenus[i].SetActive(false);
    }


    void ShowEstimatedGasConsumption()
    {
        if(estimatedConsumptionCursor != null && estimatedConsumptionTarget != null)
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                Vector3 cursorPos = new Vector3 (0,0,0);

                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Debug.DrawRay(ray.origin, ray.direction * 1000f, Color.magenta, 30f);
                if (Physics.Raycast(ray, out RaycastHit hit, 1000f))
                {
                    cursorPos = hit.point;

                }



                int hint = (int)OpenWorld._instance.CalculateGasConsumption(cursorPos);
                estimatedConsumptionCursor.text = hint.ToString();
                int hint2 = (int)OpenWorld._instance.CalculateGasConsumption(OpenWorld._instance.PlayerConvoy.TargetDest.transform.position);
                estimatedConsumptionTarget.text = hint2.ToString();
            }
            else
            {
                estimatedConsumptionCursor.text = "";
                estimatedConsumptionTarget.text = "";
            }
        }
    }


    void EventPopUp(InteractableObject enemy)
    {
        List<UnityAction> listaEventi = new List<UnityAction>();

        for (int i = 0; i < enemy.InteractableData.ListaBottoni.Count; i++)
        {
            switch (enemy.InteractableData.ListaBottoni[i])
            {
                case ("attack"):
                    listaEventi.Add(OpenWorld._instance.SelectCars);
                    break;
                case ("escape"):
                    listaEventi.Add(OpenWorld._instance.Escape);
                    break;
                case ("trade"):
                    listaEventi.Add(OpenWorld._instance.StartTrade);
                    break;
                case ("dialogue"):
                    listaEventi.Add(OpenWorld._instance.StartDialogue);
                    break;
                case ("pay"):
                    listaEventi.Add(OpenWorld._instance.GiveMoney);
                    break;
                case ("back"):
                    listaEventi.Add(OpenWorld._instance.Back);
                    break;

            }
        }

        eventPopUp.SpawnButtons(enemy.InteractableData.Description, enemy.InteractableData.ListaBottoni, listaEventi);
    }
}


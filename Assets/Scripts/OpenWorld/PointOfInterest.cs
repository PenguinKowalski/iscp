using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterest : InteractableObject
{
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        if(InteractableData.ObjectType != IntType.persistent)
        {
            RandomizeType();
        }
    }

    // Update is called once per frameWW
    protected override void Update()
    {
        base.Update();
        ManageFogOfWar();
    }

    void RandomizeType()
    {
        InteractableData.Description = possibiliDescrizioni[Random.Range(0, possibiliDescrizioni.Count)];
        int i = Random.Range(0, 3);
        switch (i)
        {
            case 0:
                InteractableData.PointType = IntPointType.CrashSite;
                break;
            case 1:
                InteractableData.PointType = IntPointType.Outpost;
                break;
            case 2:
                InteractableData.PointType = IntPointType.HiddenStash;
                break;

        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum IntType { persistent, dynamic, player};
public enum IntPointType { HiddenStash, CrashSite, Outpost, TradingOutpost, shop, aggressive, passive};


[System.Serializable]
public class InteractableObject : MonoBehaviour
{

    [SerializeField] List<string> sceneToLoad = new List<string>();
    [SerializeField] ObjectInteractableVariables interactableData;
    [SerializeField] protected GameObject view;
    [SerializeField] List<GameObject> interactableObjView = new List<GameObject>();
    float rangeEvent;
    Image background;
    [SerializeField] GameObject QuestPointer, ShopPointer;
    
    [SerializeField] protected List<string> possibiliDescrizioni = new List<string>();
    [SerializeField] GameObject targetDest;

    public List<string> SceneToLoad { get => sceneToLoad; set => sceneToLoad = value; }
    public GameObject View { get => view; set => view = value; }
    public ObjectInteractableVariables InteractableData { get => interactableData; set => interactableData = value; }
    public GameObject TargetDest { get => targetDest; set => targetDest = value; }


    virtual protected void Start()
    {
        if (interactableData.Ricompense.Count <= 0)
        {
            RandomizeRewards();
        }
        RandomizeShopInventory();
        SetView();
    }

    virtual protected void Update()
    {
        interactableData.ObjectPosition = gameObject.transform.position;

        SetPointers();
    }

    public void ManageFogOfWar()
    {
        if(interactableData.CanDisappear && Vector3.Distance(transform.position, OpenWorld._instance.Player.transform.position) > OpenWorld._instance.FogRadius)
        {
            view.SetActive(false);
        }
        else if(interactableData.CanDisappear && Vector3.Distance(transform.position, OpenWorld._instance.Player.transform.position) <= OpenWorld._instance.FogRadius)
        {
            view.SetActive(true);
        }
    }



    public void RandomizeVehicles(int difficulty)
    {
        for (int i = 0; i < difficulty; i++)
        {
            VehicleData vehicle = new VehicleData();
            vehicle.CarName = "nemico" + i.ToString();
            vehicle.VehicleType = 5;
            vehicle.Team = 1;
            vehicle.WeaponType = 8;
            if (interactableData.PointType == IntPointType.shop || interactableData.PointType == IntPointType.aggressive || interactableData.PointType == IntPointType.passive)
            {
                vehicle.IsConvoy = true;
            }
            vehicle.HealthPerc = 100;
            vehicle.ArmorType = 19;
            interactableData.CarInfo.Add(vehicle);
        }
    }

    public string RandomizeID()
    {
        const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";
        string carID = "";

        for (int i = 0; i < 10; i++)
        {
            carID += glyphs[Random.Range(0, glyphs.Length)];
        }

        return carID;
    }

    public void AddButtons()
    {
        interactableData.ListaBottoni.Clear();

        if(interactableData.ListaBottoni.Count == 0)
        {
            if (InteractableData.PointType == IntPointType.shop)
            {
                interactableData.ListaBottoni.Add("attack");
                interactableData.ListaBottoni.Add("trade");
                interactableData.ListaBottoni.Add("back");
            }
            else if (InteractableData.PointType == IntPointType.aggressive)
            {
                interactableData.ListaBottoni.Add("attack");
                interactableData.ListaBottoni.Add("escape");
            }
            else if (InteractableData.PointType == IntPointType.passive)
            {
                interactableData.ListaBottoni.Add("attack");
                interactableData.ListaBottoni.Add("back");
            }
            else if (InteractableData.PointType == IntPointType.Outpost)
            {
                interactableData.ListaBottoni.Add("attack");
                interactableData.ListaBottoni.Add("back");
            }
            else if (InteractableData.PointType == IntPointType.HiddenStash)
            {
                interactableData.ListaBottoni.Add("attack");
                interactableData.ListaBottoni.Add("escape");
            }
            else if (InteractableData.PointType == IntPointType.CrashSite)
            {
                interactableData.ListaBottoni.Add("attack");
                interactableData.ListaBottoni.Add("back");
            }
            else if (InteractableData.PointType == IntPointType.TradingOutpost)
            {
                interactableData.ListaBottoni.Add("attack");
                interactableData.ListaBottoni.Add("back");
                interactableData.ListaBottoni.Add("trade");
            }
        }
        if (TryGetComponent(out QuestTarget targ) == true)
        {
            if (targ.IsBounty == false)
            {
                interactableData.ListaBottoni.Add("dialogue");
            }
        }
    }

    void RandomizeRewards()
    {
        for (int i = 0; i < CarListOpenWorld.Instance.ListaMateriali.Count; i++)
        {
            if (CarListOpenWorld.Instance.ListaMateriali[i].ItemCategory == CraftingMaterial.Category.materiale)
            {
                switch (CarListOpenWorld.Instance.ListaMateriali[i].Tier)
                {
                    case 0:
                        int quantity = Random.Range(0, 100);
                        MaterialQuantity mat = new MaterialQuantity(quantity, CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex, null, false);
                        interactableData.Ricompense.Add(mat);
                        break;
                    case 1:
                        int quantity1 = Random.Range(0, 50);
                        MaterialQuantity mat1 = new MaterialQuantity(quantity1, CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex, null, false);
                        interactableData.Ricompense.Add(mat1);
                        break;
                    case 2:
                        int quantity2 = Random.Range(0, 10);
                        MaterialQuantity mat2 = new MaterialQuantity(quantity2, CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex, null, false);
                        interactableData.Ricompense.Add(mat2);
                        break;
                }
            }
            else if (CarListOpenWorld.Instance.ListaMateriali[i].ItemCategory == CraftingMaterial.Category.benzina)
            {
                int quantity = Random.Range(0, 200);
                MaterialQuantity mat = new MaterialQuantity(quantity, CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex, null, false);
                interactableData.Ricompense.Add(mat);
            }
        }
    }

    void SetPointers()
    {
        if(ShopPointer != null || QuestPointer != null)
        {
            if (interactableData.PointType == IntPointType.TradingOutpost || interactableData.PointType == IntPointType.shop)
            {
                ShopPointer.SetActive(true);
            }
            else
            {
                ShopPointer.SetActive(false);
            }
            if (interactableData.Quests.Count > 0)
            {
                QuestPointer.SetActive(true);
            }
            else
            {
                QuestPointer.SetActive(false);
            }
        }
    }

    public void SetQuest()
    {
        /*
        if (interactableData.Quests.Count > 0)
        {
            if (CarListOpenWorld.Instance.GetQuestData(interactableData.QuestID) != null)
            {
                //Debug.Log(interactableData.HasQuest + " " + this);
                CarListOpenWorld.Instance.GetQuest(interactableData.QuestID).ResumeQuest(CarListOpenWorld.Instance.GetQuestData(interactableData.QuestID).StepID);
            }
            else
            {
                CarListOpenWorld.Instance.GetQuest(interactableData.QuestID).AcceptQuest();
            }
        }
        */
    }

    public void AddQuest(QuestData quest)
    {
        if (!InteractableData.Quests.Contains(quest))
        {
            InteractableData.Quests.Add(quest);
        }
    }

    public void RemoveQuest(QuestData quest)
    {
        if (InteractableData.Quests.Contains(quest))
        {
            InteractableData.Quests.Remove(quest);
        }
    }
    void SetView()
    {
        if(interactableObjView.Count > 0)
        {
            IntPointType point = interactableData.PointType;
            if (interactableObjView[(int)point] != null)
            {
                interactableObjView[(int)point].SetActive(true);
            }
        }
    }

    protected void RandomizeShopInventory()
    {

        if(InteractableData.InventarioShop.ListaMateriali.Count == 0)
        {
            List<int> assignedMaterials = new List<int>();

            MaterialQuantity benza = new MaterialQuantity(Random.Range(500, 2000), 0, null, false);
            InteractableData.InventarioShop.ListaMateriali.Add(benza);
            assignedMaterials.Add(0);

            int objcount = Random.Range(0, CarListOpenWorld.Instance.ListaMateriali.Count);

            for (int i = 0; i < objcount; i++)
            {
                bool used = false;
                int id = Random.Range(0, CarListOpenWorld.Instance.ListaMateriali.Count);

                for (int k = 0; k < assignedMaterials.Count; k++)
                {
                    if (id == assignedMaterials[k])
                    {
                        used = true;
                        break;
                    }
                }

                if (used == false)
                {
                    if (CarListOpenWorld.Instance.ListaMateriali[id].ItemCategory == CraftingMaterial.Category.veicoli)
                    {
                        MaterialQuantity car = new MaterialQuantity(1, id, null, true);
                        InteractableData.InventarioShop.ListaMateriali.Add(car);
                        VehicleData newCarData = new VehicleData();
                        car.VehicleDataId = newCarData.RandomizeID();
                        newCarData.CarId = car.VehicleDataId;
                        newCarData.VehicleType = car.Index;
                        InteractableData.InventarioShop.ListaDatiVeicoli.Add(newCarData);
                        assignedMaterials.Add(id);
                    }
                    else
                    {
                        MaterialQuantity piece = new MaterialQuantity(Random.Range(1, 10), id, null, false);
                        InteractableData.InventarioShop.ListaMateriali.Add(piece);
                        assignedMaterials.Add(id);
                    }
                }
            }
        }
    }
}

[System.Serializable]
public class ObjectInteractableVariables
{
    [SerializeField] IntType objectType = IntType.persistent;
    [SerializeField] IntPointType pointType = IntPointType.Outpost;
    [SerializeField] Vector3 objectPosition;
    [SerializeField] List<VehicleData> carInfo = new List<VehicleData>();
    [SerializeField] List<string> listaBottoni = new List<string>();
    [SerializeField] string interactableID;
    [SerializeField] bool canDisappear, canInteract;
    [SerializeField] List<MaterialQuantity> ricompense;
    [SerializeField] private string description, aggressive, trade;
    [SerializeField] bool isDead;
    [SerializeField] Vector3 targetDestPosition;
    [SerializeField] OpenWorld.InventarioPlayer inventarioShop = new OpenWorld.InventarioPlayer();
    [SerializeField] List<QuestData> quests;

    public Vector3 ObjectPosition { get => objectPosition; set => objectPosition = value; }
    public IntType ObjectType { get => objectType; set => objectType = value; }
    public List<VehicleData> CarInfo { get => carInfo; set => carInfo = value; }
    public List<string> ListaBottoni { get => listaBottoni; set => listaBottoni = value; }
    public string InteractableID { get => interactableID; set => interactableID = value; }
    public bool CanDisappear { get => canDisappear; set => canDisappear = value; }
    public List<MaterialQuantity> Ricompense { get => ricompense; set => ricompense = value; }
    public bool CanInteract { get => canInteract; set => canInteract = value; }
    public bool IsDead { get => isDead; set => isDead = value; }
    public string Description { get => description; set => description = value; }
    public IntPointType PointType { get => pointType; set => pointType = value; }
    public Vector3 TargetDestPosition { get => targetDestPosition; set => targetDestPosition = value; }
    public OpenWorld.InventarioPlayer InventarioShop { get => inventarioShop; set => inventarioShop = value; }
    public string Trade { get => trade; set => trade = value; }
    public string Aggressive { get => aggressive; set => aggressive = value; }
    public List<QuestData> Quests { get => quests; set => quests = value; }

   
}

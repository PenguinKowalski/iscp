using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CraftingSystem : MonoBehaviour
{
    Recipe materialToCraft;
    [SerializeField] ScrollInventarioMenu scrollInventarioMenu;
    [SerializeField] ScrollAutoMenu scrollAutoMenu;
    bool canCraft;

    public Recipe MaterialToCraft { get => materialToCraft; set => materialToCraft = value; }
    public bool CanCraft { get => canCraft; set => canCraft = value; }

    public void SelectObjectToCraft(Recipe profit)
    {
        materialToCraft = profit;
    }

    public void UpdateCraftingbutton()
    {
        int canCraftInt = 0;

        for (int i = 0; i < materialToCraft.Cost.Count; i++)
        {
            if (materialToCraft.Cost[i].Quantity == 0)
            {
                canCraftInt += 1;
            }
            else
            {
                for (int n = 0; n < CarListOpenWorld.Instance.PlayerInventory.ListaMateriali.Count; n++)
                {
                    if (CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[n].Index == materialToCraft.Cost[i].Index)
                    {
                        if (CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[n].Quantity >= materialToCraft.Cost[i].Quantity)
                        {
                            canCraftInt += 1;
                        }
                    }
                }
            } 
        }

        if (canCraftInt == materialToCraft.Cost.Count)
        {
            canCraft = true;
        }
        else
        {
            canCraft = false;
        }
    }


    public void CarftItem()
    {
        if (canCraft == true)
        {
            

            for (int i = 0; i < materialToCraft.Cost.Count; i++)
            {
                if(materialToCraft.Cost[i].Quantity >= 0)
                {
                    OpenWorld._instance.RemoveItem(materialToCraft.Cost[i], CarListOpenWorld.Instance.PlayerInventory);
                }
            }
            if (materialToCraft.Profit[0].IsVehicle)
            {
                MaterialQuantity material = new MaterialQuantity(materialToCraft.Profit[0].Quantity, materialToCraft.Profit[0].Index, null, true);

                VehicleData data = new VehicleData();
                data.CarName = "Car";
                data.ArmorType = 25;
                data.HealthPerc = 100;
                data.WeaponType = 18;
                data.Team = 0;
                data.MotorType = 22;
                data.CarId = data.RandomizeID();
                data.VehicleType = materialToCraft.Profit[0].Index;

                scrollAutoMenu.AddMaterial(material, CarListOpenWorld.Instance.PlayerInventory, data);
                Debug.Log("crafto Veicolo");
            }
            else
            {
                MaterialQuantity material = new MaterialQuantity(materialToCraft.Profit[0].Quantity, materialToCraft.Profit[0].Index, null, false);

                scrollInventarioMenu.AddMaterial(material, CarListOpenWorld.Instance.PlayerInventory, null);
            }

            UpdateCraftingbutton();
        }
    }



    public void RecicleItem(MaterialQuantity obj)
    {
        for (int i  = 0; i <  OpenWorld._instance.PossibleRecipes.Count; i++)
        {
            if(obj.Index == OpenWorld._instance.PossibleRecipes[i].Profit[0].Index)
            {
                //Debug.Log(OpenWorld._instance.PossibleRecipes[i].Profit);
                for (int n = 0; n < OpenWorld._instance.PossibleRecipes[i].Cost.Count; n++)
                {
                    MaterialQuantity newMat = new MaterialQuantity(OpenWorld._instance.PossibleRecipes[i].Cost[n].Quantity / 2 * obj.Quantity, OpenWorld._instance.PossibleRecipes[i].Cost[n].Index , null, false);
                    //Debug.Log(newMat.Index + " " + newMat.Quantity);
                    OpenWorld._instance.AddItem(newMat, CarListOpenWorld.Instance.PlayerInventory, null);
                }
  
                break;
            }
        }
    }
}

[System.Serializable]
public class Recipe
{
    [SerializeField] List<MaterialQuantity> cost = new List<MaterialQuantity>();

    [SerializeField] List<MaterialQuantity> profit = new List<MaterialQuantity>();

    public List<MaterialQuantity> Profit { get => profit; set => profit = value; }
    public List<MaterialQuantity> Cost { get => cost; set => cost = value; }
}

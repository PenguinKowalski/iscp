using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using UnityEngine.SceneManagement;
using CleverCrow.Fluid.Dialogues;
using CleverCrow.Fluid.Databases;

public class OpenWorld : MonoBehaviour
{
    public static OpenWorld _instance;

    [SerializeField] GameObject location;
    [SerializeField] GameObject convoy;
    [SerializeField] GameObject target;
    [SerializeField] GameObject targetParent;
    [SerializeField] GameObject pointOfInterestParent;
    [SerializeField] float dist1 = 50, dist2 = 100, dist3 = 200;
    [SerializeField] int increment = 2, multiplier = 2, indexApe, indexMustang, indexVan, indexCamion;
    InteractableObject enemyScript;
    [SerializeField] int consumption = 1, gas = 0;
    int indiceBenzina;
    [SerializeField] float radiusMin = 18, radiusMax = 20;
    [SerializeField]int maxRandomPoints = 10, maxRandomConvoys = 10;
    [SerializeField] GameObject player;
    Convoy playerConvoy;
    [SerializeField] CameraOpenWorld playerCameraScript;
    [SerializeField] float fogRadius;
    [SerializeField] InteractableObject playerScript;
    [SerializeField] Camera playerCamera;
    [SerializeField] InteractableObject outOfGas;
    [SerializeField] OpenWorldUIManager openWorldUIManager;
    OpenWorldSoundManager openWorldSoundManager;
    PauseBehaviour pauseBehaviour;

    [SerializeField] List<InteractableObject> interactableObjectsObj = new List<InteractableObject>();
    [SerializeField] LayerMask ground, interactableObjects, obstacles;

    [SerializeField] UITrading tradingUi;

    [SerializeField] List<VehicleInfo> listaMacchine = new List<VehicleInfo>();
    EventPopUp popUpEvent;
    List<string> listaStringhe = new List<string>();
    [SerializeField] List<Recipe> possibleRecipes = new List<Recipe>();

    TutorialObserver tutorial;

    bool vehicleDataStartSet = false;
    bool isWritingName = false;
    public GameObject Player { get => player; set => player = value; }
    public Camera PlayerCamera { get => playerCamera; set => playerCamera = value; }
    public float FogRadius { get => fogRadius; set => fogRadius = value; }

    public List<Recipe> PossibleRecipes { get => possibleRecipes; set => possibleRecipes = value; }
    public bool VehicleDataStartSet { get => vehicleDataStartSet; set => vehicleDataStartSet = value; }
    public InteractableObject PlayerScript { get => playerScript; set => playerScript = value; }
    public List<InteractableObject> InteractableObjectsObj { get => interactableObjectsObj; set => interactableObjectsObj = value; }

    public int Gas { get => gas; set => gas = value; }
    public LayerMask Ground { get => ground; set => ground = value; }
    public LayerMask InteractableObjects { get => interactableObjects; set => interactableObjects = value; }
    public bool IsWritingName { get => isWritingName; set => isWritingName = value; }
    public Convoy PlayerConvoy { get => playerConvoy; set => playerConvoy = value; }
    public GameObject TargetParent { get => targetParent; set => targetParent = value; }
    public TutorialObserver Tutorial { get => tutorial; set => tutorial = value; }
    public PauseBehaviour PauseBehaviour { get => pauseBehaviour; set => pauseBehaviour = value; }

    void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        Tutorial = this.GetComponent<TutorialObserver>();
        playerScript = player.GetComponent<InteractableObject>();
        playerConvoy = player.GetComponent<Convoy>();
        StartOpenWorld();
        StartCoroutine(ManageGas());
        UpdatePlayerParty();
        Cursor.lockState = CursorLockMode.Confined;
        CarListOpenWorld.Instance.OpenWorldInteractableObjcts.Clear();
        openWorldSoundManager = GetComponent<OpenWorldSoundManager>();
        PauseBehaviour = this.GetComponent<PauseBehaviour>();

        for (int i=0; i< CarListOpenWorld._instance.PossibleQuests.Count; i++)
        {
            Quest q = CarListOpenWorld._instance.PossibleQuests[i];
            InteractableObject obj = GetInteractableObjectFromID(q.Steps[0].ObjectiveID);
            if (obj)
            {
                QuestData newQuest = new QuestData(q.Steps[0].DialogueID, q.QuestID, q.Steps[0]);
                obj.AddQuest(newQuest);
            }
        }

       
    }

    public InteractableObject GetInteractableObjectFromID(string objId)
    {
        //TOIMPROVE fare hashtable
        InteractableObject obj = null;

        for (int i = 0; i < interactableObjectsObj.Count; i++)
        {
            if (interactableObjectsObj[i].InteractableData.InteractableID.Equals(objId) )
            {
                return interactableObjectsObj[i];
            }
        }

        return obj;
    }

    public List<QuestData> GetActualAvailableQuests()
    {
        if (enemyScript)
            return enemyScript.InteractableData.Quests;
        else return null;
    }
    public DialogueUI GetDialogueUI()
    {
        return openWorldUIManager.SubMenus[5].GetComponent<DialogueUI>();
    }

    private void Update()
    {
        gas = CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[indiceBenzina].Quantity;
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (openWorldUIManager.SubMenus[0].activeSelf)
            {
                CloseCombatLog(true);
            }
            else
            {
                OpenCombatLog(null, 0);
            }
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (openWorldUIManager.SubMenus[4].activeSelf)
            {
                CloseCombatLog(true);
            }
            else
            {
                OpenCombatLog(null, 4);
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CloseCombatLog(false);
        }
    }
    int GetPointOfInterestNumber()
    {
        Collider[] points = Physics.OverlapSphere(player.transform.position, radiusMax);
        return points.Length;
    }

    IEnumerator SpawnRandomPointsOfInterest()
    {
        if (CarListOpenWorld.Instance.OpenWorldInteractableObjcts.Count > 0)
        {
            for (int i = 0; i < CarListOpenWorld.Instance.OpenWorldInteractableObjcts.Count; i++)
            {
                IntPointType type = CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].PointType;

                if (type == IntPointType.HiddenStash || type == IntPointType.CrashSite || type == IntPointType.Outpost || type == IntPointType.TradingOutpost)
                {
                    if(CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].ObjectType == IntType.dynamic)
                    {
                        GameObject newPoint = Instantiate(location, CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].ObjectPosition, Quaternion.identity);
                        newPoint.transform.parent = pointOfInterestParent.transform;
                        InteractableObject interactable = newPoint.GetComponent<InteractableObject>();
                        interactable.InteractableData = CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i];
                        if (interactable.InteractableData.InteractableID == CarListOpenWorld.Instance.IntercaableObjectID)  
                        {
                            if (CarListOpenWorld.Instance.HasWon == true)
                            {
                                //Debug.Log("add Ricompense");
                                AddRicompense(interactable);
                            }
                            if(interactable.InteractableData.Quests.Count <= 0)
                            {
                                //TODO settare evento specifico per la distruzione del interactableobject
                                Destroy(newPoint);
                                RemoveInteractableObj(interactable.gameObject);
                            }
                            else
                            {
                                interactableObjectsObj.Add(interactable);
                                //Debug.Log("rinnovo dio" + interactable);
                            }
                        }
                        interactableObjectsObj.Add(interactable);

                        //Debug.Log("rinnovo " + interactable);
                    }
                    else if (CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].ObjectType == IntType.persistent)
                    {
                        for(int n = 0; n < interactableObjectsObj.Count; n++)
                        {
                            if(interactableObjectsObj[n].InteractableData.InteractableID == CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].InteractableID)
                            {
                                interactableObjectsObj[n].InteractableData = CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i];
                            }
                        }


                        if (interactableObjectsObj[i].InteractableData.InteractableID == CarListOpenWorld.Instance.IntercaableObjectID)
                        {                            
                            if (CarListOpenWorld.Instance.HasWon == true)
                            {
                                //Debug.Log("add Ricompense pers");
                                AddRicompense(interactableObjectsObj[i]);
                            }
                            interactableObjectsObj[i].InteractableData.IsDead = true;
                        }

                    }
                }
            }
        }

        while (true)
        {

            if(GetPointOfInterestNumber() <= maxRandomPoints)
            {
                float angle = Random.Range(0, 2 * Mathf.PI);
                float radius = Random.Range(radiusMin, radiusMax);
                float x = Mathf.Cos(angle) * radius;
                float y = Mathf.Sin(angle) * radius;

                Vector3 SpawnPos = player.transform.position + new Vector3(x, 0, y);

                IntPointType pointType = IntPointType.Outpost;
                IntType type = IntType.dynamic;

                if (Physics.OverlapSphere(SpawnPos, 10f, interactableObjects).Length == 0)
                {
                    SpawnInteractableObject(pointType, type, SpawnPos);
                }
                yield return new WaitForSeconds(1);
            }
            else
            {
                yield return new WaitForSeconds(10);
                for(int i = 0; i < interactableObjectsObj.Count; i++)
                {
                    IntPointType type = interactableObjectsObj[i].InteractableData.PointType;

                    if (type == IntPointType.CrashSite || type == IntPointType.HiddenStash || type == IntPointType.Outpost || type == IntPointType.TradingOutpost)
                    {
                        if (Vector3.Distance(interactableObjectsObj[i].transform.position, player.transform.position) >= radiusMax && interactableObjectsObj[i].InteractableData.ObjectType == IntType.dynamic && TryGetComponent(out QuestTarget targ) == false)
                        {
                            RemoveInteractableObj(interactableObjectsObj[i].gameObject);
                        }
                    }
                }
            }
        }
    }

    IEnumerator SpawnRandomConvoys()
    {

        if (CarListOpenWorld.Instance.OpenWorldInteractableObjcts.Count > 0)
        {
            for (int i = 0; i < CarListOpenWorld.Instance.OpenWorldInteractableObjcts.Count; i++)
            {
                if (CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].ObjectType == IntType.player)
                {
                    player.transform.position = CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].ObjectPosition;
                    player.GetComponent<Convoy>().TargetDest.transform.position = player.transform.position;
                }
                else if (CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].PointType == IntPointType.aggressive || CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].PointType == IntPointType.passive || CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].PointType == IntPointType.shop)
                {

                    GameObject newConvoy = Instantiate(convoy, CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i].ObjectPosition, Quaternion.identity);
                    GameObject newTarget = Instantiate(target, targetParent.transform);
                    
                    newConvoy.transform.parent = pointOfInterestParent.transform;
                    Convoy interactableConvoy = newConvoy.GetComponent<Convoy>();
                    newTarget.transform.position = interactableConvoy.InteractableData.TargetDestPosition;
                    interactableConvoy.TargetDest = newTarget;
                    interactableConvoy.InteractableData = CarListOpenWorld.Instance.OpenWorldInteractableObjcts[i];
                    interactableConvoy.MoveTo(interactableConvoy.TargetDest.transform);

                    if (interactableConvoy.InteractableData.InteractableID == CarListOpenWorld.Instance.IntercaableObjectID)
                    {
                        if (CarListOpenWorld.Instance.HasWon == true)
                        {
                            AddRicompense(interactableConvoy);
                        }
                        else
                        {
                            RemoveInteractableObj(interactableConvoy.gameObject);
                        }
                        if(interactableConvoy.InteractableData.ObjectType == IntType.persistent)
                        {
                            interactableConvoy.InteractableData.IsDead = true;
                        }
                        else
                        {
                            Destroy(newConvoy);
                        }

                    }
                    else
                    {
                        interactableObjectsObj.Add(interactableConvoy);
                    }
                    
                }
                
            }
        }

        while (true)
        {
            yield return new WaitForSeconds(1);

            int convoyCount = 0;
            List<InteractableObject> staticObj = new List<InteractableObject>();
            for(int i = 0; i < interactableObjectsObj.Count; i++)
            {
                if (interactableObjectsObj[i].InteractableData.PointType == IntPointType.shop || interactableObjectsObj[i].InteractableData.PointType == IntPointType.aggressive ||
                    interactableObjectsObj[i].InteractableData.PointType == IntPointType.passive)
                {
                    convoyCount++;
                }
                if(interactableObjectsObj[i].InteractableData.ObjectType == IntType.persistent)
                {
                    staticObj.Add(interactableObjectsObj[i]);
                }
            }

            if(convoyCount < maxRandomConvoys)
            {
                int outpost = Random.Range(0, staticObj.Count);

                Vector3 spawnPos = staticObj[outpost].transform.position;

                IntPointType pointType = IntPointType.passive;
                
                int i = Random.Range(0, 3);
                switch (i)
                {
                    case 0:
                        pointType = IntPointType.aggressive;
                        break;
                    case 1:
                        pointType = IntPointType.passive;
                        break;
                    case 2:
                        pointType = IntPointType.shop;
                        break;
                }
                

                IntType type = IntType.dynamic;


                Convoy convoyScript = SpawnInteractableObject(pointType, type, spawnPos).gameObject.GetComponent<Convoy>();

                if (outpost >= staticObj.Count - 1)
                {
                    convoyScript.MoveTo(staticObj[0].transform);
                }
                else
                {
                    convoyScript.MoveTo(staticObj[outpost + 1].transform);
                }
                
                interactableObjectsObj.Add(convoyScript);

                yield return new WaitForSeconds(1);
            }
            yield return null;
        }
    }

    void StartOpenWorld()
    {
        StartCoroutine(SpawnRandomPointsOfInterest());
        StartCoroutine(SpawnRandomConvoys());
        UpdateOpenWorldQuests();
    }

    public float CalculateGasConsumption(Vector3 dest)
    {
        float dist = Vector3.Distance(player.transform.position, dest);
        float estimatedTime = dist / playerConvoy.TargetSpeed;
        float estimatedConsumption = estimatedTime * consumption;
        return estimatedConsumption;
    }

    

    void UpdateOpenWorldQuests()
    {
        for(int i = 0; i < interactableObjectsObj.Count; i++)
        {
            interactableObjectsObj[i].SetQuest();
        }
    }

    public VehicleInfo GetCarIndex(int Index)
    {
        for (int i = 0; i < listaMacchine.Count; i++)
        {
            if (listaMacchine[i].Index == Index)
            {
                return listaMacchine[i];
            }
        }
        return null;
    }

    public void RemoveInteractableObj(GameObject interactable)
    {
        Destroy(interactable);
        interactableObjectsObj.Remove(interactable.GetComponent<InteractableObject>());
    }

    public InteractableObject FindInteractableObj(string objID)
    {
        for(int i = 0; i < interactableObjectsObj.Count; i++)
        {
            if(interactableObjectsObj[i].InteractableData.InteractableID == objID)
            {
                return interactableObjectsObj[i];
            }
        }
        return null;
    }

    public void OpenCombatLog(InteractableObject enemy, int type)
    {
        if(openWorldSoundManager)
        {
            openWorldSoundManager.PlayOpenPopUp();
        }

        playerConvoy.CanRaycast = false;
        //Time.timeScale = 0;
        PauseBehaviour.Pause();
        if(enemy != null)
        {
            enemyScript = enemy;
            // enemy.AddButtons();
            Debug.Log("SET DATABASE");
            GlobalDatabaseManager.Instance.Database.Strings.Set("Posture", "Attackable");
            GlobalDatabaseManager.Instance.Database.Strings.Set("Vendor", enemy.InteractableData.Trade);
            
            
         //   QuestTarget targ = enemyScript.GetComponent<QuestTarget>();

          

            if (enemyScript.InteractableData.Quests.Count > 0)
            {
               // QuestData quest = CarListOpenWorld.Instance.GetQuestData(targ.QuestID);
                GlobalDatabaseManager.Instance.Database.Strings.Set("Dialogue", enemyScript.InteractableData.Description);
            }
            else
                GlobalDatabaseManager.Instance.Database.Strings.Set("Dialogue", "");



            GlobalDatabaseManager.Instance.Database.Strings.Set("Aggressive", enemy.InteractableData.Aggressive);
        }

        playerCameraScript.CanZoom = false;

        Tutorial.StartTutorial("interaction");
        openWorldUIManager.ActivateUI(enemyScript, type);

     
    }

    public void CloseCombatLog(bool fromBack)
    {
        if (openWorldSoundManager != null)
        {
            openWorldSoundManager.PlayClosePopUp();
        }

        for (int i = 0; i < openWorldUIManager.SubMenus.Count; i++)
        {
            if (openWorldUIManager.SubMenus[i].activeSelf)
            {
                if((i == 5 || i == 1 || i==3) && !fromBack)
                {
                    break;
                }
                else
                {
                    if (openWorldUIManager.SubMenus[i] != null)
                    {
                        if (!isWritingName)
                        {
                            openWorldUIManager.DeactivateUI(i);
                            playerConvoy.CanRaycast = true;
                            //Time.timeScale = 1;
                            PauseBehaviour.UnPause();
                            playerCameraScript.CanZoom = true;
                        }
                    }
                }
            }
        }
    }

    public void CloseInteractionLog()
    {
        for (int i = 0; i < openWorldUIManager.SubMenus.Count; i++)
        {
            if (openWorldUIManager.SubMenus[i] != null)
            {
                openWorldUIManager.DeactivateUI(i);
            }
        }
    }

    IEnumerator ManageGas()
    {

        while (true)
        {
            if (CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[indiceBenzina].Index != 0)
            {
                for (int i = 0; i < CarListOpenWorld.Instance.PlayerInventory.ListaMateriali.Count; i++)
                {
                    if (CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[i].Index == 0)
                    {
                        indiceBenzina = i;
                    }
                }
            }

            if (playerConvoy.IsMoving && CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[indiceBenzina].Quantity > 0)
            {
                CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[indiceBenzina].Quantity -= 1 * consumption;
            }
            else if(CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[indiceBenzina].Quantity <= 0)
            {
                playerConvoy.CanMove = false;

                OpenCombatLog(outOfGas, 5);
                //StartBattle();
            }
            else if(CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[indiceBenzina].Quantity > 0)
            {
                playerConvoy.CanMove = true;
            }

            yield return new WaitForSeconds(1f);
            
        }
    }

    int SetDifficultyLevel(InteractableObject obj)
    {
        float dist = Vector3.Distance(new Vector3(0, 0, 0), obj.transform.position);
        int n;

        if(dist > dist1)
        {
            n = Random.Range(increment * 1, increment * multiplier * 2);
        }
        else if(dist > dist2)
        {
            n = Random.Range(increment * multiplier * 2, increment * multiplier * 3);
        }
        else if(dist > dist3)
        {
            n = Random.Range(increment * multiplier * 3, increment * multiplier * 4);
        }
        else
        {
            n = Random.Range(1, increment * multiplier);
        }

        return n;
    }

    public void StartTrade()
    {
        StartCoroutine(StartTrade_Coroutine());
     
    }

    IEnumerator StartTrade_Coroutine()
    {
        while (openWorldUIManager.SubMenus[5].activeInHierarchy)
        {
            yield return new WaitForEndOfFrame();
        }
       // CloseCombatLog(true);
        OpenCombatLog(null, 2);
    }

    public void Escape()
    {
        CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[indiceBenzina].Quantity -= 100;
       // CloseCombatLog(true);
        
    }
    public void StartFight()
    {
        if (PlayerScript.InteractableData.CarInfo.Count > 0)
        {
            CloseCombatLog(true);
            CarListOpenWorld.Instance.UpdateVehiclesData(playerScript.InteractableData.CarInfo, enemyScript.InteractableData.CarInfo, enemyScript.SceneToLoad[Random.Range(0, enemyScript.SceneToLoad.Count)], CarListOpenWorld.Instance.OpenWorldMap, interactableObjectsObj);
        }
        
    }
    public void GiveMoney()
    {

    }
    public void Back()
    {
        CloseCombatLog(true);
    }
    public void SelectCars()
    {
        //Debug.Log("start attack");
        StartCoroutine(SelectCars_Coroutine());
    }

    IEnumerator SelectCars_Coroutine()
    {
        while (openWorldUIManager.SubMenus[5].activeInHierarchy)
        {
            yield return new WaitForEndOfFrame();
        }
        // CloseInteractionLog();
        //CloseCombatLog(true);
        OpenCombatLog(null, 3);
    }
    public void StartDialogue()
    {
        /*
       // QuestTarget targ = enemyScript.GetComponent<QuestTarget>();
       enemyScript.
        QuestData quest = CarListOpenWorld.Instance.GetQuestData(targ.QuestID);
        CloseCombatLog(true);
        Debug.Log(quest.DialogueID);

        DialogueManager._manager.AssignDialogue(quest.DialogueID, quest.NextStep, null);
       // Destroy(targ);
      //  enemyScript.InteractableData.HasQuest = false;
        */
    }

    public void UpdateTraderInventory(InventarioPlayer traderInventory)
    {
        tradingUi.GetVendorInventory(traderInventory);
    }


    public MaterialQuantity AddItem(MaterialQuantity material, InventarioPlayer inventory, VehicleData data)
    {
        bool wasFound = false;
        for (int i = 0; i < inventory.ListaMateriali.Count; i++)
        {
            if (!material.IsVehicle)
            {
                if (material.Index.Equals(inventory.ListaMateriali[i].Index))
                {
                    inventory.ListaMateriali[i].Quantity += material.Quantity;
                    wasFound = true;
                    return inventory.ListaMateriali[i];
                }
            }
        }
        if (!wasFound || material.IsVehicle)
        {
            string ID = "";

            if (material.IsVehicle)
            {
                if (data != null)
                {
                    inventory.ListaDatiVeicoli.Add(data);
                    ID = data.CarId;
                }
                else
                {
                    VehicleData newData = new VehicleData();
                    newData.CarId = newData.RandomizeID();
                    ID = newData.CarId;
                    inventory.ListaDatiVeicoli.Add(newData);
                }
                material.VehicleDataId = ID;
            }
            inventory.ListaMateriali.Add(material);
            return material;
        }
        return null;
    }
    public MaterialQuantity RemoveItem(MaterialQuantity material, InventarioPlayer inventory)
    {
        for (int i = 0; i < inventory.ListaMateriali.Count; i++)
        {
            if (material.Index.Equals(inventory.ListaMateriali[i].Index))
            {
                if (material.Quantity < inventory.ListaMateriali[i].Quantity && !material.IsVehicle)
                {
                    inventory.ListaMateriali[i].Quantity -= material.Quantity;
                    return inventory.ListaMateriali[i];
                }
                else
                {
                    if (material.IsVehicle)
                    {
                        inventory.ListaDatiVeicoli.Remove(FindVehicleData(inventory.ListaMateriali[i].VehicleDataId, inventory));
                    }
                    inventory.ListaMateriali.RemoveAt(i);
                    return null;
                }
            }
        }
        return null;
    }
    public VehicleData FindVehicleData(string id, InventarioPlayer inventory)
    {
        for (int i = 0; i < inventory.ListaDatiVeicoli.Count; i++)
        {
            //Debug.LogError(inventory.ListaDatiVeicoli[i].CarId);
            if (id.Equals(inventory.ListaDatiVeicoli[i].CarId))
            {
                //Debug.LogError("Removed data");
                return inventory.ListaDatiVeicoli[i];
            }
        }
        return null;
    }

    [System.Serializable]
    public class InventarioPlayer
    {
        [SerializeField] List<MaterialQuantity> listaMateriali = new List<MaterialQuantity>();
        [SerializeField] List<VehicleData> listaDatiVeicoli = new List<VehicleData>();

        public List<MaterialQuantity> ListaMateriali { get => listaMateriali; set => listaMateriali = value; }
        public List<VehicleData> ListaDatiVeicoli { get => listaDatiVeicoli; set => listaDatiVeicoli = value; }
    }

    void UpdatePlayerParty()
    {
        List<string> idToRemove = new List<string>();
        
        for (int i = 0; i < CarListOpenWorld.Instance.Team1Data.Count; i++)
        {
            if(CarListOpenWorld.Instance.Team1Data[i].HealthPerc == 0)
            {
                idToRemove.Add(CarListOpenWorld.Instance.Team1Data[i].CarId);
            }
        }
        for(int i = 0; i < idToRemove.Count; i++)
        {
            for(int n = 0; n < CarListOpenWorld.Instance.PlayerInventory.ListaMateriali.Count; n++)
            {
                if (CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[n].VehicleDataId == idToRemove[i])
                {
                    CarListOpenWorld.Instance.PlayerInventory.ListaMateriali.Remove(CarListOpenWorld.Instance.PlayerInventory.ListaMateriali[n]);
                }
            }
            for (int n = 0; n < CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli.Count; n++)
            {
                if (CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli[n].CarId == idToRemove[i])
                {
                    CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli.Remove(CarListOpenWorld.Instance.PlayerInventory.ListaDatiVeicoli[n]);
                }
            }
        }

        CarListOpenWorld.Instance.Team1Data.Clear();
        CarListOpenWorld.Instance.Team2Data.Clear();
    }

    void AddRicompense(InteractableObject obj)
    {
        for (int n = 0; n < obj.InteractableData.Ricompense.Count; n++)
        {
            AddItem(obj.InteractableData.Ricompense[n], CarListOpenWorld.Instance.PlayerInventory, null);
           //Debug.Log(obj.InteractableData.Ricompense[n].Quantity);
        }
    }


    public void LoadTestScene()
    {
        SceneManager.LoadScene(CarListOpenWorld.Instance.MenuScene);
        Time.timeScale = 1;
        playerCameraScript.CanZoom = true;
    }

    public InteractableObject SpawnInteractableObject(IntPointType type, IntType objType, Vector3 spawnPosition)
    {
        if (type == IntPointType.HiddenStash || type == IntPointType.Outpost || type == IntPointType.CrashSite)
        {
            Physics.Raycast(spawnPosition + new Vector3(0, 100, 0), -transform.up, out RaycastHit hit, 1000, ground);

            GameObject newPoint = Instantiate(location, hit.point, Quaternion.identity);
            newPoint.transform.up = hit.normal;
            newPoint.transform.parent = pointOfInterestParent.transform;
            InteractableObject interactable = newPoint.GetComponent<InteractableObject>();
            interactable.RandomizeVehicles(SetDifficultyLevel(interactable));
            interactable.InteractableData.CanDisappear = true;
            interactable.InteractableData.PointType = type;
            if (objType == IntType.dynamic)
            {
                interactable.InteractableData.ObjectType = IntType.dynamic;
            }
            else if (objType == IntType.persistent)
            {
                interactable.InteractableData.ObjectType = IntType.persistent;
            }
            interactable.InteractableData.InteractableID = interactable.RandomizeID();
            interactableObjectsObj.Add(interactable);
            interactable.ManageFogOfWar();
            //Debug.Log("aggiungo " + interactable);
            return interactable;
        }
        else
        {
            GameObject newConvoy = Instantiate(convoy, spawnPosition, Quaternion.identity);
            GameObject newTarget = Instantiate(target, targetParent.transform);
            newConvoy.transform.parent = pointOfInterestParent.transform;
            Convoy convoyScript = newConvoy.GetComponent<Convoy>();
            convoyScript.RandomizeVehicles(SetDifficultyLevel(convoyScript));
            convoyScript.TargetDest = newTarget;
            convoyScript.InteractableData.CanDisappear = true;
            if(type == IntPointType.aggressive)
            {
                convoyScript.InteractableData.CanInteract = true;
            }
            else
            {
                convoyScript.InteractableData.CanInteract = false;
            }

            convoyScript.InteractableData.InteractableID = convoyScript.RandomizeID();
            convoyScript.InteractableData.PointType = type;
            if (objType == IntType.dynamic)
            {
                convoyScript.InteractableData.ObjectType = IntType.dynamic;
            }
            else if (objType == IntType.persistent)
            {
                convoyScript.InteractableData.ObjectType = IntType.persistent;
            }
            interactableObjectsObj.Add(convoyScript);
            return convoyScript;
        }
    }
}

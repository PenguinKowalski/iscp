using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VehicleSelectionPrefab : MonoBehaviour
{
    [SerializeField] Dropdown vehicleDropdown;
    [SerializeField] Dropdown weaponDropdown;
    [SerializeField] Dropdown armorDropdown;
    [SerializeField] Dropdown engineDropdown;
    [SerializeField] TMP_InputField carName;

    [SerializeField] List<string> nomi = new List<string>();

    [SerializeField] List<int> indiciArmi = new List<int>();
    [SerializeField] List<int> indiciVeicoli = new List<int>();
    [SerializeField] List<int> indiciArmature = new List<int>();
    [SerializeField] List<int> indiciMotori = new List<int>();


    int vehicleDropdownInt;
    int weaponDropdownInt;
    int armorDropdownInt;
    int engineDropdownInt;
    string carNameString;

    public int VehicleDropdownInt { get => vehicleDropdownInt; set => vehicleDropdownInt = value; }
    public int WeaponDropdownInt { get => weaponDropdownInt; set => weaponDropdownInt = value; }
    public string CarNameString { get => carNameString; set => carNameString = value; }
    public int ArmorDropdownInt { get => armorDropdownInt; set => armorDropdownInt = value; }
    public int EngineDropdownInt { get => engineDropdownInt; set => engineDropdownInt = value; }

    private void Start()
    {
        AddOptions();
        int i = Random.Range(0, nomi.Count);
        carName.text = nomi[i];
    }

    // Update is called once per frame
    void Update()
    {
        vehicleDropdownInt = indiciVeicoli[vehicleDropdown.value];
        weaponDropdownInt = indiciArmi[weaponDropdown.value];
        armorDropdownInt = indiciArmature[armorDropdown.value];
        engineDropdownInt = indiciMotori[engineDropdown.value];
        carNameString = carName.text;
    }

    void AddOptions()
    {
        List<Dropdown.OptionData> armorOptions = new List<Dropdown.OptionData>();
        List<Dropdown.OptionData> weaponOptions = new List<Dropdown.OptionData>();
        List<Dropdown.OptionData> vehicleOptions = new List<Dropdown.OptionData>();
        List<Dropdown.OptionData> engineOptions = new List<Dropdown.OptionData>();



        for (int i = 0; i < CarListOpenWorld.Instance.ListaMateriali.Count; i++)
        {
            switch (CarListOpenWorld.Instance.ListaMateriali[i].ItemCategory)
            {
                case CraftingMaterial.Category.arma:

                    Dropdown.OptionData option = new Dropdown.OptionData();
                    option.text = CarListOpenWorld.Instance.ListaMateriali[i].Nome;
                    weaponOptions.Add(option);
                    indiciArmi.Add(CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex);

                    break;
                case CraftingMaterial.Category.armatura:

                    Dropdown.OptionData option1 = new Dropdown.OptionData();
                    option1.text = CarListOpenWorld.Instance.ListaMateriali[i].Nome;
                    armorOptions.Add(option1);
                    indiciArmature.Add(CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex);

                    break;
                case CraftingMaterial.Category.motore:

                    Dropdown.OptionData option2 = new Dropdown.OptionData();
                    option2.text = CarListOpenWorld.Instance.ListaMateriali[i].Nome;
                    engineOptions.Add(option2);
                    indiciMotori.Add(CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex);

                    break;
                case CraftingMaterial.Category.veicoli:

                    Dropdown.OptionData option3 = new Dropdown.OptionData();
                    option3.text = CarListOpenWorld.Instance.ListaMateriali[i].Nome;
                    vehicleOptions.Add(option3);
                    indiciVeicoli.Add(CarListOpenWorld.Instance.ListaMateriali[i].ItemIndex);
                    break;
            }
        }

        vehicleDropdown.options = vehicleOptions;
        weaponDropdown.options = weaponOptions;
        armorDropdown.options = armorOptions;
        engineDropdown.options = engineOptions;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CarListOpenWorld : MonoBehaviour
{
    public static CarListOpenWorld _instance;
    [SerializeField] JsonReader jsonReaderScript;

    [SerializeField] List<VehicleData> team1Data = new List<VehicleData>();
    [SerializeField] List<VehicleData> team2Data = new List<VehicleData>();

    [SerializeField] List<CraftingMaterial> listaMateriali = new List<CraftingMaterial>();

    [SerializeField] List<ObjectInteractableVariables> openWorldInteractableObjcts = new List<ObjectInteractableVariables>();

    [SerializeField] OpenWorld.InventarioPlayer playerInventory;

    [SerializeField] List<Quest> possibleQuests = new List<Quest>();
    [SerializeField] List<Quest> completedQuests = new List<Quest>();
    [SerializeField] List<Quest> failedQuests = new List<Quest>();

    [SerializeField] List<QuestData> activeQuests = new List<QuestData>();

    [SerializeField] string trakedQuestID;

    [SerializeField] string loadedScene;
    [SerializeField] string sceneLoaded;

    [SerializeField] bool hasWon;

    string intercaableObjectID;

    [SerializeField] GameObject controls;

    string openWorldMap;
    string openWorldMapTest;

    [SerializeField] string menuScene;

    public string OpenWorldMapTest { get => openWorldMapTest; set => openWorldMapTest = value; }
    public string OpenWorldMap { get => openWorldMap; set => openWorldMap = value; }

    public List<VehicleData> Team1Data { get => team1Data; set => team1Data = value; }
    public List<VehicleData> Team2Data { get => team2Data; set => team2Data = value; }
    public List<CraftingMaterial> ListaMateriali { get => listaMateriali; set => listaMateriali = value; }
    public string LoadedScene { get => loadedScene; set => loadedScene = value; }
    public List<ObjectInteractableVariables> OpenWorldInteractableObjcts { get => openWorldInteractableObjcts; set => openWorldInteractableObjcts = value; }
    public string IntercaableObjectID { get => intercaableObjectID; set => intercaableObjectID = value; }
    public OpenWorld.InventarioPlayer PlayerInventory { get => playerInventory; set => playerInventory = value; }
    public bool HasWon { get => hasWon; set => hasWon = value; }
    public string MenuScene { get => menuScene; set => menuScene = value; }
    public string SceneLoaded { get => sceneLoaded; set => sceneLoaded = value; }
    public List<QuestData> ActiveQuests { get => activeQuests; set => activeQuests = value; }
    public List<Quest> PossibleQuests { get => possibleQuests; set => possibleQuests = value; }
    public string TrakedQuestID { get => trakedQuestID; set => trakedQuestID = value; }
    public List<Quest> CompletedQuests { get => completedQuests; set => completedQuests = value; }
    public List<Quest> FailedQuests { get => failedQuests; set => failedQuests = value; }
    public JsonReader JsonReaderScript { get => jsonReaderScript; set => jsonReaderScript = value; }


    public static CarListOpenWorld Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<CarListOpenWorld>();

                if (_instance == null)
                {
                    GameObject instance = Instantiate(Resources.Load("CarLists", typeof(GameObject))) as GameObject;
                    DontDestroyOnLoad(instance);
                    _instance = instance.GetComponent<CarListOpenWorld>();
                }
            }

            return _instance;
        }
    }
    /*
    private void Awake()
    {
        if (_instance == null)
        {
        
            _instance = this;
        }
    }
    */

    public CraftingMaterial GetMaterialIndex(int Index)
    {
        for (int i = 0; i < Instance.ListaMateriali.Count; i++)
        {
            if (Instance.ListaMateriali[i].ItemIndex == Index)
            {
                return Instance.ListaMateriali[i];
            }
        }
        return null;
    }

    public void UpdateVehiclesData(List<VehicleData> team1Vehicles, List<VehicleData> team2Vehicles, string scene, string sceneToLoadFrom, List<InteractableObject> objs)
    {
        for(int i = 0; i < team1Vehicles.Count; i++)
        {
            team1Data.Add(team1Vehicles[i]);
        }
        for (int i = 0; i < team2Vehicles.Count; i++)
        {
            team2Data.Add(team2Vehicles[i]);
        }
        for (int i = 0; i < objs.Count; i++)
        {
            ObjectInteractableVariables varia = new ObjectInteractableVariables();
            varia = objs[i].InteractableData;

            OpenWorldInteractableObjcts.Add(varia);
        }

        SceneManager.LoadScene(scene);
        loadedScene = sceneToLoadFrom;
        sceneLoaded = scene;

    }


    public void RemoveCarFromList(string carID)
    {
        int dead = 0;
        int alive = 0;

        for (int n = 0; n < team1Data.Count; n++)
        {
            if (team1Data[n].HealthPerc <= 0)
            {
                dead++;
                //Debug.Log("dead "+ dead);
                //Debug.Log("alive " + alive);
            }
            else
            {
                alive++;
                //Debug.Log("dead " + dead);
                //Debug.Log("alive " + alive);
            }
        }

        
        for (int i = 0; i < team1Data.Count; i++)
        {
            if (team1Data[i].CarId == carID)
            {
                if(team1Data.Count < playerInventory.ListaDatiVeicoli.Count)
                {
                    //Debug.Log("ha piu macchine");
                    team1Data[i].HealthPerc = 0;
                }
                else if(team1Data.Count > 1)
                {
                    if(alive > dead)
                    {
                        //Debug.Log("ha ancora macchine vive");
                        team1Data[i].HealthPerc = 0;
                    }
                }

                break;
            }
        }
        
    }

    public QuestData GetQuestData(string questID)
    {
        for (int i = 0; i < ActiveQuests.Count; i++)
        {
            if (ActiveQuests[i].QuestID == questID)
            {
                return ActiveQuests[i];
            }
        }
        return null;
    }

    public Quest GetQuest(string questID)
    {
        for (int i = 0; i < PossibleQuests.Count; i++)
        {
            if (PossibleQuests[i].QuestID == questID)
            {
                return PossibleQuests[i];
            }
        }
        return null;
    }


    public QuestStep GetNextQuestStep(string questID, string actualStepID, string answer)
    {
        Quest quest = GetQuest(questID);

        QuestStep actual = quest.GetQuestStep(actualStepID);
        string nextId = actual.NextStep(answer);

        return quest.GetQuestStep(nextId);     
    }

    public void TrakNewQuest(string newQuest)
    {
        trakedQuestID = newQuest;
    }
     private void Update()
     {
         if(Input.GetKeyDown(KeyCode.Tab))
         {
             ControlsCanvasManager._controls.gameObject.SetActive(true);
         }
         if(Input.GetKeyUp(KeyCode.Tab))
         {
             ControlsCanvasManager._controls.gameObject.SetActive(false);
         }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CarSelection : MonoBehaviour
{
    [SerializeField] string sceneToLoad;
    [SerializeField] string sceneToLoadConvoy;
    [SerializeField] string sceneToLoadMenu;

    List<GameObject> team1UI = new List<GameObject>();
    List<GameObject> team2UI = new List<GameObject>();

    [SerializeField] Slider team1Size;
    [SerializeField] Slider team2Size;

    [SerializeField] GameObject team1ListParent;
    [SerializeField] GameObject team2ListParent;

    [SerializeField] GameObject CarDataUI;

    [SerializeField] Toggle toggle;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;

        if(CarListOpenWorld.Instance.Team1Data.Count > 0)
        {
            CarListOpenWorld.Instance.Team1Data.Clear();
            CarListOpenWorld.Instance.Team2Data.Clear();
        }
        UpdateSizeTeam1();
        UpdateSizeTeam2();
    }

    public void UpdateSizeTeam1()
    {        
        for(int i = 0; i < team1UI.Count; i++)
        {
            //Debug.Log(team1UI.Count);
            Destroy(team1UI[i]);
            team1UI.Remove(team1UI[i]);
        }
        for (int i = 0; i < team1Size.value; i++)
        {
            //Debug.Log(team1Size.value);
            GameObject newCarDataUI = Instantiate(CarDataUI, team1ListParent.transform);
            team1UI.Add(newCarDataUI);
        }
    }

    public void UpdateSizeTeam2()
    {
        for (int i = 0; i < team2UI.Count; i++)
        {
            Destroy(team2UI[i]);
            team2UI.Remove(team2UI[i]);
        }
        for (int i = 0; i < team2Size.value; i++)
        {
            GameObject newCarDataUI = Instantiate(CarDataUI, team2ListParent.transform);
            team2UI.Add(newCarDataUI);
        }
    }

    public void UpdateVehiclesData()
    {
        for(int i = 0; i < team1UI.Count; i++)
        {
            VehicleData data = new VehicleData();
            VehicleSelectionPrefab selection = team1UI[i].GetComponent<VehicleSelectionPrefab>();

            data.VehicleType = selection.VehicleDropdownInt;
            data.WeaponType = selection.WeaponDropdownInt;
            data.CarName = selection.CarNameString;
            data.ArmorType = selection.ArmorDropdownInt;
            data.MotorType = selection.EngineDropdownInt;
            data.Team = 0;

            CarListOpenWorld.Instance.Team1Data.Add(data);
        }
        for (int i = 0; i < team2UI.Count; i++)
        {
            VehicleData data = new VehicleData();
            VehicleSelectionPrefab selection = team2UI[i].GetComponent<VehicleSelectionPrefab>();

            data.VehicleType = selection.VehicleDropdownInt;
            data.WeaponType = selection.WeaponDropdownInt;
            data.CarName = selection.CarNameString;
            data.ArmorType = selection.ArmorDropdownInt;
            data.MotorType = selection.EngineDropdownInt;
            data.Team = 1;
            data.IsConvoy = toggle.isOn;

            CarListOpenWorld.Instance.Team2Data.Add(data);
        }

        if (toggle.isOn)
        {
            SceneManager.LoadScene(sceneToLoadConvoy);
        }
        else
        {
            SceneManager.LoadScene(sceneToLoad);
        }


        CarListOpenWorld.Instance.LoadedScene = CarListOpenWorld.Instance.OpenWorldMapTest;
    }

    public void LoadSceneMenu()
    {
        SceneManager.LoadScene(sceneToLoadMenu);
    }
}

/*
[System.Serializable]
public class VehicleData
{
    [SerializeField] int vehicleType;
    [SerializeField] int weaponType;
    [SerializeField] int armorType;
    [SerializeField] string carName;
    [SerializeField] string carId;
    [SerializeField] int team;

    public int VehicleType { get => vehicleType; set => vehicleType = value; }
    public int WeaponType { get => weaponType; set => weaponType = value; }
    public int ArmorType { get => armorType; set => armorType = value; }
    public string CarName { get => carName; set => carName = value; }
    public string CarId { get => carId; set => carId = value; }
    public int Team { get => team; set => team = value; }
}
*/
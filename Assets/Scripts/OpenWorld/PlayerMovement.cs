using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMovement : Convoy
{

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    override protected void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                MoveTo(Setdestination());
            }                       
        }

        GetInteractableObjects();
        if (!canMove)
        {
            path.maxSpeed = 0;
        }
        
        Physics.Raycast(transform.position + new Vector3(0, 100, 0), -transform.up, out RaycastHit hit, 1000, OpenWorld._instance.Ground);
        viewRotation.transform.up = hit.normal;

        if(dir != Vector3.zero)
        {
            view.transform.forward = dir;
        }


        InteractableData.ObjectPosition = gameObject.transform.position;
        InteractableData.TargetDestPosition = TargetDest.transform.position;
        ManageAudio();

    }

    Transform Setdestination()
    {
        if (CanRaycast)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * 1000f, Color.magenta, 30f);
            if (Physics.Raycast(ray, out RaycastHit hit, 1000f))
            {
                if (hit.collider.gameObject.layer != LayerMask.NameToLayer("Ground"))
                {
                    if (hit.collider.gameObject.TryGetComponent(out InteractableObject intObj))
                    {
                        intObj.InteractableData.CanInteract = true;
                        TargetDest.transform.position = intObj.gameObject.transform.position;
                        TargetDest.transform.parent = intObj.gameObject.transform;
                    }
                    else
                    {
                        TargetDest.transform.parent = OpenWorld._instance.TargetParent.transform;
                        TargetDest.transform.position = hit.point;
                        TargetDest.transform.up = hit.normal;
                    }
                }
                else
                {
                    TargetDest.transform.parent = OpenWorld._instance.TargetParent.transform;
                    TargetDest.transform.position = hit.point;

                    TargetDest.transform.up = hit.normal;
                }

                return TargetDest.transform;

            }
            else
            {
                return TargetDest.transform;
            }
        }
        return TargetDest.transform;
    }

    

    void GetInteractableObjects()
    {
        Collider[] coll = Physics.OverlapSphere(transform.position, radius, OpenWorld._instance.InteractableObjects);
        if(coll.Length > 0)
        {
            InteractableObject point = coll[0].gameObject.GetComponent<InteractableObject>();
            if (point.InteractableData.CanInteract)
            {
                if(point.InteractableData.PointType == IntPointType.shop || point.InteractableData.PointType == IntPointType.TradingOutpost)
                {
                    point.InteractableData.CanInteract = false;
                    OpenWorld._instance.UpdateTraderInventory(point.InteractableData.InventarioShop);
                    CarListOpenWorld.Instance.IntercaableObjectID = point.InteractableData.InteractableID;
                    OpenWorld._instance.OpenCombatLog(point, 5);
                }
                else
                {
                    point.InteractableData.CanInteract = false;
                    CarListOpenWorld.Instance.IntercaableObjectID = point.InteractableData.InteractableID;
                    OpenWorld._instance.OpenCombatLog(point, 5);
                }
            }
        }
    }
}

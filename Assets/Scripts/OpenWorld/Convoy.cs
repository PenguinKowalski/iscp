using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using TMPro;

public class Convoy : InteractableObject
{
    [SerializeField] AIDestinationSetter destinationSetter;
    [SerializeField] protected AIPath path;
    [SerializeField] float arrivedDistance = 0.5f;
    [SerializeField] protected GameObject viewRotation;
    [SerializeField] TMP_Text partySize;
    [SerializeField] GameObject canvas;
    [SerializeField] AudioSource movmentAudio;
    [SerializeField] float v;


    [SerializeField] protected float radius = 5f;
    [SerializeField] protected Vector3 dir;
    Vector3 prevTarget;
    [SerializeField] float targetSpeed = 4f;
    [SerializeField] protected float speed;

    bool updateDestination;
    [SerializeField] protected bool isMoving, canMove = true, canRaycast = true;

    public bool IsMoving { get => isMoving; set => isMoving = value; }
    public bool CanMove { get => canMove; set => canMove = value; }
    public bool CanRaycast { get => canRaycast; set => canRaycast = value; }
    public float TargetSpeed { get => targetSpeed; set => targetSpeed = value; }

    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();
        StartCoroutine(CalculateSpeed());
        path = GetComponent<AIPath>();
        path.maxSpeed = targetSpeed;
        InteractableData.Description = possibiliDescrizioni[Random.Range(0, possibiliDescrizioni.Count)];

        if (InteractableData.PointType == IntPointType.shop)
        {
            InteractableData.Aggressive = "No";
            InteractableData.Trade = "Yes";
        }
        else
        {
            InteractableData.Aggressive = "Yes";
            InteractableData.Trade = "No";
        }
       

    }

    // Update is called once per frame
    override protected void Update()
    {
        base.Update();
        GetPlayer();
        if (IsConvoyAtDestination())
        {
            if(InteractableData.ObjectType != IntType.player && updateDestination)
            {
                for (int n = 0; n < OpenWorld._instance.InteractableObjectsObj.Count; n++)
                {
                    if (OpenWorld._instance.InteractableObjectsObj[n].InteractableData.ObjectType == IntType.persistent)
                    {
                        updateDestination = false;
                        MoveTo(OpenWorld._instance.InteractableObjectsObj[n].transform);
                        updateDestination = true;
                    }
                }
                if (InteractableData.PointType == IntPointType.aggressive)
                {
                    //Debug.Log("passive");
                    InteractableData.PointType = IntPointType.passive;
                }
            }
            else
            {
                OpenWorld._instance.RemoveInteractableObj(this.gameObject);
            }
        }
        
        Physics.Raycast(transform.position + new Vector3(0, 100, 0), -transform.up, out RaycastHit hit, 1000, OpenWorld._instance.Ground);
        viewRotation.transform.up = hit.normal;

        if(dir != Vector3.zero)
        {
            view.transform.forward = dir;
        }

        canvas.transform.LookAt(OpenWorld._instance.PlayerCamera.gameObject.transform);
        partySize.text = InteractableData.CarInfo.Count.ToString();

        ManageFogOfWar();
        ManageAudio();
    }

    public void MoveTo(Transform target)
    {
        destinationSetter.target = TargetDest.transform;
        TargetDest.transform.position = target.position;
    }

  

    bool IsConvoyAtDestination()
    {
        if(Vector3.Distance(gameObject.transform.position, TargetDest.transform.position) <= arrivedDistance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void GetPlayer()
    {
        bool close = Vector3.Distance(transform.position, OpenWorld._instance.Player.transform.position) < radius;

        if (close && InteractableData.PointType == IntPointType.aggressive)
        {
            if (!updateDestination)
            {
                prevTarget = TargetDest.transform.position;
                TargetDest.transform.position = OpenWorld._instance.Player.transform.position;
                MoveTo(TargetDest.transform);
                updateDestination = true;
            }
            TargetDest.transform.position = OpenWorld._instance.Player.transform.position;
        }
        else if (close && !updateDestination && (InteractableData.PointType == IntPointType.shop || InteractableData.PointType == IntPointType.passive))
        {
            if (!updateDestination)
            {
                prevTarget = TargetDest.transform.position;
                Vector3 dir1 = (OpenWorld._instance.Player.transform.position - transform.position).normalized;
                TargetDest.transform.position = transform.position + -dir1 * 50f;
                MoveTo(TargetDest.transform);
                updateDestination = true;
            }
            Vector3 dir = (OpenWorld._instance.Player.transform.position - transform.position).normalized;
            TargetDest.transform.position = transform.position + -dir * 50f;
        }
        else if(prevTarget != null && !close && updateDestination)
        {
            TargetDest.transform.position = prevTarget;
            MoveTo(TargetDest.transform);
            updateDestination = false;
        }
    }

    IEnumerator CalculateSpeed()
    {
        while (true)
        {
            Vector3 prevPos = transform.position;

            yield return new WaitForSeconds(0.05f);

            speed = Vector3.Distance(prevPos, transform.position) * 10f;
            dir = transform.position - prevPos;

            if(speed > 0.1)
            {
                isMoving = true;
            }
            else
            {
                isMoving = false;
            }
        }
    }

    public void ManageAudio()
    {
        if (isMoving)
        {
            if (v < 1)
            {
                v += Time.deltaTime * 2;
            }
            float pitchBrakesBraking = Mathf.Lerp(0f, 1f, v);

            movmentAudio.volume = pitchBrakesBraking;
        }
        else
        {
            if (v > 0)
            {
                //Debug.Log("sottraggo");
                v -= Time.deltaTime * 3;
            }
            float pitchBrakesNotBraking = Mathf.Lerp(0f, 1f, v);

            movmentAudio.volume = pitchBrakesNotBraking;
        }
    }
}

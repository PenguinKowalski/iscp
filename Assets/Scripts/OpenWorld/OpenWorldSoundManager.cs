using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenWorldSoundManager : MonoBehaviour
{
    [SerializeField] AudioClip openPopUp, closePopUp;
    [SerializeField] AudioSource openWorldPopUp;


    public void PlayOpenPopUp()
    {
        openWorldPopUp.PlayOneShot(openPopUp);
    }

    public void PlayClosePopUp()
    {
        openWorldPopUp.PlayOneShot(closePopUp);
    }
}

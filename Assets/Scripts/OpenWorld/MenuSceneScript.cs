using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSceneScript : MonoBehaviour
{
    [SerializeField] GameObject carList;
    [SerializeField] string openWorldMap;
    [SerializeField] string openWorldMapTest;

    // Start is called before the first frame update
    void Start()
    {
        if (CarListOpenWorld.Instance == null)
        {
            GameObject newCarList = Instantiate(carList);
            CarListOpenWorld.Instance.OpenWorldMap = openWorldMap;
            CarListOpenWorld.Instance.OpenWorldMapTest = openWorldMapTest;
        }
    }

    public void LoadSceneTest()
    {
        SceneManager.LoadScene(openWorldMapTest);
    }

    public void LoadScenePlay()
    {
        SceneManager.LoadScene(openWorldMap);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VehicleData
{
    [SerializeField] int vehicleType, weaponType, armorType, team, motorType, totalPrice;

    [SerializeField] string carName, carId;

    [SerializeField] bool isConvoy;

    [SerializeField] float healthPerc = 100;

    const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";

    public int VehicleType { get => vehicleType; set => vehicleType = value; }
    public int WeaponType { get => weaponType; set => weaponType = value; }
    public int ArmorType { get => armorType; set => armorType = value; }
    public string CarName { get => carName; set => carName = value; }
    public string CarId { get => carId; set => carId = value; }
    public int Team { get => team; set => team = value; }
    public bool IsConvoy { get => isConvoy; set => isConvoy = value; }
    public int MotorType { get => motorType; set => motorType = value; }
    public int TotalPrice { get => totalPrice; set => totalPrice = value; }
    public float HealthPerc { get => healthPerc; set => healthPerc = value; }

    public string RandomizeID()
    {
        string carID = "";

        for (int i = 0; i < 10; i++)
        {
            carID += glyphs[Random.Range(0, glyphs.Length)];
        }

        return carID;
    }
}

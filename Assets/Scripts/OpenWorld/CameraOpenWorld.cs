using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOpenWorld : MonoBehaviour
{
    [SerializeField] GameObject openWorldPlayer;
    [SerializeField] GameObject cameraPosition;
    [SerializeField] Vector3 lastpos;
    bool map, canZoom = true;
    [SerializeField] float rotationSenityvity = 1000, maxDist = 50f, minDist = 10f;

    public bool CanZoom { get => canZoom; set => canZoom = value; }
    public GameObject CameraPosition { get => cameraPosition; set => cameraPosition = value; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        cameraPosition.transform.position = openWorldPlayer.transform.position;
        RotateCamera();
        Zoom();
    }

    void RotateCamera()
    {
        if (Input.GetButton("Fire2"))
        {
            cameraPosition.transform.rotation = cameraPosition.transform.rotation * Quaternion.Euler(0, Input.GetAxis("Mouse X") * Time.deltaTime * rotationSenityvity, 0);
        }
    }

    void Zoom()
    {
        if (CanZoom)
        {
            float dist = Vector3.Distance(cameraPosition.transform.position, gameObject.transform.position);

            if (dist > minDist && dist < maxDist)
            {
                gameObject.transform.localPosition += new Vector3(0, Input.mouseScrollDelta.y, -Input.mouseScrollDelta.y);
            }
            else if (dist <= minDist && Input.mouseScrollDelta.y > 0)
            {
                gameObject.transform.localPosition += new Vector3(0, Input.mouseScrollDelta.y, -Input.mouseScrollDelta.y);
            }
            else if (dist >= maxDist && Input.mouseScrollDelta.y < 0)
            {
                gameObject.transform.localPosition += new Vector3(0, Input.mouseScrollDelta.y, -Input.mouseScrollDelta.y);
            }

            if (dist >= maxDist && !map && Input.mouseScrollDelta.y > 0)
            {
                lastpos = gameObject.transform.localPosition;
                gameObject.transform.localPosition = new Vector3(0, maxDist, 0);
                gameObject.transform.localRotation = Quaternion.Euler(90, 0, 0);
                map = true;
            }
            else if (Input.mouseScrollDelta.y < 0 && map)
            {
                gameObject.transform.localPosition = lastpos;
                gameObject.transform.localRotation = Quaternion.Euler(45, 0, 0);
                map = false;
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestrucibleBody : MonoBehaviour
{

    public float breakingForce = 10.0f;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Rigidbody other = collision.rigidbody;
        if (other != null)
        {
            float collisionForce = Vector3.Dot(collision.relativeVelocity, collision.contacts[0].normal) * other.mass;
            if (collisionForce > breakingForce)
            {
                rb.isKinematic = false;
                other.velocity = collision.relativeVelocity;
            }
        }
    }
}

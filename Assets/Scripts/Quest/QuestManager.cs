using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{

    QuestData GetQuest(string questID)
    {
        for(int i = 0; i < CarListOpenWorld.Instance.ActiveQuests.Count; i++)
        {
            if(CarListOpenWorld.Instance.ActiveQuests[i].QuestID == questID)
            {
                return CarListOpenWorld.Instance.ActiveQuests[i];
            }
        }
        return null;
    }
}

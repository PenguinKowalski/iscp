using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Quest
{
    [SerializeField] List<QuestStep> steps = new List<QuestStep>();
    [SerializeField] string questID;
    [SerializeField] string titleID;
    [SerializeField] string actorName;
    [SerializeField] string questDescription;
    [SerializeField] Sprite actorImage;

    List<MaterialQuantity> Ricompense = new List<MaterialQuantity>();

    public string QuestID { get => questID; set => questID = value; }
    public string TitleID { get => titleID; set => titleID = value; }
    public string ActorName { get => actorName; set => actorName = value; }
    public string QuestDescription { get => questDescription; set => questDescription = value; }
    public Sprite ActorImage { get => actorImage; set => actorImage = value; }
    public List<QuestStep> Steps { get => steps; set => steps = value; }

    public void AcceptQuest()
    {
        QuestData newActiveQuest = new QuestData(steps[0].DialogueID, questID, steps[0]);
        CarListOpenWorld.Instance.ActiveQuests.Add(newActiveQuest);

        steps[0].ActivateStep(newActiveQuest);
    }

    public void ResumeQuest(string resumeStepID)
    {
        for(int i = 0; i < steps.Count; i++)
        {
            if(resumeStepID == steps[i].StepID)
            {
               // steps[i].ActivateStep();
            }
        }
    }

    public void CompleteQuest()
    {
        AddRicompense();
        CarListOpenWorld.Instance.ActiveQuests.Remove(CarListOpenWorld.Instance.GetQuestData(QuestID));
        CarListOpenWorld.Instance.CompletedQuests.Add(this);
    }

    public void FailQuest()
    {
        CarListOpenWorld.Instance.ActiveQuests.Remove(CarListOpenWorld.Instance.GetQuestData(QuestID));
        CarListOpenWorld.Instance.FailedQuests.Add(this);
    }

    public QuestStep GetQuestStep(string id)
    {
        for(int i = 0; i < steps.Count; i++)
        {
            if(steps[i].StepID == id)
            {
                return steps[i];
            }
        }
        return null;
    }

    void AddRicompense()
    {
        for (int n = 0; n < Ricompense.Count; n++)
        {
            OpenWorld._instance.AddItem(Ricompense[n], CarListOpenWorld.Instance.PlayerInventory, null);
            //Debug.Log(obj.InteractableData.Ricompense[n].Quantity);
        }
    }
}

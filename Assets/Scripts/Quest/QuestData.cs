using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestData
{
    [SerializeField] string originID;
    [SerializeField] string objectiveID;
    [SerializeField] QuestStep step;
    [SerializeField] List<QuestStep> oldSteps = new List<QuestStep>();

    [SerializeField] string dialogueID;
    [SerializeField] string questID;

    [SerializeField] bool isActive;

    public bool IsActive { get => isActive; set => isActive = value; }
    public string DialogueID { get => dialogueID; set => dialogueID = value; }
    public string QuestID { get => questID; set => questID = value; }
    public QuestStep Step { get => step; set => step = value; }
    public List<QuestStep> OldSteps { get => oldSteps; set => oldSteps = value; }
    public string ObjectiveID { get => objectiveID; set => objectiveID = value; }

    public override bool Equals(object obj)
    {
        return Equals(obj as QuestData);
    }

    public bool Equals(QuestData other)
    {
        return other != null &&
               questID == other.questID;
    }

    public QuestData(string startDialogueID, string _questID, QuestStep startStep)
    {
        Step = startStep;
        DialogueID = startDialogueID;
        QuestID = _questID;
    }
    public void NextStep(string answer)
    {
        //QuestStep steppino =  CarListOpenWorld.Instance.GetQuest(questID).GetQuestStep(stepID);

        Debug.Log("next step with answer: " + answer + ", of quest :" + this.questID);
        isActive = true;

        OldSteps.Add(step);

        QuestStep nextSteppino = CarListOpenWorld.Instance.GetNextQuestStep(QuestID,step.StepID,answer);

        nextSteppino.ActivateStep(this);


    }



}

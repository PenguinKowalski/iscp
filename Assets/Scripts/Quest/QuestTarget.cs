using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestTarget : MonoBehaviour
{
    [SerializeField] string stepID;
    [SerializeField] string questID;
    [SerializeField] PossibleActions targetType = PossibleActions.bounty;
    [SerializeField] StepType stepType = StepType.questPassage;

    [SerializeField] bool isBounty;

    public string StepID { get => stepID; set => stepID = value; }
    public string QuestID { get => questID; set => questID = value; }
    public bool IsBounty { get => isBounty; set => isBounty = value; }
    internal PossibleActions TargetType { get => targetType; set => targetType = value; }
    internal StepType StepType { get => stepType; set => stepType = value; }

}

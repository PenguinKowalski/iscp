using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveQuestTraker : MonoBehaviour
{
    [SerializeField] RectTransform pointerRectTransform;
    [SerializeField] CameraOpenWorld camOp;
    

    [SerializeField] string objectID;
    [SerializeField] GameObject trakedGameobject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SetPointerPosition();
        GetNewObj();
    }

    void SetPointerPosition()
    {
        if(trakedGameobject != null)
        {
            Vector3 dir = trakedGameobject.gameObject.transform.position - camOp.CameraPosition.transform.position;

            float angle = Vector3.SignedAngle(dir, camOp.CameraPosition.transform.forward, new Vector3(0, 1, 0));

            Vector3 targetPositionScreenPoint = Camera.main.WorldToScreenPoint(trakedGameobject.transform.position);

            bool isOffScreen = targetPositionScreenPoint.x <= 0 || targetPositionScreenPoint.x >= Screen.width || targetPositionScreenPoint.y <= 0 || targetPositionScreenPoint.y >= Screen.height;
            Debug.Log(isOffScreen);
            
            if (isOffScreen)
            {
                Vector3 cappedTargetScreenPos = targetPositionScreenPoint;

                if (cappedTargetScreenPos.x <= 0) cappedTargetScreenPos.x = Screen.width * 0.1f;
                if (cappedTargetScreenPos.x >= Screen.width) cappedTargetScreenPos.x = Screen.width * 0.9f;
                if (cappedTargetScreenPos.y <= 0) cappedTargetScreenPos.y = Screen.height * 0.1f;
                if (cappedTargetScreenPos.y >= Screen.height) cappedTargetScreenPos.y = Screen.height * 0.9f;

                pointerRectTransform.position = cappedTargetScreenPos;
                pointerRectTransform.localEulerAngles = new Vector3(0, 0, angle + 180);
            }
            else 
            {
                pointerRectTransform.position = targetPositionScreenPoint;
                pointerRectTransform.localEulerAngles = new Vector3(0, 0, 0);
            }
            

            pointerRectTransform.gameObject.SetActive(true);

        }
        else
        {
            pointerRectTransform.gameObject.SetActive(false);
        }
    }

    void GetNewObj()
    {
        string obj = null;

        if (CarListOpenWorld.Instance.GetQuestData(CarListOpenWorld.Instance.TrakedQuestID) != null)
        {
            obj = CarListOpenWorld.Instance.GetQuestData(CarListOpenWorld.Instance.TrakedQuestID).ObjectiveID;
        }
        else
        {
            obj = null;
        }

        if (objectID == null)
        {
            objectID = obj;
            TrakNewObj();
        }
        else if(obj != null)
        {
            if (objectID == obj)
            {

            }
            else
            {
                objectID = obj;
                TrakNewObj();
            }
        }
        else
        {
            trakedGameobject = null;
        }
        
    }

    public static float GetAngleFromVectorFloat(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;

        return n;
    }



    void TrakNewObj()
    {
        trakedGameobject = OpenWorld._instance.FindInteractableObj(objectID).gameObject;
    }
}

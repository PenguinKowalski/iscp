using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum PossibleActions { deliveryStart, deliveryEnd, bounty, interact };
enum StepType { questComplete, questFailed, questPassage }

[System.Serializable]
public class QuestStep 
{
    [SerializeField] string stepID;
    [SerializeField] string questID;
    [SerializeField] string dialogueID;

    [SerializeField] List<AnswerFlow> flow = new List<AnswerFlow>();
    
    [SerializeField] string descriptionID;
    [SerializeField] float distance;
    [SerializeField] IntPointType type;
    [SerializeField] string objectiveID;
    
    [SerializeField] PossibleActions stepActionType = PossibleActions.interact;
    [SerializeField] StepType stepType = StepType.questPassage;

    public string StepID { get => stepID; set => stepID = value; }
    public string DialogueID { get => dialogueID; set => dialogueID = value; }
    public string DescriptionID { get => descriptionID; set => descriptionID = value; }
    public string ObjectiveID { get => objectiveID; set => objectiveID = value; }

    public void ActivateStep(QuestData fromQuest)
    {
        switch (stepActionType)
        {
            case PossibleActions.bounty:

                if(ObjectiveID == "")
                {
                    IntType objType = IntType.dynamic;
                    float angle = Random.Range(0, 2 * Mathf.PI);
                    float radius = distance;
                    float x = Mathf.Cos(angle) * radius;
                    float y = Mathf.Sin(angle) * radius;

                    Vector3 SpawnPos = OpenWorld._instance.Player.transform.position + new Vector3(x, 0, y);

                    InteractableObject ObjTarg = OpenWorld._instance.SpawnInteractableObject(type, objType, SpawnPos);
                    /*
                    QuestTarget targ = ObjTarg.gameObject.AddComponent<QuestTarget>();
                    targ.QuestID = questID;
                    targ.StepID = stepID;
                    targ.TargetType = stepActionType;
                    targ.StepType = stepType;
                    targ.IsBounty = true;
                    */
                    ObjectiveID = ObjTarg.InteractableData.InteractableID;

                    ObjTarg.AddQuest(fromQuest);

                    //Debug.Log("spawnoNemico" + SpawnPos);

              //      UpdateQuest();

                }
                else
                {
                    /*
                    QuestTarget targ = OpenWorld._instance.FindInteractableObj(ObjectiveID).gameObject.AddComponent<QuestTarget>();
                    targ.QuestID = questID;
                    targ.StepID = stepID;
                    targ.TargetType = stepActionType;
                    targ.StepType = stepType;
                    targ.IsBounty = true;
                    */

                    InteractableObject obj = OpenWorld._instance.FindInteractableObj(ObjectiveID);

                    obj.AddQuest(fromQuest);

                    
                    //la vittoria ha sempre indice 0 sconfitta 1;


                    QuestStep steppino = CarListOpenWorld.Instance.GetQuest(questID).GetQuestStep(stepID);
                    //Debug.Log("Rinnovo Quest " + steppino.ObjectiveID);

              //      UpdateQuest();

                    if (steppino.ObjectiveID == CarListOpenWorld.Instance.IntercaableObjectID)
                    {
                        if(CarListOpenWorld.Instance.HasWon == true)
                        {
                            CarListOpenWorld.Instance.GetQuestData(questID).NextStep("0");
                        }
                        else
                        {
                            CarListOpenWorld.Instance.GetQuestData(questID).NextStep("1");
                        }
                    }

                }

                break;
           
            case PossibleActions.interact:
                if(ObjectiveID != null)
                {
                    
                    if(flow.Count > 0)
                    {
                        //Debug.Log(ObjectiveID);
                        /*
                        QuestTarget targ = OpenWorld._instance.FindInteractableObj(ObjectiveID).gameObject.AddComponent<QuestTarget>();
                        targ.QuestID = questID;
                        targ.StepID = stepID;
                        targ.TargetType = stepActionType;
                        targ.StepType = stepType;
                        */

                        InteractableObject obj = OpenWorld._instance.FindInteractableObj(ObjectiveID);
                        obj.AddQuest(fromQuest);
                    }
                    


                  //  UpdateQuest();
                }               
                break;
        }
    }

    public string NextStep(string answer)
    {
        int value = int.Parse(answer);

        for(int i = 0; i < flow.Count; i++)
        {
            if (flow[i].Answer == value)
            {
                return flow[i].AnswerStepID;
            }
        }
        return null;
    }
    /*
    public void UpdateQuest()
    {
        if(stepType == StepType.questComplete)
        {
            CarListOpenWorld.Instance.GetQuest(questID).CompleteQuest();
        }
        else if(stepType == StepType.questFailed)
        {
            CarListOpenWorld.Instance.GetQuest(questID).FailQuest();
        }
        else
        {
            for (int i = 0; i < CarListOpenWorld.Instance.ActiveQuests.Count; i++)
            {
                if (questID == CarListOpenWorld.Instance.ActiveQuests[i].QuestID)
                {
                    CarListOpenWorld.Instance.ActiveQuests[i].ObjectiveID = ObjectiveID;
                    CarListOpenWorld.Instance.ActiveQuests[i].StepID = stepID;
                    CarListOpenWorld.Instance.ActiveQuests[i].DialogueID = dialogueID;

                }
            }
        }
    
    }
    */
}


[System.Serializable]
public class AnswerFlow
{
    [SerializeField] string answerStepID;
    [SerializeField] int answer;

    public string AnswerStepID { get => answerStepID; set => answerStepID = value; }
    public int Answer { get => answer; set => answer = value; }
}



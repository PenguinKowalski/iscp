﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAI : MonoBehaviour
{
    [SerializeField] InputManager Car;

    [SerializeField] GameObject Objective;

    Transform carPosition;

    [SerializeField] float angle;
    [SerializeField] Vector3 dir;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        carPosition = Car.gameObject.transform;
        GetAngle();
        Steer();
    }


    void GetAngle()
    {
        dir = Objective.transform.position - carPosition.position;
        //angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;

        angle = Vector3.SignedAngle(dir, carPosition.forward, new Vector3(0,1,0));
    }

    void Steer()
    {
        if(angle > 0)
        {
            Car.Steering = -angle / 90;
        }
        else if(angle < 0)
        {
            Car.Steering = -angle / 90;
        }
    }
}

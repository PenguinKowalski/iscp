using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using Adrenak.Tork;

public class Car : MonoBehaviour, IDestroyable
{
    [SerializeField] int team, motorInt;
    [SerializeField] int status;
    [SerializeField] float health, speed, forceStabilizer = 1, criticalDistance, criticalMulitplier;
    [SerializeField] bool isPlayer, isDead, canFlip = true, isConvoy;
    [SerializeField] Weapon.WeaponType selectedGroup;
    [SerializeField] float healthMax, minHealthRetreat;
    [SerializeField] float healthPerc;
    [SerializeField] string carName, carID;
    [SerializeField] float[] resistenze;
    [SerializeField] LayerMask criticalLayer;
    [SerializeField] List<GameObject> spawnPoints = new List<GameObject>();
    [SerializeField] List<GameObject> armorParts = new List<GameObject>();
    //[SerializeField] Camera carCam;
    [SerializeField] List<GameObject> outRiggers = new List<GameObject>();
    [SerializeField] List<AnimationCurve> motorType = new List<AnimationCurve>();
    Vehicle veicolo;
    [SerializeField] bool isSelectingGroup = false, isSelectingTarget = false;
    Sprite weaponImage;

    //Componenti da attivare/disattivare quando isPlayer � true/false
    InputManager inputManager;
    CarSoundEffects carSounds;
    MediumAI mediumAI;
    Weapon weapon;
    Car killerEnemy;
    Car targetCar;
    Car prevTarget;
    Outline outline;
    Rigidbody rb;
    RaycastModifier raycastModifier;
    [SerializeField] GameObject cameraPivot;
    [SerializeField] RotateCamera cam;

    //Lista di macchine pericolose
    [SerializeField] List<DangerousCar> dangerousCar = new List<DangerousCar>();
    [SerializeField] Car mostDangerousCar;
    [SerializeField] float aggroTreshold = 20;

    [Header("Moving Obstacles Raycast variables")]
    [SerializeField] float rayHeight;
    [SerializeField] float rayLenghtFront, rayDistanceFront, raySpacingFront, minSpeedManouver;
    [SerializeField] float rayLenghtSide, rayDistanceSide, raySpacingSide;

    #region variabiliPubbliche
    public int Team { get => team; set => team = value; }
    public float Health { get => health; set => health = value; }
    public bool IsPlayer { get => isPlayer; set => isPlayer = value; }
    public Car MostDangerousCar { get => mostDangerousCar; set => mostDangerousCar = value; }
    public RotateCamera Cam { get => cam; set => cam = value; }
    public MediumAI MediumAI { get => mediumAI; set => mediumAI = value; }
    public float HealthPerc { get => healthPerc; set => healthPerc = value; }
    public float RayHeight { get => rayHeight; set => rayHeight = value; }
    public float RayLenghtFront { get => rayLenghtFront; set => rayLenghtFront = value; }
    public float RayDistanceFront { get => rayDistanceFront; set => rayDistanceFront = value; }
    public float RaySpacingFront { get => raySpacingFront; set => raySpacingFront = value; }
    public float MinSpeedManouver { get => minSpeedManouver; set => minSpeedManouver = value; }
    public float RayLenghtSide { get => rayLenghtSide; set => rayLenghtSide = value; }
    public float RayDistanceSide { get => rayDistanceSide; set => rayDistanceSide = value; }
    public float RaySpacingSide { get => raySpacingSide; set => raySpacingSide = value; }
    public float MinHealthRetreat { get => minHealthRetreat; set => minHealthRetreat = value; }
    public string CarName { get => carName; set => carName = value; }
    public int Status { get => status; set => status = value; }
    public float Speed { get => speed; set => speed = value; }
    public Weapon.WeaponType SelectedGroup { get => selectedGroup; set => selectedGroup = value; }
    public bool IsSelectingGroup { get => isSelectingGroup; set => isSelectingGroup = value; }
    public Weapon Weapon { get => weapon; set => weapon = value; }
    public Outline Outline { get => outline; set => outline = value; }
    public bool IsDead { get => isDead; set => isDead = value; }
    public bool IsConvoy { get => isConvoy; set => isConvoy = value; }
    public List<GameObject> SpawnPoints { get => spawnPoints; set => spawnPoints = value; }
    //public Camera CarCam { get => carCam; set => carCam = value; }
    public List<GameObject> ArmorParts { get => armorParts; set => armorParts = value; }
    public string CarID { get => carID; set => carID = value; }
    public Car TargetCar { get => targetCar; set => targetCar = value; }
    public List<GameObject> OutRiggers { get => outRiggers; set => outRiggers = value; }
    public int MotorInt { get => motorInt; set => motorInt = value; }
    public Sprite WeaponImage { get => weaponImage; set => weaponImage = value; }

    #endregion variabiliPubbliche

    //Implementazione della interfaccia Idestroyable
    public void ChangeHealth(float differenza, float[] moltiplicatore, Car attackingCar, RaycastHit? raycastHit, Ray? ray, float bulletSpeed, float raycastLength = -1f)
    {
        bool isCritical = false;
        if (!Weapon.IsCarEnemy(attackingCar, this))
        {
            return;
        }

        for (int i = 0; i < moltiplicatore.Length; i++)
        {
           
            if (health > 0)
            {
                health -= differenza * moltiplicatore[i] * resistenze[i];
               
            }
            else
            {
                if (raycastHit.HasValue)
                {
                    Death(raycastHit.Value);
                }
                else
                {
                    Death(null);
                }
            }
        }
        //Debug.LogError(raycastHit);
        if (raycastHit.HasValue)
        {
           // Debug.LogError("CRITICAL finding");
            if (raycastLength < 0)
            {
                if (Physics.Raycast(raycastHit.Value.point, ray.Value.direction, criticalDistance, criticalLayer))
                {
                    isCritical = true;
                    for (int i = 0; i < moltiplicatore.Length; i++)
                    {
                       // Debug.LogError("CRITICAL");
                        if (health > 0)
                        {
                            health -= differenza * moltiplicatore[i] * resistenze[i] * criticalMulitplier;
                        }
                        else
                        {
                            Death(raycastHit.Value);
                        }
                    }
                }
            }
            else
            {
                if (Physics.Raycast(ray.Value.origin, ray.Value.direction, raycastLength, criticalLayer))
                {
                    isCritical = true;
                    for (int i = 0; i < moltiplicatore.Length; i++)
                    {
                        if (health > 0)
                        {
                            health -= differenza * moltiplicatore[i] * resistenze[i] * criticalMulitplier;
                        }
                        else
                        {
                            if (raycastHit.HasValue)
                            {
                                Death(raycastHit.Value);
                            }
                        }
                    }
                }
            }
        }
        if (ray.HasValue)
        {
            rb.AddForceAtPosition(ray.Value.direction * bulletSpeed * forceStabilizer, raycastHit.Value.point, ForceMode.Impulse);
        }
        UpdateDamages(attackingCar, differenza);
        killerEnemy = attackingCar;
        if (attackingCar.IsPlayer)
        {
            Arena._instance.HitMarkerEffect(isCritical);
        }
    }
    //Implementazione della interfaccia Idestroyable
    public void Death(RaycastHit? raycastHit)
    {
        return;
    }

    void Start()
    {
        health = healthMax * HealthPerc / 100;
        veicolo = GetComponent<Vehicle>();
        if(motorInt != 0)
        {
            veicolo.MotorTorqueVsSpeed = motorType[motorInt];
        }
        inputManager = GetComponent<InputManager>();
        carSounds = GetComponent<CarSoundEffects>();
        mediumAI = GetComponentInChildren<MediumAI>();
        raycastModifier = GetComponent<RaycastModifier>();
        weapon = GetComponentInChildren<Weapon>();
        outline = GetComponentInChildren<Outline>();
        rb = GetComponent<Rigidbody>();
        StartCoroutine(FlipCoolDown());
    }

    // Update is called once per frame
    void Update()
    {

        CheckIfPlayer();

        if (dangerousCar.Count > 0 && health >= 0)
        {
            mostDangerousCar = GetMostDangerousCar();
        }

        if (isPlayer)
        {
            if (Input.GetKeyDown(KeyCode.F2) && isSelectingTarget)
            {
                AssignOrderToGroup(1);
                isSelectingTarget = false;
            }
            
            else
            {
                AssignOrders();
            }

            if (isSelectingTarget)
            {
                targetCar = Arena._instance.CameraScript.LookObject;
                //Debug.Log(targetCar);
                if (targetCar != null && targetCar.Team != team)
                {
                    targetCar.Outline.OutlineWidth = 10;
                    prevTarget = targetCar;
                }
                else if((targetCar == null || prevTarget != targetCar) && prevTarget != null)
                {
                    prevTarget.Outline.OutlineWidth = 0;
                }     
            }
            else if(prevTarget != null)
            {
                prevTarget.Outline.OutlineWidth = 0;
            }
        }

        healthPerc = (health / healthMax) * 100;


        float d = Vector3.Angle(gameObject.transform.forward, rb.velocity);
        if(d < 90)
        {
            speed = rb.velocity.magnitude;
        }
        else
        {
            speed = rb.velocity.magnitude * -1;
        }

        
        

       

        if (health <= 0 && !isDead)
        {
            if (isPlayer)
            {
                Arena._instance.NextCar();

                inputManager.IsDead = true;
            }
            Arena._instance.RemoveCar(team, this);
            //Debug.Log("rimuovo");
            //Debug.Log("aggiungo il log");
            if(killerEnemy != null)
            {
                Arena._instance.UpdateLog(carName + "(" + weapon.WeaponName + ")" + " � stato distrutto da " + killerEnemy.carName + "(" + killerEnemy.weapon.WeaponName + ")");
            }
            

            isDead = true;
        }
        
    }



    //Restituisce la macchina pi� pericolosa 
    Car GetMostDangerousCar()
    {
        float damageTakenCurrent = 0;
        Car mostDangerous = null;

        for (int i = 0; i < dangerousCar.Count; i++)
        {
            float ct = dangerousCar[i].CurrentTimer;

            if (dangerousCar[i].CarEnemy.Health <= 0)
            {
                RemoveDangerousCar(dangerousCar[i]);
            }
            else if (ct > 0)
            {
                dangerousCar[i].CurrentTimer -= Time.deltaTime;
                if (dangerousCar[i].DamageTaken > damageTakenCurrent)
                {
                    damageTakenCurrent = dangerousCar[i].DamageTaken;
                    if (damageTakenCurrent > aggroTreshold)
                    {
                        mostDangerous = dangerousCar[i].CarEnemy;
                    }
                }
            }
            else
            {
                RemoveDangerousCar(dangerousCar[i]);
            } 
        }

        return mostDangerous;
    }

    //Controlla se la macchina � controllata dal giocatore
    void CheckIfPlayer()
    {
        if (isPlayer)
        {
            inputManager.AI = false;
            mediumAI.IsAI = false;
            raycastModifier.enabled = false;
            //cameraPivot.SetActive(true);
        }
        else
        {
            inputManager.AI = true;
            mediumAI.IsAI = true;
            raycastModifier.enabled = true;
            //cameraPivot.SetActive(false);
        }
    }

    //Aggiunge alla lista macchine pericolose
    void AddDangerousCar(Car _car, float _damageTaken)
    {
        DangerousCar dg = new DangerousCar();
        dg.CarEnemy = _car;
        dg.DamageTaken = _damageTaken;
        dangerousCar.Add(dg);
    }

    //Rimuove dalla lista macchine pericolose
    void RemoveDangerousCar(DangerousCar dgCar)
    {
        dangerousCar.Remove(dgCar);
    }

    //Aggiorna la lista di Macchine pericolose
    void UpdateDamages(Car _attackingCar, float differenza)
    {
        if (dangerousCar.Count == 0)
        {
            AddDangerousCar(_attackingCar, differenza);
        }
        else
        {
            bool carIsAlreadyPresent = false;
            int n = 0;

            for (int i = 0; i < dangerousCar.Count; i++)
            {
                if (dangerousCar[i].CarEnemy == _attackingCar)
                {
                    carIsAlreadyPresent = true;
                    n = i;
                    break;
                }
                else
                {
                    carIsAlreadyPresent = false;
                }
            }

            if (carIsAlreadyPresent)
            {
                dangerousCar[n].DamageTaken += differenza;
                dangerousCar[n].CurrentTimer = dangerousCar[n].Timer;
            }
            else
            {
                AddDangerousCar(_attackingCar, differenza);
            }
        }
    }

    //Gestione dell'assegnazione degli ornini
    void AssignOrders()
    {
        if (!isSelectingGroup && !isSelectingTarget)
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                AssignGroup(Weapon.WeaponType._long);
                carSounds.PlayOrderAudio("gruppo1");
            }
            else if (Input.GetKeyDown(KeyCode.F2))
            {
                AssignGroup(Weapon.WeaponType._medium);
                carSounds.PlayOrderAudio("gruppo2");
            }
            else if (Input.GetKeyDown(KeyCode.F3))
            {
                AssignGroup(Weapon.WeaponType._melee);
                carSounds.PlayOrderAudio("gruppo3");
            }
            else if (Input.GetKeyDown(KeyCode.F4))
            {
                AssignGroup(Weapon.WeaponType._short);
                carSounds.PlayOrderAudio("gruppo4");
            }
        }
        else if(isSelectingGroup && !isSelectingTarget)
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                AssignOrderToGroup(0);
                carSounds.PlayOrderAudio("attack");
            }
            else if (Input.GetKeyDown(KeyCode.F2))
            {
                isSelectingTarget = true;
                carSounds.PlayOrderAudio("focus");
            }
            else if (Input.GetKeyDown(KeyCode.F3))
            {
                AssignOrderToGroup(2);
                carSounds.PlayOrderAudio("follow");
            }
            else if (Input.GetKeyDown(KeyCode.F4))
            {
                AssignOrderToGroup(3);
                carSounds.PlayOrderAudio("flee");
            }
            else if (Input.GetKeyDown(KeyCode.F5))
            {
                AssignOrderToGroup(4);
                carSounds.PlayOrderAudio("stay");
            }
        }   
    }

    //Classe macchina piu pericolosa
    [System.Serializable]
    class DangerousCar
    {
        [SerializeField] Car carEnemy;
        [SerializeField] float damageTaken;
        [SerializeField] float timer = 10;
        [SerializeField] float currentTimer = 10;

        public float DamageTaken { get => damageTaken; set => damageTaken = value; }
        public Car CarEnemy { get => carEnemy; set => carEnemy = value; }
        public float CurrentTimer { get => currentTimer; set => currentTimer = value; }
        public float Timer { get => timer; set => timer = value; }
    }

    //Assegnazione del gruppo
    void AssignGroup(Weapon.WeaponType group)
    {
        selectedGroup = group;
        isSelectingGroup = true;
    }

    //Assegnazione dell'ordine al gruppo
    void AssignOrderToGroup(int order)
    {
        isSelectingGroup = false;
        List<Car> cars = Arena._instance.GetCarOfSameGroup(team, selectedGroup);
        for (int i = 0; i < cars.Count; i++)
        {
            if(order == 0)
            {
                cars[i].mediumAI.PlayerCommand = MediumAI.playerCommands.attack;
                //Arena._instance.UpdateLog(cars[i].carName + "(" + weapon.WeaponName + ")" + " va all' attacco");
            }
            else if (order == 1)
            {
                cars[i].mediumAI.PlayerCommand = MediumAI.playerCommands.focusAttack;


            }
            else if (order == 2)
            {
                cars[i].mediumAI.PlayerCommand = MediumAI.playerCommands.follow;
                //Arena._instance.UpdateLog(cars[i].carName + "(" + weapon.WeaponName + ")" + " segue il giocatore");
            }
            else if (order == 3)
            {
                cars[i].mediumAI.PlayerCommand = MediumAI.playerCommands.flee;
                //Arena._instance.UpdateLog(cars[i].carName + "(" + weapon.WeaponName + ")" + " scappa");
            }
            else if (order == 4)
            {
                cars[i].mediumAI.PlayerCommand = MediumAI.playerCommands.stay;
            }

            cars[i].mediumAI.ManageCommands();
        }
    }

    IEnumerator FlipCoolDown()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.F) && canFlip && IsPlayer)
            {
                this.gameObject.transform.position += new Vector3(0, 3, 0);
                this.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
                rb.velocity = new Vector3(0, 0, 0);
                Arena._instance.FlipCar(this);
                canFlip = false;

                yield return new WaitForSeconds(10);

                canFlip = true;
            }
            else
            {
                yield return null;
            }
        }
    }
}

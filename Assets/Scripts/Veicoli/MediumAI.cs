﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using Adrenak.Tork;

public class MediumAI : MonoBehaviour
{
    Seeker seeker;
    InputManager carInputs;
    Rigidbody rb;
    protected Path path;
    protected Car car;
    [SerializeField] GameObject carGameobject;
    [SerializeField] protected Weapon weapon;

    protected Vehicle vehicle;
    
    [SerializeField] protected bool isAI; 

    [SerializeField] protected LayerMask Obstacle;
    [SerializeField] protected LayerMask Cars;

    [SerializeField] protected Vector3 targetAttack;
    protected Vector3 targetMovement;
    [SerializeField] protected GameObject movingTarget;
    protected GameObject shootingTarget;
    protected MediumAI movingTargetAI;
    protected MediumAI shootingTargetAI;

    [SerializeField]protected bool obstacleFront, obstacleBack, carFront, carBack, canShoot, canUpdateTarget, canAggro = true, isAggro, isStronger, enemyTooClose, enemyTooFar, isInjured;
    protected float maxTargetDistance, minTargetDistance, targetDistance, minHealthRetreat;
    protected float health;

    float rayHeight;
    float rayLenghtFront, rayDistanceFront, raySpacingFront, minSpeedManouver;
    float rayLenghtSide, rayDistanceSide, raySpacingSide;


    public enum AIStates {movement, manouverObstacles, manouverAttack, flee, Idle, death}
    [SerializeField] protected AIStates aIState = AIStates.movement;


    public enum playerCommands {attack, focusAttack, follow, flee, stay}
    [SerializeField] protected playerCommands playerCommand = playerCommands.stay;


    [SerializeField] protected List<Vector3> PathVectors = new List<Vector3>();

    public Vector3 TargetAttack { get => targetAttack; set => targetAttack = value; }
    public AIStates AIState { get => aIState; set => aIState = value; }
    public bool IsAI { get => isAI; set => isAI = value; }
    public playerCommands PlayerCommand { get => playerCommand; set => playerCommand = value; }

    protected virtual void Start()
    {
        seeker = GetComponentInParent<Seeker>();
        rb = GetComponentInParent<Rigidbody>();
        vehicle = GetComponentInParent<Vehicle>();
        car = GetComponentInParent<Car>();
        weapon = GetComponent<Weapon>();
        carInputs = GetComponentInParent<InputManager>();
        carGameobject = car.gameObject;

        GetNewTargetMovement();
        GetNewTargetShooting();
        if (car.IsConvoy)
        {
            movingTarget = Arena._instance.ConvoyTarget;
            //movingTargetAI = Arena._instance.ConvoyTarget.GetComponent<MediumAI>();
            playerCommand = playerCommands.attack;
        }
        

        StartCoroutine(UpdateDestination());
        StartCoroutine(UpdateNearestTarget());
        StartCoroutine(UpdateAggroTarget());

        ManageCommands();
        setObstaclesRays();
        SetMinMaxDistance();
    }

    protected virtual void Update()
    {
        health = car.Health;
        if (isAI)
        {
            if (path.vectorPath != null)
            {
                PathVectors = path.vectorPath;
            }

            UpdateLogicBools();
            GetObstacles();
            ManageStates();

            if (!car.IsConvoy)
            {
                if (movingTargetAI.AIState == AIStates.death)
                {
                    GetNewTargetMovement();
                    GetNewTargetShooting();
                }
            }
        }
        if (health <= 0)
        {
            aIState = AIStates.death;
        }
    }

    //Manage degli stati in base ai comandi assegnati
    public virtual void ManageCommands()
    {
        if(playerCommand == playerCommands.attack)
        {
            GetNewTargetMovement();
            GetNewTargetShooting();

            aIState = AIStates.movement;
            car.Status = 0;
        }
        else if(playerCommand == playerCommands.focusAttack)
        {
            if (GetFocusTarget())
            {
                aIState = AIStates.movement;
            }
            car.Status = 1;
        }
        else if (playerCommand == playerCommands.follow)
        {
            movingTarget = Arena._instance.GetPlayerCar().gameObject;

            aIState = AIStates.movement;
            car.Status = 2;
        }

        else if (playerCommand == playerCommands.flee)
        {
            GetNewTargetMovement();
            GetNewTargetShooting();

            aIState = AIStates.flee;
            car.Status = 3;
        }

        else if (playerCommand == playerCommands.stay)
        {
            aIState = AIStates.Idle;
            car.Status = 4;
        }

    }

    protected virtual void ManageStates()
    {
        if(aIState == AIStates.movement)
        {
            ManageMovement();
        }
        if(aIState == AIStates.manouverObstacles)
        {
            ManageManouverObstacles();
        }
        if (aIState == AIStates.manouverAttack)
        {
            ManageManouverAttack();
        }
        if (aIState == AIStates.flee)
        {
            ManageFlee();
        }
        if (aIState == AIStates.death)
        {
            ManageDeath();
        }
        if(aIState == AIStates.Idle)
        {
            ManageIdle();
        }
    }

    protected virtual void Movement()
    {
        if(PathVectors.Count > 0)
        {
            Steer(GetAngle(PathVectors[0]), GetMovingObstacles()[1], GetMovingObstacles()[2]);
            SetNewWaypoint();
            Accelerate(1 - (GetMovingObstacles()[0] * GetMovingObstacles()[0]));
            Brake(GetMovingObstacles()[0] * GetMovingObstacles()[0]);
        }
        else
        {
            Steer(GetAngle(targetMovement), GetMovingObstacles()[1], GetMovingObstacles()[2]);
            SetNewWaypoint();
            Accelerate(1 - (GetMovingObstacles()[0] * GetMovingObstacles()[0]));
            Brake(GetMovingObstacles()[0] * GetMovingObstacles()[0]);
        }
    }

    protected virtual void MovementBackwards()
    {
        if (PathVectors.Count > 0)
        {
            SteerBack(GetAngle(PathVectors[0]));
            SetNewWaypoint();
            Accelerate(-1 - (GetMovingObstacles()[0] * GetMovingObstacles()[0]));
            Brake(GetMovingObstacles()[0] * GetMovingObstacles()[0]);
        }
        else
        {
            SteerBack(GetAngle(targetMovement));
            SetNewWaypoint();
            Accelerate(-1 - (GetMovingObstacles()[0] * GetMovingObstacles()[0]));
            Brake(GetMovingObstacles()[0] * GetMovingObstacles()[0]);
        }
    }

    protected virtual void ManouverObstacles()
    {
        if (obstacleFront || carFront)
        {
            if(PathVectors.Count > 0)
            {
                SteerBack(GetAngle(PathVectors[0]));
            }
            else
            {
                SteerBack(GetAngle(targetMovement));
            }
            if (obstacleBack)
            {
                Accelerate(1);
            }
            else
            {
                Accelerate(-1);
            }
            Brake(0);
        }

        TargetAttack = shootingTarget.transform.position;
    }

    protected virtual bool FaceTarget(float maxAngle)
    {
        targetAttack = movingTarget.transform.position;

        bool isOriented = GetAngle(targetAttack) > -maxAngle && GetAngle(targetAttack) < maxAngle;
        //Debug.Log("is Oriented " + isOriented);
        bool isTooClose = GetDistance(targetAttack) < minTargetDistance;
        //Debug.Log("is too close " + isTooClose);


        //Cosa deve fare se non vede ostacoli
        if (!obstacleFront && !carFront && !obstacleBack && !carBack)
        {
            if(!isOriented && !isTooClose)
            {
                Steer(GetAngle(targetAttack), -1, 1);
                Accelerate(1);
                Brake(0);
            }
            else if(!isOriented && isTooClose)
            {
                SteerBack(GetAngle(targetAttack));
                Accelerate(-1);
                Brake(0);
            }
        }
        //Cosa deve fare se ha un ostacolo davanti a se
        else if(obstacleFront)
        {
            if (!isOriented)
            {
                SteerBack(GetAngle(targetAttack));
                Accelerate(-1);
                Brake(0);
            }
        }
        //Cosa deve fare se ha un ostacolo dietro di se
        else if(obstacleBack)
        {
            if (!isOriented)
            {
                SteerBack(GetAngle(targetAttack));
                Accelerate(1);
                Brake(0);
            }
        }
        else
        {
            if (!isOriented)
            {
                SteerBack(GetAngle(targetAttack));
                Accelerate(-1);
                Brake(0);
            }
        }

        return isOriented;

    }

    protected virtual void ManouverAttack()
    {
        Steer(GetAngle(targetMovement), -1f, 1f);
        Accelerate(0.5f);
    }

    protected virtual void Flee()
    {
        Vector3 dir = (movingTarget.transform.position - transform.position).normalized;
        targetMovement = transform.position + -dir * 50f;

        Debug.DrawRay(transform.position, -dir * 50, Color.magenta);
      
        if (PathVectors.Count > 0)
        {
            Steer(GetAngle(PathVectors[0]), GetMovingObstacles()[1], GetMovingObstacles()[2]);
            SetNewWaypoint();
            Accelerate(1);
            Brake(GetMovingObstacles()[1]);
        }
        else
        {
            Steer(GetAngle(targetMovement), GetMovingObstacles()[1], GetMovingObstacles()[2]);
            Accelerate(1);
            Brake(GetMovingObstacles()[1]);
        }
        
    }

    protected virtual void Death()
    {
        Accelerate(0);
        Brake(1);
    }

    protected virtual void Idle()
    {
        Accelerate(0);
        Brake(1);

        FaceTarget(30);

        targetAttack = shootingTarget.transform.position;
        targetMovement = movingTarget.transform.position;
    }

    IEnumerator UpdateNearestTarget()
    {
        while (true)
        {
            if (canUpdateTarget)
            {
                if (Arena._instance.GetOppositeTeamSize(car.Team) > 0)
                {
                    shootingTarget = Arena._instance.GetClosestCar(car.Team, transform).gameObject;
                    shootingTargetAI = shootingTarget.GetComponent<MediumAI>();
                }

                yield return new WaitForSeconds(2f);
            }

            yield return null;
        }

        yield return null;
    }

    IEnumerator UpdateDestination()
    {
        while(this)
        {
            if(aIState == AIStates.movement && targetMovement != null)
            {
                
                path = seeker.StartPath(transform.position, targetMovement);

                if (path != null)
                {
                    //Debug.Log("path " + gameObject.name);
                    PathVectors = path.vectorPath;
                    //Debug.Log(PathVectors.Count);
                }
                yield return new WaitForSeconds(0.2f);
            }
            else if(aIState == AIStates.flee && targetMovement != null)
            {
                //Debug.Log("path");
                path = seeker.StartPath(transform.position, targetMovement);
                if(path != null)
                {
                    PathVectors = path.vectorPath;
                }
                yield return new WaitForSeconds(0.2f);
            }
            else
            {
                yield return null;
            }
        }

        yield return null;
    }

    protected float GetDistance(Vector3 target)
    {
        float distance = Vector3.Distance(transform.position, target);

        return distance;
    }

    protected float GetAngle(Vector3 target)
    {
        Vector3 dir = target - transform.position;

        float angle = Vector3.SignedAngle(dir, transform.forward, new Vector3(0, 1, 0));

        return angle;
    }

    void SetNewWaypoint()
    {
        if (PathVectors.Count > 0)
        {
            if (Vector3.Distance(PathVectors[0], transform.position) < 8f)
            {
                PathVectors.Remove(PathVectors[0]);
            }
        }
    }

    protected void Steer(float angle, float minValue, float maxValue)
    {
        if (angle > 0)
        {
            float steering = -angle / 30;

            carInputs.Steering = Mathf.Clamp(steering, minValue, maxValue);
        }
        else if (angle < 0)
        {
            float steering = -angle / 30;

            carInputs.Steering = Mathf.Clamp(steering, minValue, maxValue);
        }
    }

    protected void SteerBack(float angle)
    {
        if (angle > 0)
        {
            carInputs.Steering = angle / 30;
        }
        else if (angle < 0)
        {
            carInputs.Steering = angle / 30;
        }
    }

    protected void Accelerate(float acceleration)
    {
        carInputs.Acceleration = acceleration;
    }

    protected void Brake(float brake)
    {
        carInputs.Brake = brake;
    }

    protected void GetObstacles()
    {
        //Debug.Log("i raggi degli ostacoli");
        Debug.DrawRay(carGameobject.transform.position, carGameobject.transform.forward * rayLenghtFront, Color.magenta);

        if (Physics.SphereCast(carGameobject.transform.position, 2f, carGameobject.transform.forward, out RaycastHit info, rayLenghtFront, Obstacle))
        {
            float dist = Vector3.Distance(info.point, carGameobject.transform.position);

            if (dist < rayLenghtFront/2)
            {
                obstacleFront = true;
            }
            else if(dist >= rayLenghtFront || info.point == null)
            {
                obstacleFront = false;
            }
        }
        else
        {
            obstacleFront = false;
        }

        if (Physics.SphereCast(carGameobject.transform.position + carGameobject.transform.forward * rayDistanceFront + carGameobject.transform.up * rayHeight, 2f, carGameobject.transform.forward, out RaycastHit infocar, rayLenghtFront, Cars))
        {
            float dist = Vector3.Distance(infocar.point, carGameobject.transform.position);

            if (dist < rayLenghtFront/2)
            {
                carFront = true;
            }
            else if (dist > rayLenghtFront || infocar.point == null)
            {
                carFront = false;
            }
        }
        else
        {
            carFront = false;
        }

        //Get Obstacles Back()
        Debug.DrawRay(carGameobject.transform.position, -carGameobject.transform.forward * rayLenghtFront, Color.magenta);
        //Debug.Log("Obstacle Back");

        if (Physics.Raycast(carGameobject.transform.position, -carGameobject.transform.forward, out RaycastHit infoBack1, rayLenghtFront, Obstacle))
        {
            float dist = Vector3.Distance(infoBack1.point, carGameobject.transform.position);

            if (dist < rayLenghtFront / 2)
            {
                obstacleBack = true;
            }
            else if (dist >= rayLenghtFront || infoBack1.point == null)
            {
                obstacleBack = false;
            }
        }
        else
        {
            obstacleBack = false;
        }

        if (Physics.Raycast(carGameobject.transform.position + -carGameobject.transform.forward * rayDistanceFront + carGameobject.transform.up * rayHeight, -carGameobject.transform.forward, out RaycastHit infoCarBack, rayLenghtFront, Cars))
        {
            float dist = Vector3.Distance(infoCarBack.point, carGameobject.transform.position);

            if (dist < rayLenghtFront / 2)
            {
                carBack = true;
            }
            else if (dist > rayLenghtFront || infoCarBack.point == null)
            {
                carBack = false;
            }
        }
        else
        {
            carBack = false;
        }
    }

    protected float[] GetMovingObstacles()
    {
        //Debug.Log("i raggi degli ostacoli dinamici");
        //raggio di destra frontale
        Debug.DrawRay(carGameobject.transform.position + carGameobject.transform.right * raySpacingFront + carGameobject.transform.forward * rayDistanceFront + carGameobject.transform.up * rayHeight, carGameobject.transform.forward * rayLenghtFront, Color.magenta);
        //raggio di sinistra frontale
        Debug.DrawRay(carGameobject.transform.position+ -carGameobject.transform.right * raySpacingFront + carGameobject.transform.forward * rayDistanceFront + carGameobject.transform.up * rayHeight, carGameobject.transform.forward * rayLenghtFront, Color.magenta);
        //raggio centrale frontale
        Debug.DrawRay(carGameobject.transform.position, carGameobject.transform.forward * rayLenghtFront, Color.magenta);

        bool carFront1 = Physics.Raycast(carGameobject.transform.position + carGameobject.transform.right * raySpacingFront + carGameobject.transform.forward * rayDistanceFront + carGameobject.transform.up * rayHeight, carGameobject.transform.forward, out RaycastHit hit, rayLenghtFront, Cars);
        bool carFront2 = Physics.Raycast(carGameobject.transform.position + -carGameobject.transform.right * raySpacingFront + carGameobject.transform.forward * rayDistanceFront + carGameobject.transform.up * rayHeight, carGameobject.transform.forward, out RaycastHit hit2, rayLenghtFront, Cars);

        //Ray SideRightFront
        Debug.DrawRay(carGameobject.transform.position + carGameobject.transform.forward * raySpacingSide + carGameobject.transform.right * rayDistanceSide + carGameobject.transform.up * rayHeight, carGameobject.transform.right * rayLenghtSide, Color.magenta);
        //Ray SideRightBack
        Debug.DrawRay(carGameobject.transform.position + -carGameobject.transform.forward * raySpacingSide + carGameobject.transform.right * rayDistanceSide + carGameobject.transform.up * rayHeight, carGameobject.transform.right * rayLenghtSide, Color.magenta);
        //Ray SideLeftFront
        Debug.DrawRay(carGameobject.transform.position + carGameobject.transform.forward * raySpacingSide + -carGameobject.transform.right * rayDistanceSide + carGameobject.transform.up * rayHeight, -carGameobject.transform.right * rayLenghtSide, Color.magenta);
        //Ray SideLeftBack
        Debug.DrawRay(carGameobject.transform.position + -carGameobject.transform.forward * raySpacingSide + -carGameobject.transform.right * rayDistanceSide + carGameobject.transform.up * rayHeight, -carGameobject.transform.right * rayLenghtSide, Color.magenta);

        bool CarSideRightFront = Physics.Raycast(carGameobject.transform.position + carGameobject.transform.forward * raySpacingSide + carGameobject.transform.right * rayDistanceSide + carGameobject.transform.up * rayHeight, carGameobject.transform.right,/* out RaycastHit hitright1,*/ rayLenghtSide, Cars);
        bool CarSideRightBack = Physics.Raycast(carGameobject.transform.position + -carGameobject.transform.forward * raySpacingSide + carGameobject.transform.right * rayDistanceSide + carGameobject.transform.up * rayHeight, carGameobject.transform.right,/* out RaycastHit hitright2,*/ rayLenghtSide, Cars);
        bool CarSideLeftFront = Physics.Raycast(carGameobject.transform.position + carGameobject.transform.forward * raySpacingSide + -carGameobject.transform.right * rayDistanceSide + carGameobject.transform.up * rayHeight, -carGameobject.transform.right, /*out RaycastHit hitright1,*/ rayLenghtSide, Cars);
        bool CarSideLeftBack = Physics.Raycast(carGameobject.transform.position + -carGameobject.transform.forward * raySpacingSide + -carGameobject.transform.right * rayDistanceSide + carGameobject.transform.up * rayHeight, -carGameobject.transform.right, /*out RaycastHit hitright1,*/ rayLenghtSide, Cars);

        float brakeRight = 0;
        float brakeLeft = 0;
        float brake;

        float hitDistanceRight;
        float hitDistanceLeft;

        float minValue;
        float maxValue;



        if (carFront1)  
        {
            hitDistanceRight = Vector3.Distance(carGameobject.transform.position + new Vector3(1, 0, 0), hit.point);
            brakeRight = 1 - (hitDistanceRight / rayLenghtFront);
            //Debug.Log(angleRight);
        }
        else
        {
            hitDistanceRight = rayLenghtFront;
        }
        if (carFront2)
        {
            hitDistanceLeft = Vector3.Distance(carGameobject.transform.position + new Vector3(1, 0, 0), hit2.point);
            brakeLeft = 1 - (hitDistanceLeft / rayLenghtFront);
            //Debug.Log(angleLeft);
        }
        else
        {
            hitDistanceLeft = rayLenghtFront;
        }


        if((hitDistanceLeft < rayLenghtFront/2 || hitDistanceRight < rayLenghtFront/2) && rb.velocity.magnitude < minSpeedManouver)
        {
            carFront = true;
            aIState = AIStates.manouverObstacles;
           // Debug.Log("manovra");
        }

        if (hitDistanceRight > hitDistanceLeft)
        {
            brake = brakeLeft;
        }
        else
        {
            brake = brakeRight;
        }

        if ((CarSideLeftFront || CarSideLeftBack) && !CarSideRightFront && !CarSideRightBack)
        {
            minValue = 0;
            maxValue = 1;

        }
        else if((CarSideRightFront || CarSideRightBack) && !CarSideLeftFront && !CarSideLeftBack)
        {
            minValue = -1;
            maxValue = 0;
        }
        else if ((CarSideRightFront || CarSideRightBack) && (CarSideLeftFront || !CarSideLeftBack))
        {
            minValue = 0;
            maxValue = 0;
        }
        else
        {
            minValue = -1;
            maxValue = 1;
        }

        float[] obstaclesOverrides = new float[] { brake, minValue, maxValue};

        return obstaclesOverrides;
    }

    protected void GetNewTargetMovement()
    {
        if (Arena._instance.GetOppositeTeamSize(car.Team) > 0 && !car.IsConvoy)
        {
            movingTarget = Arena._instance.GetClosestCar(car.Team, transform).gameObject;
            movingTargetAI = movingTarget.GetComponentInChildren<MediumAI>();
        }
    }

    protected void GetNewTargetShooting()
    {
        if (Arena._instance.GetOppositeTeamSize(car.Team) > 0)
        {
            shootingTarget = Arena._instance.GetClosestCar(car.Team, transform).gameObject;
            //Debug.Log("cerco ai");
            shootingTargetAI = shootingTarget.GetComponentInChildren<MediumAI>();
        }

    }

    protected bool GetFocusTarget()
    {
        if(Arena._instance.CameraScript.LookObject != null && Arena._instance.CameraScript.LookObject.Team != car.Team)
        {
            movingTarget = Arena._instance.CameraScript.LookObject.gameObject;
            movingTargetAI = movingTarget.GetComponentInChildren<MediumAI>();
            shootingTarget = Arena._instance.CameraScript.LookObject.gameObject;
            shootingTargetAI = shootingTarget.GetComponentInChildren<MediumAI>();

            Arena._instance.UpdateLog(car.CarName + "(" + weapon.WeaponName + ")" + " vuole dirtuggere a tutti i costi " + Arena._instance.PlayerCar.TargetCar.CarName + "(" + Arena._instance.PlayerCar.TargetCar.Weapon.WeaponName + ")");
            
            return true;
        }
        return false;
    }

    protected void GetAggroTarget()
    {
        if(car.MostDangerousCar != null)
        {
            shootingTarget = car.MostDangerousCar.gameObject;
            if (car.IsConvoy)
            {
                movingTarget = car.MostDangerousCar.gameObject;
            }
            isAggro = true;
        }
        else
        {
            isAggro = false;
        }
    }

    protected virtual void UpdateLogicBools()
    {
        health = car.Health;
        canShoot = weapon.CanShootTarget;

        if (car.MostDangerousCar != null)
        {
            if (car.HealthPerc > car.MostDangerousCar.HealthPerc + 10)
            {
                isStronger = true;
            }
            else
            {
                isStronger = false;
            }
        }
        else
        {
            isStronger = true;
        }


        if(GetDistance(movingTarget.transform.position) < minTargetDistance)
        {
            enemyTooClose = true;
        }
        else
        {
            enemyTooClose = false;
        }

        if(GetDistance(movingTarget.transform.position) > maxTargetDistance)
        {
            enemyTooFar = true;
            //Debug.Log(GetDistance(movingTarget.transform.position));
        }
        else
        {
            enemyTooFar = false;
        }

        if(health < minHealthRetreat)
        {
            isInjured = true;
        }
        else
        {
            isInjured = false;
        }
    }

    IEnumerator UpdateAggroTarget()
    {
        while(aIState != AIStates.death)
        {
            if (canAggro)
            {
                GetAggroTarget();

                yield return new WaitForSeconds(10);
            }

            yield return null;
        }

        yield return null;
    }



    protected virtual void ManageMovement()
    {
        if (movingTarget != null && AIState != AIStates.death)
        {
            targetMovement = movingTarget.transform.position;
        }
        if (shootingTarget != null && AIState != AIStates.death)
        {
            targetAttack = shootingTarget.transform.position;
        }

        if (playerCommand == playerCommands.focusAttack)
        {
            canAggro = false;

            if (!isAggro)
            {
                canUpdateTarget = false;
            }

            Movement();
            GetMovingObstacles();

            if (enemyTooClose && canShoot)
            {
                aIState = AIStates.manouverAttack;
            }
            else if (obstacleFront == true || carFront == true)
            {
                aIState = AIStates.manouverObstacles;
            }
            else if (isInjured && !isStronger)
            {
                aIState = AIStates.flee;
            }
        }
        else
        {
            canAggro = true;

            if (!isAggro)
            {
                canUpdateTarget = true;
            }

            Movement();
            GetMovingObstacles();

            if (obstacleFront == true || carFront == true)
            {
                aIState = AIStates.manouverObstacles;
            }
            else if (enemyTooClose && canShoot)
            {
                aIState = AIStates.manouverAttack;
            }
            else if (playerCommand == playerCommands.follow && enemyTooClose)
            {
                aIState = AIStates.manouverAttack;
            }
            else if (isInjured && !isStronger)
            {
                aIState = AIStates.flee;
            }
        }
    }

    protected virtual void ManageManouverObstacles()
    {
        if (movingTarget != null && AIState != AIStates.death)
        {
            targetMovement = movingTarget.transform.position;
        }
        if (shootingTarget != null && AIState != AIStates.death)
        {
            targetAttack = shootingTarget.transform.position;
        }

        canAggro = true;

        if (!isAggro)
        {
            canUpdateTarget = true;
        }

        ManouverObstacles();

        if (playerCommand == playerCommands.flee)
        {
            if (obstacleFront == false && carFront == false)
            {
                aIState = AIStates.flee;
            }
        }
        else
        {
            if (obstacleFront == false && carFront == false)
            {
                aIState = AIStates.movement;
            }
            else if (obstacleFront == false && carFront == false && !isStronger && isInjured)
            {
                aIState = AIStates.flee;
            }
        }
    }

    protected virtual void ManageManouverAttack()
    {
        if (movingTarget != null && AIState != AIStates.death)
        {
            targetMovement = movingTarget.transform.position;
        }
        if (shootingTarget != null && AIState != AIStates.death)
        {
            targetAttack = shootingTarget.transform.position;
        }

        if (playerCommand == playerCommands.focusAttack)
        {
            canAggro = false;

            if (!isAggro)
            {
                canUpdateTarget = false;
            }

            ManouverAttack();

            if (obstacleFront == true)
            {
                aIState = AIStates.manouverObstacles;
            }
            else if (!enemyTooClose || !canShoot)
            {
                aIState = AIStates.movement;
            }
            else if (isInjured && !isStronger)
            {
                aIState = AIStates.flee;
            }
        }
        else if (playerCommand == playerCommands.follow)
        {
            canAggro = false;

            if (!isAggro)
            {
                canUpdateTarget = false;
            }

            Brake(1);
            Accelerate(0);

            if (obstacleFront == true)
            {
                aIState = AIStates.manouverObstacles;
            }
            else if (!enemyTooClose || !canShoot)
            {
                aIState = AIStates.movement;
            }
            else if (isInjured && !isStronger)
            {
                aIState = AIStates.flee;
            }
        }
        else
        {
            if (!isAggro)
            {
                canUpdateTarget = true;
            }

            ManouverAttack();

            if (obstacleFront == true)
            {
                aIState = AIStates.manouverObstacles;
            }
            else if (enemyTooClose || !canShoot)
            {
                aIState = AIStates.movement;
            }
            if (health <= 0)
            {
                aIState = AIStates.death;
            }
            else if (isInjured && !isStronger)
            {
                aIState = AIStates.flee;
            }
        }
    }

    protected virtual void ManageFlee()
    {
        if (movingTarget != null && AIState != AIStates.death)
        {
            targetMovement = movingTarget.transform.position;
        }
        if (shootingTarget != null && AIState != AIStates.death)
        {
            targetAttack = shootingTarget.transform.position;
        }

        canAggro = true;

        if (!isAggro)
        {
            canUpdateTarget = true;
        }

        Flee();

        if (obstacleFront || carFront)
        {
            aIState = AIStates.manouverObstacles;
        }
        if (enemyTooFar && playerCommand != playerCommands.flee && !isStronger)
        {
            aIState = AIStates.Idle;
        }
    }

    protected virtual void ManageDeath()
    {
        if (!isAggro)
        {
            canUpdateTarget = true;
        }
        Death();
    }

    protected virtual void ManageIdle()
    {
        if (movingTarget != null && AIState != AIStates.death)
        {
            targetMovement = movingTarget.transform.position;
        }
        if (shootingTarget != null && AIState != AIStates.death)
        {
            targetAttack = shootingTarget.transform.position;
        }

        canAggro = true;

        if (!isAggro)
        {
            canUpdateTarget = true;
        }

        Idle();


        if (playerCommand == playerCommands.attack && isStronger)
        {
            AIState = AIStates.movement;
        }
    }

    void setObstaclesRays()
    {
        rayDistanceFront = car.RayDistanceFront;
        rayDistanceSide = car.RayDistanceSide;
        rayHeight = car.RayHeight;
        rayLenghtFront = car.RayLenghtFront;
        rayLenghtSide = car.RayLenghtSide;
        raySpacingFront = car.RaySpacingFront;
        raySpacingSide = car.RaySpacingSide;
        minSpeedManouver = car.MinSpeedManouver;
        minHealthRetreat = car.MinHealthRetreat;

        maxTargetDistance = weapon.MaxDistance;
        minTargetDistance = weapon.MinDistance;
    }

    void SetMinMaxDistance()
    {
        minTargetDistance = car.Weapon.MinDistance;
        maxTargetDistance = car.Weapon.MaxDistance;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualEffects : MonoBehaviour
{
    [SerializeField] List<GameObject> bodyParts = new List<GameObject>();

    [SerializeField] Material carMaterial;

    [SerializeField] ParticleSystem healthSmoke;
    [SerializeField] AnimationCurve curve;
    [SerializeField] ParticleSystem Explosion;
    [SerializeField] Image visualHint;

    [SerializeField] List<GameObject> deathEffects = new List<GameObject>();

    Car car;
    float em;
    bool isDead = false;

    // Start is called before the first frame update
    void Start()
    {
        car = GetComponent<Car>();
        if (healthSmoke == null)
        {
            return;
        }
        StartCoroutine(HealthParticles());
        AssignMaterials();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateVisualEffects();
    }

    //Aggiorna gli effetti visivi della macchina
    void UpdateVisualEffects()
    {
        if (healthSmoke == null)
        {
            return;
        }

        float perc = car.Health / 100;

        em = curve.Evaluate(perc);

        if (isDead == false && car.Health <= 0)
        {
            Instantiate(Explosion, transform.position, Quaternion.Euler(-90, 0, 0));
            UpdateTextures();
            for(int i = 0; i < deathEffects.Count; i++)
            {
                deathEffects[i].gameObject.SetActive(true);
            }
        }

        if (car.IsPlayer == false && car.Team == 0)
        {
            visualHint.enabled = true;
            //Debug.Log("disattivo");
        }
        else
        {
            visualHint.enabled = false;
            //Debug.Log("attivo");
        }
    }

    //Emissione particelle in base alla salute della macchina
    IEnumerator HealthParticles()
    {
        while (true)
        {
            if (car.Health != 100)
            {
                healthSmoke.Emit(3);
                yield return new WaitForSeconds(em);
            }
            else
            {
                yield return null;
            }
        }
        yield return null;
    }


    void AssignMaterials()
    {
        for(int i = 0; i < bodyParts.Count; i++)
        {
            Renderer rend = bodyParts[i].GetComponent<Renderer>();
            rend.sharedMaterials[1] = new Material(carMaterial);
        }
    }

    void UpdateTextures()
    {
        if(car.Health <= 0 && isDead == false)
        {
            for (int i = 0; i < bodyParts.Count; i++)
            {
                SetMaterialFloat(bodyParts[i]);
            }
            for(int i = 0; i < car.Weapon.ListaParti.Count; i++)
            {
                SetMaterialFloat(car.Weapon.ListaParti[i]);
            }
            isDead = true;
        }
    }

    void SetMaterialFloat(GameObject part)
    {
        Material mat = part.GetComponent<Renderer>().material;
        mat.SetFloat("Vector1_bf8f9c3cf31e4833a35279e101f6a13f", 0);
        mat.SetFloat("Vector1_d99a8238062f40798342af291631c749", 1);
    }
}

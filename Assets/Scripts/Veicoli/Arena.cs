using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Arena : MonoBehaviour
{
    public static Arena _instance;

    [SerializeField] GameObject endOfRoundUI;
    [SerializeField] Compass compass;
    [SerializeField] GameObject partyCarHudPrefab;
    [SerializeField] GameObject partyHudParent;
    [SerializeField] List<CarHud> partyCarsHud = new List<CarHud>();
    [SerializeField] PlayerHud playerHud;
    [SerializeField] LogUI logUI;
    [SerializeField] GameObject hitMarkerImage;
    ArenaSoundManager arenaSoundManager;

    [SerializeField] public List<Car> Team1 = new List<Car>();
    [SerializeField] public List<Car> Team2 = new List<Car>();

    [SerializeField] Car playerCar;
    Weapon playerWeapon;

    [SerializeField] GameObject convoyTarget;
    ConvoyTarget convoyTargetScript;

    [SerializeField] const int armor0 = 25, armor1 = 19, armor2 = 20, armor3 = 21;
    [SerializeField] const int motor0 = 22, motor1 = 23, motor2 = 24, motor3 = 210;

    [SerializeField] List<GameObject> Weapons = new List<GameObject>();
    [SerializeField] List<GameObject> Vehicles = new List<GameObject>();

    [SerializeField] List<Transform> SpawnPointTeam1 = new List<Transform>();
    [SerializeField] List<Transform> SpawnPointTeam2 = new List<Transform>();

    [SerializeField] bool canSpawn, hitMarker, endOfRound = false;

    [SerializeField] Camera playerCamera;
    [SerializeField] RotateCamera cameraScript;
    [SerializeField] CameraFollow cameraFollow;

    [SerializeField] OverHeatSlider overHeatSlider;

    int indexPlayer = 0;
    public Camera PlayerCamera { get => playerCamera; set => playerCamera = value; }
    public Car PlayerCar { get => playerCar; set => playerCar = value; }
    public Weapon PlayerWeapon { get => playerWeapon; set => playerWeapon = value; }
    public bool HitMarker { get => hitMarker; set => hitMarker = value; }
    public GameObject ConvoyTarget { get => convoyTarget; set => convoyTarget = value; }
    public RotateCamera CameraScript { get => cameraScript; set => cameraScript = value; }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            NextCar();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(CarListOpenWorld.Instance.SceneLoaded);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            playerCar.Health = 0;
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            if(Team2.Count > 0)
            {
                Team2[0].Health = 0;
            }
        }
        ManageEndOfRound();

    }

    private void Start()
    {
        arenaSoundManager = GetComponent<ArenaSoundManager>();
        overHeatSlider.ChangeReferencedCar(playerCar);
    }
    //Spawn delle macchine e inzio della partita
    void Awake()
    {
        cameraScript = playerCamera.gameObject.GetComponent<RotateCamera>();
        if (canSpawn)
        {
            Spawn();
        }
        _instance = this;
        
        StartCoroutine(StartGameAi());
        StartCoroutine(HitMarkerManager());
        if(convoyTarget != null)
        {
            convoyTargetScript = convoyTarget.GetComponent<ConvoyTarget>();
        }
    }


    //Aggiunge la macchina alla lista del team
    void AddCar(int team, Car car)
    {
        if(team == 0)
        {
            Team1.Add(car);
        }
        else if(team == 1)
        {
            Team2.Add(car);
        }
    }

    //Rimuove la macchina dalla lista del team
    public void RemoveCar(int team, Car car)
    {
        if (!endOfRound)
        {
            if (team == 0)
            {
                Team1.Remove(car);
                CarListOpenWorld.Instance.RemoveCarFromList(car.CarID);
            }
            else if (team == 1)
            {
                Team2.Remove(car);
                compass.RemoveMarker(car);
            }
        }
    }

    //Spawn Base allo startPartita che spawna in base ai vehicle data;
    void Spawn()
    {
        //Debug.Log("Spawn");
        for(int i = 0; i < CarListOpenWorld.Instance.Team1Data.Count; i++)
        {
            GameObject newCar = null;

            for (int n = 0; n < CarListOpenWorld.Instance.ListaMateriali.Count; n++)
            {
                if(CarListOpenWorld.Instance.Team1Data[i].VehicleType == CarListOpenWorld.Instance.ListaMateriali[n].ItemIndex)
                {
                    newCar = Instantiate(CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab, SpawnPointTeam1[i]);
                    break;
                }
            }
            Car car = newCar.GetComponent<Car>();
            AddCar(CarListOpenWorld.Instance.Team1Data[i].Team, car);
            car.Team = CarListOpenWorld.Instance.Team1Data[i].Team;
            car.CarName = CarListOpenWorld.Instance.Team1Data[i].CarName;
            car.CarID = CarListOpenWorld.Instance.Team1Data[i].CarId;
            car.HealthPerc = CarListOpenWorld.Instance.Team1Data[i].HealthPerc;
            switch (CarListOpenWorld.Instance.Team1Data[i].MotorType)
            {
                case motor0:

                    car.MotorInt = 0;

                    break;
                case motor1:

                    car.MotorInt = 1;

                    break;
                case motor2:

                    car.MotorInt = 2;

                    break;
                case motor3:

                    car.MotorInt = 3;

                    break;
            }

            GameObject newWeapon = null;
            if (car.SpawnPoints.Count > 0)
            {
                for (int n = 0; n < CarListOpenWorld.Instance.ListaMateriali.Count; n++)
                {
                    if (CarListOpenWorld.Instance.Team1Data[i].WeaponType == CarListOpenWorld.Instance.ListaMateriali[n].ItemIndex)
                    {
                        if (car.SpawnPoints.Count > 0)
                        {
                            if (CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab.GetComponent<RangeWeapon>() != null)
                            {
                                newWeapon = Instantiate(CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab, car.SpawnPoints[0].transform.position, Quaternion.identity);
                                car.WeaponImage = CarListOpenWorld.Instance.ListaMateriali[n].Icona;
                                newWeapon.transform.parent = newCar.transform;
                            }
                            else
                            {
                                newWeapon = Instantiate(CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab, car.SpawnPoints[3].transform.position, Quaternion.identity);
                                car.WeaponImage = CarListOpenWorld.Instance.ListaMateriali[n].Icona;
                                newWeapon.transform.parent = newCar.transform;
                            }
                        }
                        else
                        {
                            newWeapon = Instantiate(CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab, newCar.transform);
                        }
                        break;
                    }
                }
            }


            if(car.ArmorParts.Count > 0)
            {
                for(int n = 0; n < CarListOpenWorld.Instance.ListaMateriali.Count; n++)
                {
                    if(CarListOpenWorld.Instance.Team1Data[i].ArmorType == CarListOpenWorld.Instance.ListaMateriali[n].ItemIndex)
                    {
                        switch (CarListOpenWorld.Instance.Team1Data[i].ArmorType)
                        {
                            case armor0:
                                for (int k = 0; k < car.ArmorParts.Count; k++)
                                {
                                    car.ArmorParts[k].SetActive(false);
                                }
                                break;
                            case armor1:
                                for (int k = 0; k < car.ArmorParts.Count; k++)
                                {
                                    if (k <= 2)
                                    {
                                        car.ArmorParts[k].SetActive(true);
                                    }
                                    else
                                    {
                                        car.ArmorParts[k].SetActive(false);
                                    }
                                }
                                break;
                            case armor2:
                                for (int k = 0; k < car.ArmorParts.Count; k++)
                                {
                                    if (k <= 5)
                                    {
                                        car.ArmorParts[k].SetActive(true);
                                    }
                                    else
                                    {
                                        car.ArmorParts[k].SetActive(false);
                                    }
                                }
                                break;
                            case armor3:
                                for (int k = 0; k < car.ArmorParts.Count; k++)
                                {
                                    car.ArmorParts[k].SetActive(true);
                                }
                                break;
                        }
                        break;
                    }
                }
            }
            

            if (i == 0)
            {
                car.IsPlayer = true;
                playerCar = GetPlayerCar();
                cameraFollow.ResetCamera(car.gameObject.transform);
            }
            else
            {
                car.IsPlayer = false;
            }

        }
        AssignCarHud();

        for (int i = 0; i < CarListOpenWorld.Instance.Team2Data.Count; i++)
        {
            GameObject newCar = null;

            for (int n = 0; n < CarListOpenWorld.Instance.ListaMateriali.Count; n++)
            {
                if (CarListOpenWorld.Instance.Team2Data[i].VehicleType == CarListOpenWorld.Instance.ListaMateriali[n].ItemIndex)
                {
                    newCar = Instantiate(CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab, SpawnPointTeam2[i]);
                    break;
                }
            }
            Car car = newCar.GetComponent<Car>();
            AddCar(CarListOpenWorld.Instance.Team2Data[i].Team, car);
            car.Team = CarListOpenWorld.Instance.Team2Data[i].Team;
            car.CarName = CarListOpenWorld.Instance.Team2Data[i].CarName;
            car.CarID = CarListOpenWorld.Instance.Team2Data[i].CarId;
            car.HealthPerc = CarListOpenWorld.Instance.Team2Data[i].HealthPerc;
            car.IsConvoy = CarListOpenWorld.Instance.Team2Data[i].IsConvoy;
            switch (CarListOpenWorld.Instance.Team2Data[i].MotorType)
            {
                case motor0:

                    car.MotorInt = 0;

                    break;
                case motor1:

                    car.MotorInt = 1;

                    break;
                case motor2:

                    car.MotorInt = 2;

                    break;
                case motor3:

                    car.MotorInt = 3;

                    break;
            }

            GameObject newWeapon = null;
            if (car.SpawnPoints.Count > 0)
            {
                for (int n = 0; n < CarListOpenWorld.Instance.ListaMateriali.Count; n++)
                {
                    if (CarListOpenWorld.Instance.Team2Data[i].WeaponType == CarListOpenWorld.Instance.ListaMateriali[n].ItemIndex)
                    {
                        if (car.SpawnPoints.Count > 0)
                        {
                            if (CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab.GetComponent<RangeWeapon>() != null)
                            {
                                newWeapon = Instantiate(CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab, car.SpawnPoints[0].transform.position, Quaternion.identity);
                                newWeapon.transform.parent = newCar.transform;
                            }
                            else
                            {
                                newWeapon = Instantiate(CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab, car.SpawnPoints[3].transform.position, Quaternion.identity);
                                newWeapon.transform.parent = newCar.transform;
                            }
                        }
                        else
                        {
                            newWeapon = Instantiate(CarListOpenWorld.Instance.ListaMateriali[n].ItemPrefab, newCar.transform);
                        }
                        break;
                    }
                }
            }


            if (car.ArmorParts.Count > 0)
            {
                for (int n = 0; n < CarListOpenWorld.Instance.ListaMateriali.Count; n++)
                {
                    if (CarListOpenWorld.Instance.Team2Data[i].ArmorType == CarListOpenWorld.Instance.ListaMateriali[n].ItemIndex)
                    {
                        switch (CarListOpenWorld.Instance.Team2Data[i].ArmorType)
                        {
                            case armor0:
                                for (int k = 0; k < car.ArmorParts.Count; k++)
                                {
                                    car.ArmorParts[k].SetActive(false);
                                }
                                break;
                            case armor1:
                                for (int k = 0; k < car.ArmorParts.Count; k++)
                                {
                                    if (k <= 2)
                                    {
                                        car.ArmorParts[k].SetActive(true);
                                    }
                                    else
                                    {
                                        car.ArmorParts[k].SetActive(false);
                                    }
                                }
                                break;
                            case armor2:
                                for (int k = 0; k < car.ArmorParts.Count; k++)
                                {
                                    if (k <= 5)
                                    {
                                        car.ArmorParts[k].SetActive(true);
                                    }
                                    else
                                    {
                                        car.ArmorParts[k].SetActive(false);
                                    }
                                }
                                break;
                            case armor3:
                                for (int k = 0; k < car.ArmorParts.Count; k++)
                                {
                                    car.ArmorParts[k].SetActive(true);
                                }
                                break;
                        }
                        break;
                    }
                }
            }


            car.IsPlayer = false;
        }
        
    }

    //Riporta la macchina nemica pi� vicina 
    public Car GetClosestCar(int team, Transform playerPosition)
    {
        float dist = 1000000;
        Car closestCar = null;

        if(team == 1 && Team1.Count > 0)
        {
            for(int i = 0; i < Team1.Count; i++)
            {
                if(Vector3.Distance(playerPosition.position, Team1[i].gameObject.transform.position) < dist)
                {
                    closestCar = Team1[i];
                   
                    dist = Vector3.Distance(playerPosition.position, Team1[i].gameObject.transform.position);
                }
            }
        }
        else if(team == 0 && Team2.Count > 0)
        {
            for (int i = 0; i < Team2.Count; i++)
            {
                if (Vector3.Distance(playerPosition.position, Team2[i].gameObject.transform.position) < dist)
                {
                    closestCar = Team2[i];
                    
                    dist = Vector3.Distance(playerPosition.position, Team2[i].gameObject.transform.position);
                }
            }
        }

        return closestCar;
    }

    //Riporta il numero di macchine presenti nel team opposto
    public float GetOppositeTeamSize(int team)
    {
        if(team == 1)
        {
            return Team1.Count;
        }
        else
        {
            return Team2.Count;
        }
    }

    //Assegna isPlayer alla macchina successiva nella lista;
    public void NextCar()
    {
        indexPlayer = (indexPlayer + 1) % Team1.Count;

        for (int i=0; i< Team1.Count; i++)
        {
            if (i == indexPlayer)
                Team1[i].IsPlayer = true;
            else
                Team1[i].IsPlayer = false;
        }
        cameraFollow.ResetCamera(Team1[indexPlayer].gameObject.transform);     
        playerCar = Team1[indexPlayer];
        playerWeapon = Team1[indexPlayer].GetComponentInChildren<Weapon>();
        //compass.UpdatePlayerCarCompass(Team1[indexPlayer].gameObject);
        overHeatSlider.ChangeReferencedCar(playerCar);
    }

    //Riporta le macchine dello stesso gruppo del team
    public List<Car> GetCarOfSameGroup(int team, Weapon.WeaponType wpnTyp)
    {
        if(team == 0)
        {
            List<Car> cars = new List<Car>();

            for(int i = 0; i < Team1.Count; i++)
            {
                if(Team1[i].Weapon.WpnType1 == wpnTyp)
                {
                    cars.Add(Team1[i]);
                }
            }
            return cars;
        }
        else
        {
            List<Car> cars = new List<Car>();

            for (int i = 0; i < Team2.Count; i++)
            {
                if (Team2[i].Weapon.WpnType1 == wpnTyp)
                {
                    cars.Add(Team2[i]);
                }
            }
            return cars;
        }
    }

    //Riporta la macchina del giocatore
    public Car GetPlayerCar()
    {
        for (int i = 0; i < Team1.Count; i++)
        {
            if (Team1[i].IsPlayer)
            {
                playerCar = Team1[i];
                playerWeapon = Team1[i].GetComponentInChildren<Weapon>();
                overHeatSlider.ChangeReferencedCar(playerCar);
                return Team1[i];
            }
        }
        return null;
    }

    public void FlipCar(Car car)
    {
        cameraFollow.ResetCamera(car.transform);
    }

    //Assegnazione dei comandi allo start della partita
    IEnumerator StartGameAi()
    {
        yield return new WaitForSeconds(10);

        for(int i = 0; i < Team2.Count; i++)
        {
            Team2[i].MediumAI.PlayerCommand = MediumAI.playerCommands.attack;
        }
    }

    //Controlla il progesso della partita
    void ManageEndOfRound()
    {
        if(endOfRound == false)
        {            
            if (Team1.Count <= 0)
            {
                CarListOpenWorld.Instance.HasWon = false;
                endOfRoundUI.SetActive(true);
                arenaSoundManager.playLossSound();
                endOfRound = true;
            }
            else if (Team2.Count <= 0)
            {               
                CarListOpenWorld.Instance.HasWon = true;
                endOfRoundUI.SetActive(true);
                arenaSoundManager.playWinSound();
                endOfRound = true;
            }
            if(convoyTargetScript != null)
            {
                if(convoyTargetScript.EnemyEscaped == true)
                {
                    CarListOpenWorld.Instance.HasWon = false;
                    endOfRoundUI.SetActive(true);
                    arenaSoundManager.playLossSound();
                    endOfRound = true;
                }
            }
        }
        else
        {
            OpenWorldLoading();
        }
    }

    void AssignCarHud()
    {
        for(int i = 0; i < Team1.Count; i++)
        {
            GameObject newHud = Instantiate(partyCarHudPrefab, partyHudParent.transform);
            CarHud carHudScript = newHud.GetComponent<CarHud>();
            carHudScript.Car = Team1[i];
            partyCarsHud.Add(carHudScript);
        }
    }

    public void UpdateLog(string text)
    {
        logUI.AddLog(text);
    }

    public void HitMarkerEffect(bool isCritical)
    {
        if (isCritical)
        {
            hitMarkerImage.GetComponent<Image>().color = Color.red;
        }
        else
        {
            hitMarkerImage.GetComponent<Image>().color = Color.white;
        }
        hitMarkerImage.SetActive(true);
        hitMarker = true;
        //Play audio effect;
    }


    IEnumerator HitMarkerManager()
    {
        while (true)
        {
            if (hitMarker)
            {
                yield return new WaitForSeconds(0.1f);

                hitMarkerImage.SetActive(false);

                hitMarker = false;
            }
            yield return null;
        }
    }

    void OpenWorldLoading()
    {
        if (endOfRound && Input.GetKeyDown(KeyCode.Return))
        {
            if (CarListOpenWorld.Instance.LoadedScene == CarListOpenWorld.Instance.OpenWorldMap)
            {
                SceneManager.LoadScene(CarListOpenWorld.Instance.OpenWorldMap);
            }
            else
            {
                SceneManager.LoadScene(CarListOpenWorld.Instance.OpenWorldMapTest);
            }
        }
    }

}

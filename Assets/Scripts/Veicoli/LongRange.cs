using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongRange : MediumAI
{
    protected override void Start()
    {
        base.Start();

        UpdateCanShoot();
    }

    //Override della manovra di attacco
    protected override void ManouverAttack()
    {
        bool isFacing = FaceTarget(30f);

        TargetAttack = movingTarget.transform.position;


        if (isFacing && !enemyTooFar)
        {
            Brake(1);
            Accelerate(0);
        }
        else if (enemyTooClose)
        {
            Vector3 dir = (movingTarget.transform.position - transform.position).normalized;

            targetMovement = transform.position + -dir * 50f;

            Debug.DrawRay(transform.position, -dir * 50, Color.magenta);

            MovementBackwards();
        }
        else if(!enemyTooClose && !isFacing)
        {
            FaceTarget(10);
        }
    }

    //Override dell?aggiornamento delle logiche
    protected override void UpdateLogicBools()
    {
        health = car.Health;

        if (weapon.CanShootTarget == true)
        {
            canShoot = true;
        }

        if (car.MostDangerousCar != null)
        {
            if (car.HealthPerc > car.MostDangerousCar.HealthPerc + 10)
            {
                isStronger = true;
            }
            else
            {
                isStronger = false;
            }
        }
        else
        {
            isStronger = true;
        }


        if (GetDistance(movingTarget.transform.position) < minTargetDistance)
        {
            enemyTooClose = true;
        }
        else
        {
            enemyTooClose = false;
        }

        if (GetDistance(movingTarget.transform.position) > maxTargetDistance)
        {
            enemyTooFar = true;
            //Debug.Log(GetDistance(movingTarget.transform.position));
        }
        else
        {
            enemyTooFar = false;
        }

        if (health < minHealthRetreat)
        {
            isInjured = true;
        }
        else
        {
            isInjured = false;
        }
    }

    //Aggiornamento del canShoot per il veicolo
    IEnumerator UpdateCanShoot()
    {
        while(aIState != AIStates.death)
        {
            if (canShoot == true && weapon.CanShootTarget == false)
            {
                yield return new WaitForSeconds(5);

                canShoot = false;
            }
        }
    }

    //Override della logica del manouver attack
    protected override void ManageManouverAttack()
    {
        if (shootingTarget != null && AIState != AIStates.death)
        {
            targetAttack = shootingTarget.transform.position;
        }

        if (playerCommand == playerCommands.focusAttack)
        {
            canAggro = false;

            if (!isAggro)
            {
                canUpdateTarget = false;
            }

            ManouverAttack();

            if (obstacleFront == true)
            {
                aIState = AIStates.manouverObstacles;
            }
            else if (enemyTooFar || !canShoot)
            {
                aIState = AIStates.movement;
            }
            else if (isInjured && !isStronger)
            {
                aIState = AIStates.flee;
            }
            if (health <= 0)
            {
                aIState = AIStates.death;
            }
        }
        else
        {
            if (!isAggro)
            {
                canUpdateTarget = true;
            }

            GetAggroTarget();
            ManouverAttack();

            if (obstacleFront == true)
            {
                aIState = AIStates.manouverObstacles;
            }
            else if (enemyTooFar || !canShoot)
            {
                aIState = AIStates.movement;
            }
            if (health <= 0)
            {
                aIState = AIStates.death;
            }
            else if (isInjured && !isStronger)
            {
                aIState = AIStates.flee;
            }
        }
    }

}

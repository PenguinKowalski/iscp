using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConvoyTarget : MonoBehaviour
{
    bool enemyEscaped = false;

    public bool EnemyEscaped { get => enemyEscaped; set => enemyEscaped = value; }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Car")) 
        {
            Car car = other.gameObject.GetComponentInParent<Car>();

            if (car.Team == 1)
            {
                enemyEscaped = true;
            }
        }     
    }
}

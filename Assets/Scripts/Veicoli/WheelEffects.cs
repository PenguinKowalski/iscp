using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adrenak.Tork;

public class WheelEffects : MonoBehaviour
{
    [SerializeField] Wheel ruota;
    [SerializeField] TrailRenderer skidMark;
    [SerializeField] GameObject skidMarkParent;
    [SerializeField] float offsetX, offsetY, offsetZ;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(ruota.isGrounded == true)
        {
            skidMark.emitting = true;
            skidMarkParent.transform.position = new Vector3(ruota.Hit.point.x + offsetX, ruota.Hit.point.y + offsetY, ruota.Hit.point.z + offsetZ);
        }
        else
        {
            skidMark.emitting = false;
        }
    }
}

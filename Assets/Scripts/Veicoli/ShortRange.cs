using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShortRange : MediumAI
{

    AnimationCurve pervTurbo; 
    [SerializeField] AnimationCurve turbo = AnimationCurve.Linear(0, 40000, 2000, 0);

    float manouverStear;


    protected override void Start()
    {
        base.Start();

        UpdateStearingManouver();
    }


    protected override void Update()
    {
        if (isAI)
        {
            if (path.vectorPath != null)
            {
                PathVectors = path.vectorPath;
            }
            if (movingTarget != null && AIState != AIStates.death)
            {
                targetMovement = movingTarget.transform.position;
            }
            if (shootingTarget != null && AIState != AIStates.death)
            {
                targetAttack = shootingTarget.transform.position;
            }

            UpdateLogicBools();
            GetObstacles();
            ManageStates();

            if (movingTargetAI.AIState == AIStates.death)
            {
                GetNewTargetMovement();
                GetNewTargetShooting();
            }
        }
    }

    protected override void ManouverAttack()
    {
        Steer(manouverStear, -1, 1);
        Accelerate(1);
        Brake(0);
    }

    public void Turbo()
    {
        pervTurbo = vehicle.MotorTorqueVsSpeed;

        vehicle.MotorTorqueVsSpeed = turbo;
    }

    public void StopTurbo()
    {
        vehicle.MotorTorqueVsSpeed = pervTurbo;
    }

    //Override dei buleani di logica
    protected override void UpdateLogicBools()
    {
        health = car.Health;
        canShoot = weapon.CanShootTarget;

        if (car.MostDangerousCar != null)
        {
            if (car.HealthPerc > car.MostDangerousCar.HealthPerc + 10)
            {
                isStronger = true;
            }
            else
            {
                isStronger = false;
            }
        }
        else
        {
            isStronger = true;
        }


        if (GetDistance(movingTarget.transform.position) < minTargetDistance)
        {
            enemyTooClose = true;
        }
        else
        {
            enemyTooClose = false;
        }

        if (GetDistance(movingTarget.transform.position) > maxTargetDistance)
        {
            enemyTooFar = true;
        }
        else
        {
            enemyTooFar = false;
        }

        if (health < minHealthRetreat)
        {
            isInjured = true;
        }
        else
        {
            isInjured = false;
        }
    }

    //Aggiornamenti dello stearing durante manouverAttack
    IEnumerator UpdateStearingManouver()
    {
        while(aIState != AIStates.death)
        {
            if(aIState == AIStates.manouverAttack)
            {
                manouverStear =  Random.Range(30, 20);

                yield return new WaitForSeconds(5);
            }
        }
    }

}



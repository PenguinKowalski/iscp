using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaSoundManager : MonoBehaviour
{
    [SerializeField] AudioSource endGameSound;

    [SerializeField] AudioClip winSound, lossSound;

    public void playWinSound()
    {
        endGameSound.PlayOneShot(winSound);
    }

    public void playLossSound()
    {
        endGameSound.PlayOneShot(lossSound);
    }
}

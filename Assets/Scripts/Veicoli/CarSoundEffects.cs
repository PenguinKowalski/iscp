using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adrenak.Tork;

public class CarSoundEffects : MonoBehaviour
{
    [SerializeField] AudioSource engineAudioSource, BrakesAudioSource, carCollisionAudioSource, carOrdersAudioSource;
    [SerializeField] AnimationCurve enginerPitchSpeed, tiresVolumeSpeed;
    
    [SerializeField] List<AudioClip> crashSounds = new List<AudioClip>();

    [SerializeField] AudioClip fireEffect, explosionEffect;

    [SerializeField] List<WheelAudio> wheels = new List<WheelAudio>();
    [SerializeField] List<OrderAudio> orders = new List<OrderAudio>();

    float i, n, brakeVolume = 0;
    bool isDead;

    Car car;
    InputManager inputs;

    // Start is called before the first frame update
    void Start()
    {
        car = GetComponent<Car>();
        inputs = GetComponent<InputManager>();
        StartCoroutine(StartAudio());
    }

    // Update is called once per frame
    void Update()
    {
        
        EngineAudio();

        UpdateWheelsAudio();
        
        ManageDeathAudio();

        BrakeAudio();
        
    }

    void BrakeAudio()
    {
        if(car.Speed > 5)
        {
            if(inputs.Acceleration < 0 || inputs.Brake > 0)
            {              
                if(n < 1)
                {
                    n += Time.deltaTime * 2;
                }
                float pitchBrakesBraking = Mathf.Lerp(brakeVolume, 1f, n);
                Debug.Log(pitchBrakesBraking);
                BrakesAudioSource.volume = pitchBrakesBraking;
            }
            else
            {
                if (n > 0)
                {
                    n -= Time.deltaTime * 3;
                }
                float pitchBrakesNotBraking = Mathf.Lerp(0f, brakeVolume, n);
                BrakesAudioSource.volume = pitchBrakesNotBraking;
            }
        }
        else
        {
            if (n > 0)
            {
                n -= Time.deltaTime * 3;
            }
            float pitchBrakesNotBraking = Mathf.Lerp(0f, brakeVolume, n);
            BrakesAudioSource.volume = pitchBrakesNotBraking;
        }
    }

    void EngineAudio()
    {
        float pitch = enginerPitchSpeed.Evaluate(car.Speed);

        if (inputs.Acceleration > 0)
        {
            if (!wheels[0].WheelScript.isGrounded && !wheels[1].WheelScript.isGrounded)
            {
                if (i < 1)
                {
                    i += Time.deltaTime * 2;
                }
                float picthNotGrounded = Mathf.Lerp(pitch, 1.5f, i);
                engineAudioSource.pitch = picthNotGrounded;
            }
            else
            {
                if (i > 0)
                {
                    i -= Time.deltaTime * 2;
                }
                float picthGrounded = Mathf.Lerp(pitch, 1.5f, i);
                engineAudioSource.pitch = picthGrounded;
            }
        }
        else
        {
            engineAudioSource.pitch = pitch; ;
        }
    }

    void ManageDeathAudio()
    {
        if (car.Health <= 0 && isDead == false)
        {
            carCollisionAudioSource.PlayOneShot(explosionEffect);
            engineAudioSource.clip = fireEffect;
            isDead = true;
        }
    }


    void UpdateWheelsAudio()
    {
        for(int i = 0; i < wheels.Count; i++)
        {
            if (!wheels[i].WheelScript.isGrounded)
            {
                wheels[i].WheelAudioSource.Pause();
                wheels[i].WheelAudioSource.volume = tiresVolumeSpeed.Evaluate(car.Speed); 
                wheels[i].Paused = true;
            }
            else if(wheels[i].Paused)
            {
                wheels[i].WheelAudioSource.UnPause();
                wheels[i].Paused = false;
            }
        }
    }

    public void PlayOrderAudio(string order)
    {
        for(int k = 0; k < orders.Count; k++)
        {
            if(orders[k].OrderString == order)
            {
                carOrdersAudioSource.PlayOneShot(orders[k].AudioOrder);
                return;
            }
        }
    }

    IEnumerator StartAudio()
    {
        float i = Random.Range(0, 0.5f);
        yield return new WaitForSeconds(i);
        engineAudioSource.Play();
    }

    public void PlayCollisionAudio()
    {
        int i = Random.Range(0, crashSounds.Count);

        carCollisionAudioSource.PlayOneShot(crashSounds[i]);
    }
}

[System.Serializable]
public class WheelAudio
{
    [SerializeField] Wheel wheelScript;
    [SerializeField] AudioSource wheelAudioSource;
    bool paused;

    public Wheel WheelScript { get => wheelScript; set => wheelScript = value; }
    public AudioSource WheelAudioSource { get => wheelAudioSource; set => wheelAudioSource = value; }
    public bool Paused { get => paused; set => paused = value; }
}

[System.Serializable]
public class OrderAudio
{
    [SerializeField] AudioClip audioOrder;
    [SerializeField] string orderString;

    public AudioClip AudioOrder { get => audioOrder; set => audioOrder = value; }
    public string OrderString { get => orderString; set => orderString = value; }
}

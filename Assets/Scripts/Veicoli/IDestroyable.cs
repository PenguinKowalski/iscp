using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDestroyable
{
    // Start is called before the first frame update
    void ChangeHealth(float differenza, float[] moltiplicatore, Car attackingCar, RaycastHit? raycastHit, Ray? ray, float bulletSpeed, float raycastLength = -1);


    void Death(RaycastHit? raycastHit);
    // Update is called once per frame
}

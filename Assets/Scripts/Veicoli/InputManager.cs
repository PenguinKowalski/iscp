﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adrenak.Tork;

public class InputManager : MonoBehaviour
{
	protected VehicleInput p_Input = new VehicleInput();

    [SerializeField] float acceleration, steering, brake;

    [SerializeField] bool aI, isDead;

    public float Acceleration { get => acceleration; set => acceleration = value; }
    public float Brake { get => brake; set => brake = value; }
    public float Steering { get => steering; set => steering = value; }
    public bool AI { get => aI; set => aI = value; }
    public bool IsDead { get => isDead; set => isDead = value; }

    private void Update()
    {
        if (!aI)
        {
            Acceleration = 0;
            Steering = 0;
            brake = 0;
            if (!isDead)
            {
                if (Input.GetKey(KeyCode.W))
                {
                    Acceleration = 1;
                }
                if (Input.GetKey(KeyCode.S))
                {
                    Acceleration = -1;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    Steering = -1;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    Steering = 1;
                }
            }
        }
    }


}

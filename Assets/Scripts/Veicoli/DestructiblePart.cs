using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructiblePart : MonoBehaviour, IDestroyable
{
    [SerializeField]float maxHealth, health, explosionForce, carDamageMulti;
    Rigidbody rb;
    bool isDead;
    [SerializeField] float[] resistenze;
    [SerializeField] float forceStabilizer = 1;
    Car thisCar;

    void Start()
    {
        health = maxHealth;
        isDead = false;
        thisCar = gameObject.GetComponentInParent<Car>();
    }
    public void ChangeHealth(float differenza, float[] moltiplicatore, Car car, RaycastHit? raycastHit, Ray? ray, float bulletSpeed, float raycastLength = -1)
    {
        if (!Weapon.IsCarEnemy(car, thisCar))
        {
            return;
        }
        for (int i = 0; i < moltiplicatore.Length; i++)
        {
            if (health > 0)
            {
                health -= differenza * moltiplicatore[i] * resistenze[i];
                thisCar.ChangeHealth(differenza * carDamageMulti, moltiplicatore, car, raycastHit.Value, ray, bulletSpeed);
            }
            else
            {               
                if (isDead)
                {
                    if (ray.HasValue && rb != null)
                    {
                        rb.AddForceAtPosition(ray.Value.direction * bulletSpeed * forceStabilizer, raycastHit.Value.point, ForceMode.Impulse);
                    }
                }
                else
                {
                    Death(raycastHit.Value);
                }
            }
        }
    }
    public void Death(RaycastHit? raycastHit)
    {
        transform.parent = null;
        rb = gameObject.AddComponent<Rigidbody>();
        isDead = true;
        rb.AddForce(raycastHit.Value.normal * explosionForce);
        //Debug.Log(raycastHit.Value.normal);
    }
}

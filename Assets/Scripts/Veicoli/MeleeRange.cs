using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeRange : MediumAI
{
    
    AnimationCurve pervTurbo; 
    [SerializeField] AnimationCurve turbo = AnimationCurve.Linear(0, 40000, 2000, 0);
    
    protected override void Update()
    {
        if (isAI)
        {
            if (path.vectorPath != null)
            {
                PathVectors = path.vectorPath;
            }

            health = car.Health;

            UpdateLogicBools();
            GetObstacles();
            ManageStates();
            
            if (movingTargetAI.AIState == AIStates.death)
            {
                GetNewTargetMovement();
                GetNewTargetShooting();
            }
            
        }

    }

    protected override void ManouverAttack()
    {
        if(canShoot && FaceTarget(30))
        {
            Accelerate(1);
            Brake(0);    
        }
        else
        {
            FaceTarget(20);
        }
    }
    
    public void Turbo()
    {
        
        pervTurbo = vehicle.MotorTorqueVsSpeed;

        vehicle.MotorTorqueVsSpeed = turbo;
        
    }

    public void StopTurbo()
    {
        
        vehicle.MotorTorqueVsSpeed = pervTurbo;
        
    }
    
    protected override void ManageManouverAttack()
    {
        if (movingTarget != null && AIState != AIStates.death)
        {
            targetMovement = movingTarget.transform.position;
        }
        if (shootingTarget != null && AIState != AIStates.death)
        {
            targetAttack = shootingTarget.transform.position;
        }

        if (playerCommand == playerCommands.focusAttack)
        {
            canAggro = false;

            if (!isAggro)
            {
                canUpdateTarget = false;
            }

            ManouverAttack();

            if (obstacleFront == true)
            {
                aIState = AIStates.manouverObstacles;
            }
            else if (!enemyTooClose || !canShoot)
            {
                aIState = AIStates.movement;
            }
            else if (isInjured && !isStronger)
            {
                aIState = AIStates.flee;
            }
            if (health <= 0)
            {
                aIState = AIStates.death;
            }
        }
        else
        {
            if (!isAggro)
            {
                canUpdateTarget = true;
            }

            GetAggroTarget();
            ManouverAttack();

            if (obstacleFront == true)
            {
                aIState = AIStates.manouverObstacles;
            }
            else if (enemyTooClose || !canShoot)
            {
                aIState = AIStates.movement;
            }
            if (health <= 0)
            {
                aIState = AIStates.death;
            }
            else if (isInjured && !isStronger)
            {
                aIState = AIStates.flee;
            }
        }
    }
    
    


}


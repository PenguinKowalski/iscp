using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lanciafiamme : RangeWeapon
{
    Car enemyCar;
    List<Car> collidingCars = new List<Car>();
    List<Coroutine> listaCoroutine = new List<Coroutine>();
    bool firing;
    [SerializeField] ParticleSystem flames;
    [SerializeField]FlameThrower_Damage damageApplier;
    //AudioSource[] theseAudios;
    [SerializeField] List<AudioClip> flameSounds = new List<AudioClip>();
    [SerializeField] float startingClipLength;


    //[SerializeField] bool isAttacking;
    // Start is called before the first frame update
    public override void Start()
    {
        thisVehicle = GetComponentInParent<Car>();
        theseAudios = GetComponents<AudioSource>();
        firing = false;
        canFirePrimary = true;
        inputManager = GetComponentInParent<InputManager>();
        aiStates = GetComponentInParent<MediumAI>();
        flames.Stop();
        damageApplier.GetInformation(firing, damage, thisVehicle, fireRate, enemyCar, collidingCars, listaCoroutine, this);
        for(int i = 0; i < theseAudios.Length; i++)
        {
            theseAudios[i].Stop();
        }
    }

    // Update is called once per frame
    public override void Update()
    {
        if (!inputManager.AI)
        {

            if (!mainCamera)
            {
                mainCamera = Arena._instance.PlayerCamera;
            }
            CalculateTrajectory();
        }
        if (firing)
        {
            overHeatPrimary += Time.deltaTime * overHeatRatePrimary;
        }
        if (overHeatPrimary >= overHeatMaxPrimary)
        {
            canFirePrimary = false;
        }
        if (aiStates.AIState != MediumAI.AIStates.death)
        {
            if (!inputManager.AI)
            {
                Inputs();
            }
            else
            {
                if (CanShootTarget)
                {
                    if (!canFirePrimary)
                    {
                        firing = false;
                        EndFlameSounds();
                        overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
                    }
                    else
                    {
                        firing = true;
                        StartCoroutine(FlameSoundsStart());
                    }
                    damageApplier.Firing = firing;
                }
            }
        }
        if (canFirePrimary)
        {
            if (firing)
            {
                Debug.Log("wadwa");
                flames.Play();
            }
            else
            {
                flames.Stop();
            }
        }
        else
        {
            firing = false;
            EndFlameSounds();
            damageApplier.Firing = firing;
        }
        if (!firing)
        {
            if (overHeatPrimary >= 0)
                overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
            flames.Stop();
        }
        if (overHeatPrimary < 0)
        {
            overHeatPrimary = 0;
        }
        if(!firing && theseAudios[0].isPlaying)
        {
            EndFlameSounds();
        }

    }

    IEnumerator FlameSoundsStart()
    {
        theseAudios[0].clip = flameSounds[0];
        theseAudios[0].Play();
        yield return new WaitForSeconds(startingClipLength);
        theseAudios[0].clip = flameSounds[1];
        theseAudios[0].loop = true;
        theseAudios[0].Play();
    }

    public override void Inputs()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Debug.Log(firing+ " "+ canFirePrimary);
            firing = true;
            StartCoroutine(FlameSoundsStart());
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            firing = false;
            EndFlameSounds();
        }
        damageApplier.Firing = firing;
    }

    void EndFlameSounds()
    {
        theseAudios[0].Stop();
        theseAudios[0].loop = false;
        theseAudios[0].clip = flameSounds[2];
        theseAudios[0].Play();
    }


    public override bool CanShoot()
    {
        CanShootTarget = false;
        if (Physics.Raycast(transform.position, transform.forward, out hit, allowedDistance, destroyableMask))
        {
            Car targetVehicle = hit.collider.gameObject.GetComponentInParent<Car>();
            //Debug.Log("hit" + targetVehicle);
            if (IsCarEnemy(targetVehicle, thisVehicle) && targetVehicle != null && (targetVehicle.transform.position - thisVehicle.transform.position).magnitude < allowedDistance)
            {
                //  Debug.Log("hit 1");
                CanShootTarget = true;

            }
            else
            {
                CanShootTarget = false;
                //return canShootTarget;
            }
            return CanShootTarget;
        }
        return CanShootTarget;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proiettile
{
    public Vector3 bulletVelocity;
    public GameObject bullet;
    public float lifeTimer;
    public AudioSource thisAudio;

    public Proiettile(Vector3 bulletVelocity, GameObject bullet)
    {
        this.bulletVelocity = bulletVelocity;
        this.bullet = bullet;
    }
}

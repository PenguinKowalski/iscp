using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailProjectile : MonoBehaviour
{
    TrailRenderer trail;
   
    void Start()
    {
        trail = this.GetComponentInChildren<TrailRenderer>();
    }

    // Update is called once per frame
    void OnEnable()
    {
        if (trail==null)
            trail = this.GetComponentInChildren<TrailRenderer>();
        trail.Clear();
    }

    void OnDisable()
    {
        if (trail == null)
            trail = this.GetComponentInChildren<TrailRenderer>();

        trail.Clear();
    }
}

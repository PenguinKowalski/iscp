using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adrenak.Tork;

public class ConvertForceToWheel : MonoBehaviour
{
    [SerializeField] Wheel[] wheels;
    // Start is called before the first frame update

    [SerializeField] float mult=2f;
    /*
    public bool updateflag = false;

    private void Start()
    {
        forcevalues = new float[wheels.Length];
    }
    private void Update()
    {
        if (updateflag)
        {
           // ApplyForce(forceDirection);
            updateflag = false;
        }
     
    }
    */
    public void ApplyForce(Vector3 position, Vector3 direction)
    {     
        Debug.DrawRay(position, direction, Color.magenta,10f);

        StartCoroutine(ApplyForce_internal(position, direction));
    }


    IEnumerator ApplyForce_internal(Vector3 position, Vector3 direction)
    {
        float[] forcevalues= new float[wheels.Length];
        for (int i = 0; i < wheels.Length; i++)
        {
            Vector3 dir = wheels[i].transform.position - position;


            Vector3 directionHorizontalX = Vector3.Project(-direction, this.transform.forward);
            Vector3 directionHorizontalZ = Vector3.Project(-direction, this.transform.right);

            Vector3 directionHorizontal = directionHorizontalX + directionHorizontalZ;

            Vector3 dirHorizontalX = Vector3.Project(dir, this.transform.forward);
            Vector3 dirHorizontalZ = Vector3.Project(dir, this.transform.right);

            Vector3 dirHorizontal = dirHorizontalX + dirHorizontalZ;

            Debug.DrawRay(position, directionHorizontal.normalized, Color.blue,10f);
            Debug.DrawRay(position, dirHorizontal.normalized, Color.red, 10f);

            float angle= Vector3.Angle(dirHorizontal, directionHorizontal);
           

            float val = mult* (1000f / angle);
           // Debug.Log(val);
            forcevalues[i] = val;

        }


        for (int i=0; i< wheels.Length; i++)
        {
            wheels[i].forceAdd -= forcevalues[i];
        }
        yield return new WaitForSeconds(0.2f);

        for (int i = 0; i < wheels.Length; i++)
        {
            wheels[i].forceAdd += forcevalues[i];
        }
        yield return null;
    }
}

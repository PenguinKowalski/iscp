using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour , IDestroyable
{
    [SerializeField] float speedX, speedY, health, angle, speedXMalus, speedYMalus;
    float speedXOriginal, speedYOriginal;
    [Range(-179, 179)]
    [SerializeField] float rotationX, rotationY, rotationXmin, rotationYmin, forceStabilizer = 1;
    bool isDead;
    [SerializeField] bool useLowAngle, needsLineRenderer;
    [SerializeField] float[] resistenze;
    [SerializeField] GameObject Base;
    [SerializeField] GameObject yRotator;
    [SerializeField] GameObject xRotator;
    RaycastHit hit;
    RangeWeapon rangeWeapon;
    MediumAI mediumAI;
    InputManager inputManager;
    bool zoomSet;
    Car thisVehicle;
    Vector3 target, origine, direction;
    Mortaio isMortar;
    [SerializeField]ProjectileArc projectileArc;
    [SerializeField] List<GameObject> turretParts = new List<GameObject>();
    [SerializeField] ParticleSystem Smoke;


    void Start()
    {
        thisVehicle = GetComponentInParent<Car>();
        rangeWeapon = GetComponentInChildren<RangeWeapon>();
        inputManager = GetComponentInParent<InputManager>();  
        mediumAI = GetComponentInChildren<MediumAI>(); 
        speedXOriginal = speedX;
        speedYOriginal = speedY;
        isMortar = GetComponent<Mortaio>();
        Smoke.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (thisVehicle.IsDead)
        {
            speedX = 0;
            speedY = 0;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            health = 0;
            Debug.Log(health);
        }

        target = mediumAI.TargetAttack;
        origine = rangeWeapon.ListaCanne[rangeWeapon.IndiceCanna].position;
        SetTargetWithSpeed(target, rangeWeapon.BulletSpeed, useLowAngle, origine);
        
         
        TurretMovement();
        if (mediumAI.TargetAttack == null)
        {
            target = transform.forward;
            return;

        }

        if (health <= 0 && !isDead)
        {
            Death(null);    
            
        }
    }

    Vector3 GunDirection()
    {
        return target - Base.transform.position;
    }

    void TurretMovement()
    {

        Vector3 directionTurret = Math3d.ProjectVectorOnPlane(Vector3.up, GunDirection());

        directionTurret.y += Mathf.Tan(angle) * directionTurret.magnitude;

        //Debug.DrawRay(Base.transform.position, directionTurret,Color.red);       
        Vector3 rot = Quaternion.LookRotation(directionTurret, Base.transform.up).eulerAngles;

        //Converte la rotazione globale della torretta rispetto al target in rotazione locale.
        Quaternion targetGunRotation = Quaternion.Inverse(Base.transform.rotation) * Quaternion.Euler(rot);
        //Rotazione locale della torretta ad una data velocit�.
        Vector3 RotationY = Quaternion.RotateTowards(yRotator.transform.localRotation, targetGunRotation, speedY * Time.deltaTime).eulerAngles;
        //Clamp della rotazione attorno all'asse Y.
        RotationY.y = ClampAngle(RotationY.y, rotationYmin, rotationY);
        //Applicazione di una mashera di rotazione affinch� la torretta si muova solo attorno all'asse richiesto.
        yRotator.transform.localRotation = Quaternion.Euler(Vector3.Scale(RotationY, new Vector3(0, 1, 0)));

        
        //Rotazione locale della torretta  ad una data velocit�.
        Vector3 RotationX = Quaternion.RotateTowards(xRotator.transform.localRotation, targetGunRotation, speedX * Time.deltaTime).eulerAngles;
        //Clamp della rotazione attorno all'asse X.
        RotationX.x = ClampAngle(RotationX.x, rotationXmin, rotationX);
        //Applicazione di una mashera di rotazione affinch� la torretta si muova solo attorno all'asse richiesto.
        xRotator.transform.localRotation = Quaternion.Euler(Vector3.Scale(RotationX, new Vector3(1, 0, 0)));
        
    }

    //Normalize angles to a range from -180 to 180 an then clamp the angle with min and max.

    private float ClampAngle(float angle, float min, float max)
    {

        angle = NormalizeAngle(angle);
        if (angle > 180)
        {
            angle -= 360;
        }
        else if (angle < -180)
        {
            angle += 360;
        }

        min = NormalizeAngle(min);
        if (min > 180)
        {
            min -= 360;
        }
        else if (min < -180)
        {
            min += 360;
        }

        max = NormalizeAngle(max);
        if (max > 180)
        {
            max -= 360;
        }
        else if (max < -180)
        {
            max += 360;
        }

        //Aim is, convert angles to -180 until 180.
        return Mathf.Clamp(angle, min, max);
    }

    //If angles over 360 or under 360 degree, then normalize them.

    private float NormalizeAngle(float angle)
    {
        while (angle > 360)
            angle -= 360;
        while (angle < 0)
            angle += 360;
        return angle;
    }

    public void SetTargetWithSpeed(Vector3 point, float speed, bool useLowAngle, Vector3 origi)
    {
        direction = GunDirection();
        float yOffset = direction.y;
        direction = Math3d.ProjectVectorOnPlane(Vector3.up, direction);
        float distance = direction.magnitude;

        float angle0, angle1;
        bool targetInRange = ProjectileMath.LaunchAngle(speed, distance, yOffset, Physics.gravity.magnitude, out angle0, out angle1);

        if (targetInRange)
            angle = useLowAngle ? angle1 : angle0;

        if (projectileArc)
        {
            if (needsLineRenderer && !inputManager.AI)
            {
                projectileArc.Enable();
                projectileArc.UpdateArc(speed, distance, Physics.gravity.magnitude, angle, direction, targetInRange);
            }
            else
            {
                projectileArc.Disable();
            }
        }
      

        return;
        
        //SetTurret(direction, angle * Mathf.Rad2Deg);   
    }

    public void ChangeHealth(float differenza, float[] moltiplicatore, Car car, RaycastHit? raycastHit, Ray? ray, float bulletSpeed, float raycastLength = -1)
    {
        if (!Weapon.IsCarEnemy(car, thisVehicle))
        {
            return;
        }
        for (int i = 0; i < moltiplicatore.Length; i++)
        {
            if (health > 0)
            {
                health -= differenza * moltiplicatore[i] * resistenze[i];
            }
            else
            {
                Death(raycastHit.Value);
            }
        }
        Rigidbody rbEnemyCar = car.gameObject.GetComponent<Rigidbody>();
        if (ray.HasValue)
        {
            rbEnemyCar.AddForceAtPosition(ray.Value.direction * bulletSpeed * forceStabilizer, raycastHit.Value.point, ForceMode.Impulse);
        }
    }

    public void Death(RaycastHit? raycastHit)
    {
        speedX = speedXMalus;
        speedY = speedYMalus;
        Smoke.Play();
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(target, 0.5f);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kamikaze : MeleeWeapon
{
    public override void Attack(Collider collider)
    {
        Debug.Log("Attacking");
        IDestroyable targetPart = null;
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider collision in colliders)
        {
            Debug.DrawRay(explosionPos, (collision.gameObject.transform.position - explosionPos) * radius, Color.blue, 10f);
            Ray rayExplosion = new Ray(explosionPos, collision.gameObject.transform.position - explosionPos);
            if (Physics.Raycast(rayExplosion, radius, destroyableMask))
            {
                targetPart = collision.gameObject.GetComponent<IDestroyable>();
                if (targetPart == null)
                {
                    targetPart = collision.gameObject.gameObject.GetComponentInParent<IDestroyable>();
                }
                if (targetPart != null)
                {
                    targetPart.ChangeHealth(damage, GetDamageMultiplier(), thisVehicle, null, rayExplosion, power);
                }
            }
        }
        thisVehicle.Health = 0;
    }
}

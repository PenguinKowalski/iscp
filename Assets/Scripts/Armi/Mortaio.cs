using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mortaio : RangeWeapon
{
    [SerializeField] float spawnTime;
    float spawnTimer;
    [SerializeField] GameObject mina;
    Explosives mineSc;
    List<GameObject> listaMine = new List<GameObject>();
    [SerializeField] GameObject explosionPrefab;
    [SerializeField] AudioClip mineSpawn;


    //public Vector3 TargetPos { get => targetPos; set => targetPos = value; }
    public override void Update()
    {
        base.Update();
        spawnTimer += Time.deltaTime;
        
    }

    public override void SecondaryAttack()
    {
        if(spawnTimer >= spawnTime)
        {
            GameObject go = Instantiate(mina, thisVehicle.transform.position, thisVehicle.transform.rotation);
            mineSc = go.GetComponent<Explosives>();
            mineSc.ThisCar = thisVehicle;
            listaMine.Add(go);
            spawnTimer = 0;
            theseAudios[0].PlayOneShot(mineSpawn);
        }
    }

    public override void SimulateBulletPhysics(Proiettile proiettile)
    {
        Vector3 point1 = proiettile.bullet.transform.position;
        for (float step = 0; step < stepLength; step += stepsize)
        {
            proiettile.bulletVelocity += Physics.gravity * stepsize * Time.deltaTime;
            Vector3 point2 = point1 + proiettile.bulletVelocity * stepsize * Time.deltaTime;
            proiettile.bullet.transform.LookAt(point2);
            Ray ray = new Ray(point1, point2 - point1);
            IDestroyable targetPart = null;
            int hitsnum = Physics.RaycastNonAlloc(ray, hits, (point2 - point1).magnitude);
            for (int j = 0; j < hitsnum; j++)
            {
                ApplyDamage(proiettile.bullet, targetPart, hits[j], ray);
            }
            point1 = point2;
            if (hitsnum == 0)
            {
                proiettile.bullet.transform.position = point1;
            }
            else
            {
                GameObject go = Instantiate(explosionPrefab, transform);
                go.SetActive(false);
                ParticleSystem explosion = go.GetComponent<ParticleSystem>();
                go.transform.position = proiettile.bullet.transform.position;
                go.gameObject.SetActive(true);
                explosion.Play();
                if(!explosion.isPlaying)
                {
                    Destroy(go);
                }
                DespawnProjectile(proiettile);
                break;
            }
        }

    }
}

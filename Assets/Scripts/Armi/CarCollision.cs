using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adrenak.Tork;

public class CarCollision : MonoBehaviour
{
    [SerializeField] enum carType { light, medium, heavy};
    [SerializeField] carType carWeight;
    [SerializeField] float baseDamage, lightMult, mediumMult, heavyMult, minimumSpeed, minimumDistance;
    Vector3 difference;
    Car enemyCar, thisVehicle;
    Vehicle thisVelocity;
    Rigidbody thisRb;
    float multiplier, differenceMagnitude;
    [SerializeField] float[] damageMultiplier;
    [SerializeField] ParticleSystem sparks;
    [SerializeField] bool isWeapon;
    [SerializeField] GameObject audioSource;
    AudioSource thisAudio;
    [SerializeField] List<AudioClip> collisionSounds = new List<AudioClip>();
    
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(damageMultiplier.Length);
        thisVehicle = GetComponentInParent<Car>();
        thisRb = thisVehicle.GetComponent<Rigidbody>();
        thisVelocity = GetComponentInParent<Vehicle>();
        sparks.Stop();
        thisAudio = audioSource.GetComponent<AudioSource>();
        thisAudio.Stop();
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.GetComponentInParent<Car>() != thisVehicle)
        {
            thisAudio.PlayOneShot(collisionSounds[Random.Range(0, collisionSounds.Count)]);
        }
        
        float speed = thisVehicle.Speed;
        enemyCar = collision.collider.gameObject.GetComponentInParent<Car>();
        if (enemyCar != null && !enemyCar.IsDead)
        {
            
            foreach (ContactPoint contact in collision.contacts)
            {
                sparks.gameObject.transform.position = contact.point;
                sparks.Play();
            }
            if (carWeight == carType.light)
            {
                multiplier = lightMult;
            }
            if (carWeight == carType.medium)
            {
                multiplier = mediumMult;
            }
            if (carWeight == carType.heavy)
            {
                multiplier = heavyMult;
            }
            difference = thisVehicle.gameObject.transform.forward - enemyCar.gameObject.transform.forward;
            differenceMagnitude = difference.magnitude;
            if (difference.magnitude >= minimumDistance || difference.magnitude <= -minimumDistance)
            {
                if (isWeapon)
                {
                    enemyCar.ChangeHealth(baseDamage, damageMultiplier, thisVehicle, null, null, 1);
                }
                else
                {
                    if (thisVehicle.Speed >= minimumSpeed && thisVehicle.Speed > enemyCar.Speed)
                    {
                        enemyCar.ChangeHealth(baseDamage, damageMultiplier, thisVehicle, null, null, 1);
                    }
                }
            }
        }
    }
}
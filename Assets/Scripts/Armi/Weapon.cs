using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] protected float  damage, fireRate,  overHeatRatePrimary, overHeatRateSecondary, overHeatMaxPrimary, overHeatMaxSecondary, coolDown, destCarMult, allowedDistance, minAiTime = 3f, maxAiTime = 25f, radius, power;
    protected float fireRateTimer, overHeatPrimary, overHeatSecondary, aiTime;
    [SerializeField] protected int  maxFiringTypes;
    protected int fireSelection;

    [SerializeField] List<GameObject> listaParti = new List<GameObject>();
    [SerializeField] public enum collisionType { single, explosion }
    [SerializeField] public collisionType collType;
    [SerializeField] protected LayerMask destroyableMask, criticalLayer;
    [SerializeField] public enum WeaponType { _long, _medium, _short, _melee}
    [SerializeField] WeaponType WpnType;
    protected Car thisVehicle;
    protected bool vehicleFound, isShootingPrimary, isShootingSecondary;
    protected bool canFirePrimary, canFireSecondary;
    protected InputManager inputManager;
    protected MediumAI aiStates;
    [SerializeField]
    private float maxDistance;
    [SerializeField]
    private float minDistance;
    [SerializeField] protected int maxAmmo;
    protected int currentAmmo;
    [SerializeField] float[] damageMultiplier;
    [SerializeField] string weaponName;
    bool canShootTarget;
    public WeaponType WpnType1 { get => WpnType; set => WpnType = value; }
    public float MaxDistance { get => maxDistance; set => maxDistance = value; }
    public float MinDistance { get => minDistance; set => minDistance = value; }
    public int CurrentAmmo { get => currentAmmo; set => currentAmmo = value; }
    public string WeaponName { get => weaponName; set => weaponName = value; }
    public bool CanShootTarget { get => canShootTarget; set => canShootTarget = value; }
    public float OverHeatMaxSecondary { get => overHeatMaxSecondary; set => overHeatMaxSecondary = value; }
    public List<GameObject> ListaParti { get => listaParti; set => listaParti = value; }

    public virtual void Start()
    {
        maxFiringTypes += 1;
        StartCoroutine(AIFireChoice());
    }
    protected IEnumerator AIFireChoice()
    {
        while (true)
        {
            aiTime = Random.Range(minAiTime, maxAiTime);
            fireSelection = Random.Range(1, maxFiringTypes);
            //Debug.Log(fireSelection + " , " + aiTime);
            yield return new WaitForSeconds(aiTime);
        }
        yield return null;
    }
    public static bool IsCarEnemy(Car car, Car thisCar)
    {
        if (car == null) return false;
        if (thisCar == null) return false;
        if (car.Equals(thisCar))
        {
           // Debug.Log("car is the sender");
            return false;
        }
        if (car.Team == thisCar.Team)
        {
            //Debug.Log("car is same team");
            return false;
        }
        //Debug.Log("car is same enemy");
        return true;
    }
    protected float GetBluntDamage()
    {
        return damageMultiplier[0];
    }
    protected float GetPierceDamage()
    {
        return damageMultiplier[1];
    }
    public float[] GetDamageMultiplier()
    {
        return damageMultiplier;
    }
    public float GetOverheatPrimary()
    {
        return (overHeatPrimary / overHeatMaxPrimary) * 100;
    }
    public float GetOverheatSecondary()
    {
        return (overHeatSecondary / overHeatMaxSecondary) * 100;
    }
    protected void AOEDamage(GameObject bullet, IDestroyable targetPart, RaycastHit? hit)
    {
        Vector3 explosionPos = bullet.transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider collision in colliders)
        {
            Debug.DrawRay(explosionPos, (collision.gameObject.transform.position - explosionPos) * radius, Color.blue, 10f);
            Ray rayExplosion = new Ray(explosionPos, collision.gameObject.transform.position - explosionPos);
            if (Physics.Raycast(rayExplosion, radius, destroyableMask))
            {
                if(hit.HasValue)
                {
                    OnHit(rayExplosion, collision.gameObject, power, targetPart, hit.Value);
                }
                else
                {
                    OnHit(rayExplosion, collision.gameObject, power, targetPart, null);
                }
            }
        }
    }

    protected void OnHit(Ray ray, GameObject collidedObj, float hitForce, IDestroyable targetPart, RaycastHit? hit)
    {
        targetPart = collidedObj.GetComponent<IDestroyable>();
        if (targetPart == null)
        {
            targetPart = collidedObj.gameObject.GetComponentInParent<IDestroyable>();
        }
        if (targetPart != null)
        {
            if (hit.HasValue)
            {
                targetPart.ChangeHealth(damage, GetDamageMultiplier(), thisVehicle, hit.Value, ray, hitForce);
            }
            else
            {
                targetPart.ChangeHealth(damage, GetDamageMultiplier(), thisVehicle, null, ray, hitForce);
            }
            
        }
    }
    public virtual void PrimaryFire()
    {
        
    }
    public virtual void SecondaryFire()
    {
        
    }

}
[System.Serializable]
public class MuzzleSmoke
{
    [SerializeField] List<ParticleSystem> muzzleSmokes = new List<ParticleSystem>();

    public List<ParticleSystem> MuzzleSmokes { get => muzzleSmokes; set => muzzleSmokes = value; }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameThrower_Damage : MonoBehaviour
{
    Lanciafiamme flamethrower;
    bool firing;
    float damage;
    Car thisVehicle;
    float fireRate;
    Car enemyCar;
    Weapon weapon;
    List<Car> collidingCars;
    List<Coroutine> listaCoroutine;

    public bool Firing { get => firing; set => firing = value; }

    // Start is called before the first frame update
    public void GetInformation(bool firing_, float damage_, Car thisVehicle_, float fireRate_, Car enemyCar_, List<Car> collidingCars_, List<Coroutine> listaCoroutine_, Lanciafiamme lanciafiamme_)
    {
        flamethrower = lanciafiamme_;
        firing = firing_;
        damage = damage_;
        thisVehicle = thisVehicle_;
        fireRate = fireRate_;
        enemyCar = enemyCar_;
        collidingCars = collidingCars_;
        listaCoroutine = listaCoroutine_;
    }
    /*  IEnumerator Fire(Car car)
      {
          while (true)
          {
              if (firing)
              {
                  car.ChangeHealth(damage, flamethrower.GetDamageMultiplier(), thisVehicle, null, null, 0);
                  Debug.Log("Injured " + car.name);
              }
              yield return new WaitForSeconds(fireRate);
          }
          yield return null;
      }*/
    private void OnParticleCollision(GameObject other)
    {
        enemyCar = other.GetComponentInParent<Car>();
        if (Weapon.IsCarEnemy(enemyCar, thisVehicle))
        {
            if (firing)
            {
                enemyCar.ChangeHealth(damage, flamethrower.GetDamageMultiplier(), thisVehicle, null, null, 0);
                Debug.Log("Injured " + enemyCar.name);
            }
        }
    }
   /* private void OnTriggerEnter(Collider other)
    {
        enemyCar = other.gameObject.GetComponentInParent<Car>();
        /*Debug.Log(other.name);
        Debug.Log(enemyCar);
        if (Weapon.IsCarEnemy(enemyCar, thisVehicle))
        {
            Debug.Log(firing);
            if (!collidingCars.Contains(enemyCar) && firing)
            {
                collidingCars.Add(enemyCar);
                Debug.Log("Added " + enemyCar.name);
                listaCoroutine.Add(StartCoroutine(Fire(enemyCar)));
            }
        }

    }
    private void OnTriggerExit(Collider other)
    {
        enemyCar = other.gameObject.GetComponentInParent<Car>();
        if (enemyCar != null && Weapon.IsCarEnemy(enemyCar, thisVehicle))
        {
            if (collidingCars.Contains(enemyCar))
            {
                for (int i = 0; i < listaCoroutine.Count; i++)
                {
                    if (enemyCar == collidingCars[i])
                    {

                        collidingCars.RemoveAt(i);
                        Debug.Log("Removed " + enemyCar.name);

                        if(listaCoroutine.Count >= 0)
                        {
                            StopCoroutine(listaCoroutine[i]);
                        }
                        listaCoroutine.RemoveAt(i);

                    }
                }
            }
        }
    }*/
}

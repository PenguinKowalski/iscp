using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    [SerializeField] float force, noRotationTime;
    float rotSpeed, usedRotationSpeed;
    [SerializeField] Rigidbody rb;
    RangeWeapon launcher;
    Transform targetPosition;
    [SerializeField]AudioSource thisAudio;
    [SerializeField] List<AudioClip> missileNoises = new List<AudioClip>();

    private void Start()
    {
        rb.velocity = Vector3.zero;
        //thisAudio.GetComponent<AudioSource>();
    }
    private void OnDisable()
    {
        if(thisAudio != null)
        {
            thisAudio.Stop();
        }
        
    }

    private void OnEnable()
    {
        if(rb != null)
        {
            rb.velocity = Vector3.zero;
        }
        thisAudio.pitch = Random.Range(0.75f, 1.25f);
        int noiseIndex = Random.Range(0, missileNoises.Count);
        thisAudio.clip = missileNoises[noiseIndex];
        thisAudio.Play();
    }

    public void StartVelocity(RangeWeapon _launcher, float startSpeed, float rotspeed)
    {
        rb.velocity =  transform.forward * force * startSpeed;
        rotSpeed = rotspeed;
        launcher = _launcher;
        StartCoroutine(AssignRotationSpeed());
    }
    void FixedUpdate()
    {
      
        rb.velocity = transform.forward * force;
        Vector3 targetPosition = launcher.TargetPos - transform.position;
        Quaternion rotation = Quaternion.LookRotation(targetPosition);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, usedRotationSpeed * Time.fixedDeltaTime);
    }

    IEnumerator AssignRotationSpeed()
    {
        yield return new WaitForSeconds(noRotationTime);
        usedRotationSpeed = rotSpeed;
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    [SerializeField] List<GameObject> listaArmi = new List<GameObject>();
    enum WeaponState { Colliding, notColliding }
    WeaponState wpnState;
    bool isAttacking, canAttack, canTurbo, turboStarted, turboActive;
    [SerializeField] float turboLifetimeMax, turboLifeTimeMin,  multiplierMin, multiplierMax, startAudioLength;
    float turboTimer, multiplier = 1;
    MeleeRange this_vehicle_melee;
    Car vehicle_target;
    bool turboStopped;
    RaycastHit hit;
    [SerializeField] ParticleSystem particles;
     AudioSource[] theseAudios;
    [SerializeField] List<AudioClip> sawSounds = new List<AudioClip>();


    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        canFirePrimary = true;
        canFireSecondary = true;
        isAttacking = false;
        canAttack = true;
        turboStarted = false;
        thisVehicle = this.GetComponentInParent<Car>();
        this_vehicle_melee = GetComponentInParent<MeleeRange>();
        turboStopped = false;
        aiStates = GetComponentInParent<MediumAI>();
        inputManager = GetComponentInParent<InputManager>();
        particles.Stop();
        theseAudios = GetComponents<AudioSource>();
        for (int i = 0; i < theseAudios.Length; i++)
        {
            theseAudios[i].Stop();
        }
    }

    IEnumerator SawSounds()
    {
        Debug.Log("starting coroutine");
        theseAudios[1].clip = sawSounds[0];
        theseAudios[1].Play();
        yield return new WaitForSeconds(startAudioLength);
        theseAudios[1].clip = sawSounds[1];
        theseAudios[1].loop = true;
        theseAudios[1].Play();
    }
    
    void Update()
    {
        //wpnState = WeaponState.notColliding;
        CanAttack();
        fireRateTimer += Time.deltaTime;

        if (aiStates.AIState != MediumAI.AIStates.death)
        {
            if (canFireSecondary && !isShootingPrimary)
            {
                SecondaryFire();
                if (!inputManager.AI)
                {
                    SecondaryInputs();
                }
            }
        }
        if(particles != null && damage > 0)
        {
            if (wpnState == WeaponState.Colliding)
            {
                particles.Play();
                if (theseAudios.Length > 0)
                {
                    StopSawSound();theseAudios[2].Play();
                }
                
            }
            else
            {
                particles.Stop();
                if (theseAudios.Length > 0)
                {
                    theseAudios[2].Stop();
                    
                }
                
            }
        }
        if(theseAudios.Length > 0)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                StartCoroutine(SawSounds());
            }
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                StopSawSound();
            }
        }

        if(theseAudios.Length > 0)
        {
            if (!Input.GetKey(KeyCode.Mouse0) && theseAudios[1].isPlaying)
            {
                StopSawSound();
            }
        }
    }
    void SecondaryInputs()
    {
         if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (!isShootingPrimary && !turboActive)
            {
                this_vehicle_melee.Turbo();
                if(theseAudios.Length > 0)
                {
                    theseAudios[0].Play();
                }
                
                // Debug.Log("started turbo");
                isShootingSecondary = true;
                turboActive = true;
                turboStopped = false;
            }
        }
        if (!canFireSecondary && turboActive)
        {
            overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
            this_vehicle_melee.StopTurbo();
            if (theseAudios.Length > 0)
            {
                theseAudios[0].Stop();
            }
            //Debug.Log("stopped turbo");
        }


        if (turboActive)
        {
            overHeatSecondary += overHeatRateSecondary * Time.deltaTime;
            if (Input.GetKeyUp(KeyCode.Mouse1) || overHeatSecondary >= overHeatMaxSecondary)
            {
                this_vehicle_melee.StopTurbo();
                if (theseAudios.Length > 0)
                {
                    theseAudios[0].Stop();
                }
                turboStopped = true;
                turboActive = false;
                //Debug.Log("stopped turbo 2");
            }
        }

        else
        {
            isShootingSecondary = false;
            if (overHeatSecondary > 0)
            {
                overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
            }
            else
            {
                overHeatSecondary = 0;
            }
        }
    }
    public virtual void Inputs(Collider collided)
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (!isShootingSecondary)
            {
                Attack(collided);
                isShootingPrimary = true;
                //StartCoroutine(SawSounds());
                if (!canFirePrimary)
                {                   
                    overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
                }
            }
        }
        else
        {
            isShootingPrimary = false;
            if (overHeatPrimary > 0)
            {
                overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
            }
            else
            {
                overHeatPrimary = 0;
                canFireSecondary = true;
            }
        }   
       /*
        if(Input.GetKeyUp(KeyCode.Mouse0))
        {
            StopSawSound();
        }*/
    }

    public void StopSawSound()
    {
        theseAudios[1].Stop();
        theseAudios[0].loop = false;
        theseAudios[1].clip = sawSounds[2];
        theseAudios[1].Play();
    }

    public virtual void Attack(Collider collided)
    {
        for (int i = 0; i < listaArmi.Count; i++)
        {
            IDestroyable targetPart = null;
            Ray spherecast = new Ray(listaArmi[i].transform.position, listaArmi[i].transform.forward);
            if(collided != null)
            {
                targetPart = collided.gameObject.GetComponent<IDestroyable>();
                if (targetPart == null)
                {
                    targetPart = collided.gameObject.GetComponentInParent<IDestroyable>(); 
                }
                if (targetPart != null)
                {
                   // wpnState = WeaponState.Colliding;
                    targetPart.ChangeHealth(damage, GetDamageMultiplier(), thisVehicle, hit, spherecast, 0, 0.5f);
                    fireRateTimer = 0;
                    targetPart = null;
                }
            }
        }
    }
    public void PrimaryFire(Collider collided)
    {

        if (overHeatPrimary >= overHeatMaxPrimary)
        {
            canFirePrimary = false;
        }

        if (inputManager.AI)
        {
            if (CanShootTarget)
            {
                isShootingPrimary = true;
                if (theseAudios.Length > 0)
                {
                    StartCoroutine(SawSounds());
                }
                if (isShootingPrimary)
                {
                    Attack(collided);

                    if (!canFirePrimary)
                    {
                        overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
                    }
                }
            }
            else
            {
                isShootingPrimary = false;
                if (theseAudios.Length > 0)
                {
                    if (theseAudios[1].isPlaying)
                    {
                        StopSawSound();
                    }
                }
                
                if (overHeatPrimary > 0)
                {
                    overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
                }
                else
                {
                    overHeatPrimary = 0;
                }
            }
        }

        if (!canFirePrimary && overHeatPrimary <= 0)
        {
            canFirePrimary = true;
        }
    }
    public override void SecondaryFire()
    {
        if (overHeatSecondary >= overHeatMaxSecondary)
        {
            canFireSecondary = false;
        }
        if (inputManager.AI && aiStates.AIState == MediumAI.AIStates.manouverAttack)
        {
            if (!Physics.Raycast(thisVehicle.gameObject.transform.position, thisVehicle.gameObject.transform.forward, 10))
            {
                if (!isShootingPrimary)
                {
                    this_vehicle_melee.Turbo();
                    if (theseAudios.Length > 0)
                    {
                        theseAudios[0].Play();
                    }
                    // Debug.Log("Started turbo 2");
                    isShootingSecondary = true;
                    turboActive = true;
                    turboStopped = false;
                    if (!canFireSecondary)
                    {
                        overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
                    }
                }
            }
            else
            {
                isShootingSecondary = false;
                if (overHeatSecondary > 0)
                {
                    overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
                }
                else
                {
                    overHeatSecondary = 0;
                }
            }
            if (!canFireSecondary && !turboStopped && turboActive)
            {
                this_vehicle_melee.StopTurbo();
                if (theseAudios.Length > 0)
                {
                    theseAudios[0].Stop();
                }
                //Debug.Log("stopped turbo 3");
                turboStopped = true;
                turboActive = false;
            }
            if (!canFireSecondary && overHeatSecondary <= 0)
            {
                canFireSecondary = true;
            }
        }
    }
    bool CanAttack()
    {
        CanShootTarget = false;
        for (int i = 0; i < listaArmi.Count; i++)
        {
            if (Physics.Raycast(listaArmi[i].transform.position, listaArmi[i].transform.forward, out hit, 2, destroyableMask))
            {
                Car targetVehicle = hit.collider.gameObject.GetComponentInParent<Car>();
                //Debug.Log("hit" + targetVehicle);

                if (IsCarEnemy(targetVehicle, thisVehicle) && targetVehicle != null && (targetVehicle.transform.position - thisVehicle.transform.position).magnitude < allowedDistance)
                {
                    //  Debug.Log("hit 1");
                    CanShootTarget = true;

                }
                else
                {
                    CanShootTarget = false;
                }
                return CanShootTarget;
            }
        }
        return CanShootTarget;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (theseAudios.Length > 0)
        {
            theseAudios[2].Play();
        }
        StartCoroutine(ApplyDamage(other));
        wpnState = WeaponState.Colliding;
    }
    private void OnTriggerExit(Collider other)
    {
        if (theseAudios.Length > 0)
        {
            theseAudios[2].Stop();
        }
        
        StopAllCoroutines();
        wpnState = WeaponState.notColliding;
    }

    IEnumerator ApplyDamage(Collider collided)
    {
       
        while (true)
        {
            if (aiStates.AIState != MediumAI.AIStates.death)
            {
                if (canFirePrimary && fireRateTimer >= fireRate && !isShootingSecondary)
                {

                    PrimaryFire();
                    if (!inputManager.AI)
                    {
                        Inputs(collided);
                    }
                }
            }
            yield return new WaitForSeconds(fireRate);
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cecchino : RangeWeapon
{
    [SerializeField] GameObject zoomCamera;
    CameraFollow cameraPivot;
    public override void Start()
    {
        base.Start();
        cameraPivot = mainCamera.transform.parent.GetComponent<CameraFollow>();
    }
    public override void Update()
    {
        base.Update();
        if (aiStates.AIState == MediumAI.AIStates.death)
        {
            cameraPivot.ChangeBackFromSniperMode(zoomCamera);
        }
    }
    public override void Inputs()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {

            Shoot(fireRateTimer, fireRate, canFirePrimary, 0);
            isShootingPrimary = true;
            if (!canFirePrimary)
            {
                overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
            }

        }
        else
        {
            isShootingPrimary = false;
            if (overHeatPrimary > 0)
            {
                overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
            }
            else
            {
                overHeatPrimary = 0;
            }
        }
        SecondaryAttack();
    }
    public override void SecondaryAttack()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            cameraPivot.ChangeToSniperMode(zoomCamera);
        }
        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            cameraPivot.ChangeBackFromSniperMode(zoomCamera);
        }
    }
}

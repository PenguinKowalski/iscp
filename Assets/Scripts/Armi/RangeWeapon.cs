using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adrenak.Tork;
using Lean.Pool;
using UnityEngine.VFX;
using UnityEngine.Events;

public class RangeWeapon : Weapon
{
    Turret torretta;
    [SerializeField] protected GameObject prefabProiettile, bulletCasingPrefab;
    protected AudioSource[] theseAudios;
    [SerializeField] protected List<AudioClip> shotSounds = new List<AudioClip>();
    [SerializeField] protected List<AudioClip> bulletImpacts = new List<AudioClip>();
    [SerializeField] protected List<AudioClip> overheatShots = new List<AudioClip>();
    protected List<GameObject> bulletCasings = new List<GameObject>();
    [SerializeField] List<GameObject> bulletCasingOrigins = new List<GameObject>();
    protected GameObject cartellaProiettili;
    [SerializeField] protected float  lifeTime, errorAngle, recoil, collisionForce,  raycastLength, bulletSpeed, bulletCarSpeedMulti, canShootRadius;
    protected float stepLength = 1f, prevRecoil, stepDivision = 1f;
    private int indiceCanna = 0;
    [SerializeField] protected int predictionStepsPerFrame = 6;
    protected List<Proiettile> listaProiettili = new List<Proiettile>();
    [SerializeField] protected List<Transform> listaCanne = new List<Transform>();
    protected Rigidbody rbVehicle;
    protected RaycastHit hit;
    protected float lifeTimer, stepsize;
    protected Vector3 angleError3;
    protected RaycastHit[] hits = new RaycastHit[5];
    protected ConvertForceToWheel forcetowheel;
    private Vector3 targetPos;
    RotateCamera mainCameraTarget;
    protected Camera mainCamera;
    [SerializeField] protected LayerMask terrainLayer;
    [SerializeField] protected string nomeCartellaProiettili;
    [SerializeField] protected List<VisualEffect> muzzleFlash = new List<VisualEffect>();
    [SerializeField] protected List<MuzzleSmoke> muzzleFlashSmoke = new List<MuzzleSmoke>();
    public float BulletSpeed { get => bulletSpeed; set => bulletSpeed = value; }
    //public bool CanShootTarget { get => canShootTarget; set => canShootTarget = value; }
    public List<Transform> ListaCanne { get => listaCanne; set => listaCanne = value; }
    public int IndiceCanna { get => indiceCanna; set => indiceCanna = value; }
    public Vector3 TargetPos { get => targetPos; set => targetPos = value; }
    [SerializeField] bool debugDraw;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
           prevRecoil = recoil;
        theseAudios = GetComponents<AudioSource>();
        for(int i = 0; i < theseAudios.Length; i++)
        {
            theseAudios[i].Stop();
        }
        currentAmmo = maxAmmo;
        canFirePrimary = true;
        canFireSecondary = true;
        vehicleFound = false;
        inputManager = GetComponentInParent<InputManager>();
        cartellaProiettili = GameObject.Find(nomeCartellaProiettili);
        if(cartellaProiettili== null)
        {
            Debug.LogError("Manca nella scena il gameobject pool");
        }
        thisVehicle = this.GetComponentInParent<Car>();
        for(int i = 0; i < thisVehicle.OutRiggers.Count; i++)
        {
            thisVehicle.OutRiggers[i].gameObject.SetActive(false);
        }
        aiStates = GetComponent<MediumAI>();                                 
        forcetowheel = this.GetComponentInParent<ConvertForceToWheel>();
        rbVehicle = GetComponentInParent<Rigidbody>();
        stepsize = stepDivision / predictionStepsPerFrame;
        for(int i = 0; i < muzzleFlash.Count; i++)
        {
            muzzleFlash[i].Stop();
        }
        for(int j = 0; j < muzzleFlashSmoke.Count; j++)
        {
            for (int i = 0; i < muzzleFlashSmoke[j].MuzzleSmokes.Count; i++)
            {
                muzzleFlashSmoke[j].MuzzleSmokes[i].Stop();
            }
        }
        mainCamera = Arena._instance.PlayerCamera;
        mainCameraTarget = mainCamera.GetComponent<RotateCamera>();
    }

    void OverheatingSounds(float overheat, int index)
    {
        if (overheat > 0.01f)
        {
            if (!theseAudios[index].isPlaying)
            {
                theseAudios[index].Play();
            }
            if (index == 1)
            {
                theseAudios[index].pitch = GetOverheatPrimary() / 100;
            }
            else
            {
                theseAudios[index].pitch = GetOverheatSecondary() / 100;
            }

        }
        else
        {
            if (theseAudios[index].isPlaying)
            {
                theseAudios[index].Stop();
            }
        }
    }

    // Update is called once per frame
    public virtual void Update()
    {
      //  OverheatingSounds(overHeatPrimary, 1);
      //  OverheatingSounds(overHeatSecondary, 2);
        if (!inputManager.AI)
        {

            if (!mainCamera)
            {
                mainCamera = Arena._instance.PlayerCamera;
            }
            RaycastHit hit;
            if (Physics.Raycast(listaCanne[indiceCanna].transform.position, listaCanne[indiceCanna].transform.forward, out hit, 5000))
            {
                Vector3 newPosition = mainCamera.WorldToScreenPoint(hit.point);
                //mainCameraTarget.MirinoReale.transform.position = newPosition;
            }
            CalculateTrajectory();
        }
        
        
        CanShoot();
        if(currentAmmo > 0)
        {
            fireRateTimer += Time.deltaTime;
            for (int i = 0; i < listaProiettili.Count; i++)
            {
                SimulateBulletPhysics(listaProiettili[i]);
            }
            if (aiStates.AIState != MediumAI.AIStates.death)
            {
                if (!inputManager.AI)
                {
                    Inputs();
                }
                else
                {
                    if(fireSelection == 1)
                    {
                        if (CanShootTarget)
                        {
                            isShootingPrimary = true;
                            if (isShootingPrimary)
                            {
                                Shoot(fireRateTimer, fireRate, canFirePrimary, 0);

                                if (!canFirePrimary)
                                {
                                    overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
                                }
                            }
                        }
                        else
                        {
                            isShootingPrimary = false;
                            if (overHeatPrimary > 0)
                            {
                                overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
                            }
                            else
                            {
                                overHeatPrimary = 0;
                            }
                        }
                    }
                    if (fireSelection == 3)
                    {
                        if (fireSelection == 1)
                        {
                            if (CanShootTarget)
                            {
                                isShootingSecondary = true;
                                if (isShootingSecondary)
                                {
                                    SecondaryAttack();

                                    if (!canFireSecondary)
                                    {
                                        overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
                                    }
                                }
                            }
                            else
                            {
                                isShootingSecondary = false;
                                if (overHeatSecondary > 0)
                                {
                                    overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
                                }
                                else
                                {
                                    overHeatSecondary = 0;
                                }
                            }
                        }
                    }
                }
                PrimaryFire();
                SecondaryFire();
            }
            for (int i = 0; i < listaProiettili.Count; i++)
            {
                listaProiettili[i].lifeTimer += Time.deltaTime;
                if (listaProiettili[i].lifeTimer >= lifeTime)
                {
                    DespawnProjectile(listaProiettili[i]);
                }
            }
        }
    }

    public override void PrimaryFire()
    {
        if (overHeatPrimary >= overHeatMaxPrimary)
        {
            canFirePrimary = false;
        }

        if (!canFirePrimary && overHeatPrimary <= 0)
        {
            canFirePrimary = true;
        }
    }
    public override void SecondaryFire()
    {

        if (overHeatSecondary >= overHeatMaxSecondary)
        {
            canFireSecondary = false;
        }

        if (!canFireSecondary && overHeatSecondary <= 0)
        {
            canFireSecondary = true;
        }
    }

    public virtual void SecondaryAttack()
    {
        Shoot(fireRateTimer, fireRate / 2, canFireSecondary, 0) ;
    }
    public virtual void Inputs()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (!isShootingSecondary && !canFirePrimary)
            {
                theseAudios[0].PlayOneShot(overheatShots[Random.Range(0, overheatShots.Count)]);
            }
        }
        if (Input.GetKey(KeyCode.Mouse0))
        {

            if (!isShootingSecondary)
            {
                Shoot(fireRateTimer, fireRate, canFirePrimary, 0);
                isShootingPrimary = true;
                if (!canFirePrimary)
                {
                    overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
                }
            }
        }
        else
        {
            isShootingPrimary = false;
            if (overHeatPrimary > 0)
            {
                overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
            }
            else
            {
                overHeatPrimary = 0;
            }
        }


        if (Input.GetKey(KeyCode.Mouse1))
        {
            if (!isShootingPrimary)
            {
                SecondaryAttack();
                isShootingSecondary = true;
                if (!canFireSecondary)
                {
                    overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
                }
            }
        }
        else
        {
            isShootingSecondary = false;
            if (overHeatSecondary > 0)
            {
                overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
            }
            else
            {
                overHeatSecondary = 0;
            }
        }
    }
    public virtual void Shoot(float _fireRateTimer, float _fireRate, bool canFire, float? speed)
    {
        if (canFire && _fireRateTimer >= _fireRate && currentAmmo > 0)
        {
            SpawnProjectile();
            fireRateTimer = 0;
        }
    }

    public virtual void SimulateBulletPhysics(Proiettile proiettile)
    {
        Vector3 point1 = proiettile.bullet.transform.position;
        for (float step = 0; step < stepLength; step += stepsize)
        {
            proiettile.bulletVelocity += Physics.gravity * stepsize * Time.deltaTime;
            Vector3 point2 = point1 + proiettile.bulletVelocity * stepsize * Time.deltaTime;
            proiettile.bullet.transform.LookAt(point2);
            Ray ray = new Ray(point1, point2 - point1);
            IDestroyable targetPart = null;
            int hitsnum = Physics.RaycastNonAlloc(ray, hits, (point2 - point1).magnitude);
            for (int j = 0; j < hitsnum; j++)
            {
                ApplyDamage(proiettile.bullet, targetPart, hits[j], ray);
            }
            point1 = point2;
            if (hitsnum == 0)
            {
                proiettile.bullet.transform.position = point1;
            }
            else
            {
                proiettile.thisAudio.pitch = Random.Range(0.75f, 1.25f);
                int impactIndex = Random.Range(0, bulletImpacts.Count);
                proiettile.thisAudio.PlayOneShot(bulletImpacts[impactIndex]);
               /* if (!proiettile.thisAudio.isPlaying)
                {*/
                    DespawnProjectile(proiettile);
               /* }*/
                break;
            }
        }

    }
    protected void ApplyDamage(GameObject bullet, IDestroyable targetPart, RaycastHit hit, Ray ray)
    {
        if (collType == collisionType.explosion)
        {
            AOEDamage(bullet, targetPart, hit);
        }
        else
        {
            OnHit(ray, hit.collider.gameObject, bulletSpeed, targetPart, hit);
        }
    }
    public virtual GameObject SpawnProjectile()
    {
        for(int i = 0; i < bulletCasings.Count; i++)
        {
            if (!bulletCasings[i].GetComponent<ParticleSystem>().isPlaying)
            {
                Destroy(bulletCasings[i]);
                bulletCasings.RemoveAt(i);
            }
        }
        Vector3 newDir = listaCanne[indiceCanna].transform.forward;
        var v3 = Random.insideUnitCircle * errorAngle;
        newDir += v3.x * listaCanne[indiceCanna].transform.right;
        newDir += v3.y * listaCanne[indiceCanna].transform.up;
        GameObject go= LeanPool.Spawn( prefabProiettile,  listaCanne[indiceCanna].position,  Quaternion.Euler(newDir), cartellaProiettili.transform);
        Proiettile proiettile = new Proiettile(newDir * bulletSpeed + new Vector3(0 , 0, rbVehicle.velocity.z * bulletCarSpeedMulti), go);
        listaProiettili.Add(proiettile);
        go.transform.forward = newDir;
        indiceCanna = (1 + indiceCanna) % listaCanne.Count;
        currentAmmo--;
        if (isShootingPrimary)
        {
            overHeatPrimary += overHeatRatePrimary;
        }
        if (isShootingSecondary)
        {
            overHeatSecondary += overHeatRateSecondary;
        }
        if(muzzleFlash != null)
        {
            muzzleFlash[indiceCanna].Play();
        }
        if (muzzleFlashSmoke.Count > 0)
        {
            for (int i = 0; i < muzzleFlashSmoke[indiceCanna].MuzzleSmokes.Count; i++)
            {
                muzzleFlashSmoke[indiceCanna].MuzzleSmokes[i].Play();
            }
        }
        if (bulletCasingPrefab != null)
        {
            GameObject bc = Instantiate(bulletCasingPrefab, bulletCasingOrigins[indiceCanna].transform);
            bulletCasings.Add(bc);
        }
        proiettile.thisAudio = GetComponent<AudioSource>();
        theseAudios[0].pitch = Random.Range(0.75f, 1.25f);
        int soundIndex = Random.Range(0, shotSounds.Count);
        theseAudios[0].PlayOneShot(shotSounds[soundIndex]);
        forcetowheel.ApplyForce(listaCanne[indiceCanna].position, listaCanne[indiceCanna].forward);
        rbVehicle.AddForceAtPosition(-listaCanne[indiceCanna].forward * recoil, listaCanne[indiceCanna].position, ForceMode.Impulse);
        return go;
    }
    protected void DespawnProjectile(Proiettile _proiettile)
    {
        LeanPool.Despawn(_proiettile.bullet);
        listaProiettili.Remove(_proiettile);
    }
    void OnDrawGizmos()
    {
        if (debugDraw)
        {
            for (int i = 0; i < listaCanne.Count; i++)
            {
                Vector3 point1 = listaCanne[i].transform.position;
                Vector3 predictedBulletVelocity = listaCanne[i].forward * bulletSpeed;
                float stepsize = 1f / predictionStepsPerFrame;
                for (float step = 0; step < stepLength; step += stepsize)
                {
                    predictedBulletVelocity += Physics.gravity * stepsize;
                    Vector3 point2 = point1 + predictedBulletVelocity * stepsize * 0.05f;
                    Gizmos.DrawWireSphere(point1, canShootRadius * (point1 - listaCanne[i].transform.position).magnitude);  //Gizmos.DrawLine(point1, point2);
                    point1 = point2;
                }
            }
            /*for (int i = 0; i < listaCanne.Count; i++)
            {

                Vector3 point1 = listaCanne[i].transform.position;
                Vector3 predictedBulletVelocity = listaCanne[i].forward * bulletSpeed;
                float stepsize = 1f / predictionStepsPerFrame;
                Gizmos.DrawSphere(point1, canShootRadius);
                point1 = point2;
                /*for (float step = 0; step < stepLength; step += stepsize)
                {
                    predictedBulletVelocity += Physics.gravity * stepsize;
                    Vector3 point2 = point1 + predictedBulletVelocity * stepsize;
                    Ray ray = new Ray(point1, point2 - point1);
                    hits = Physics.SphereCastAll(point1, canShootRadius, point2 - point1, (point2 - point1).magnitude);
                    //int hitsnum = Physics.RaycastNonAlloc(ray, hits, (point2 - point1).magnitude);
                    /* foreach (RaycastHit q in hits)
                    {
                        Car targetVehicle = q.collider.gameObject.GetComponentInParent<Car>();
                        //Debug.Log("hit" + targetVehicle);
                        if (IsCarEnemy(targetVehicle, thisVehicle) && targetVehicle != null && (targetVehicle.transform.position - thisVehicle.transform.position).magnitude < allowedDistance)
                        {
                            //  Debug.Log("hit 1");
                            CanShootTarget = true;
                        }
                        else
                        {
                            CanShootTarget = false;
                        }
                       // return CanShootTarget;
                    }
                    point1 = point2;
                }
            }*/
        }

    }

    public virtual bool CanShoot()
    {
        CanShootTarget = false;

        for (int i = 0; i < listaCanne.Count; i++)
        {

            Vector3 point1 = listaCanne[i].transform.position;
            Vector3 predictedBulletVelocity = listaCanne[i].forward * bulletSpeed;   
            float stepsize = 1f / predictionStepsPerFrame;
            
            for (float step = 0; step < stepLength; step += stepsize)
            {
                predictedBulletVelocity += Physics.gravity * stepsize;
                Vector3 point2 = point1 + predictedBulletVelocity * stepsize;
                Ray ray = new Ray(point1, point2 - point1);
                //Debug.Dr
                float radius = canShootRadius * (point1 - listaCanne[i].transform.position).magnitude;
                if(radius < canShootRadius)
                {
                    hits = Physics.SphereCastAll(point1, radius, point2 - point1, canShootRadius/*(point2 - point1).magnitude*/);
                }
                else
                {
                    hits = Physics.SphereCastAll(point1, canShootRadius, point2 - point1, canShootRadius/*(point2 - point1).magnitude*/);
                }
                //int hitsnum = Physics.RaycastNonAlloc(ray, hits, (point2 - point1).magnitude);
                foreach (RaycastHit q in hits)
                {
                    Car targetVehicle = q.collider.gameObject.GetComponentInParent<Car>();
                    if(targetVehicle == thisVehicle)
                    {
                        targetVehicle = null;
                    }
                    Debug.Log(targetVehicle + " " + thisVehicle);
                    if (IsCarEnemy(targetVehicle, thisVehicle))
                    {
                        Debug.Log("A");
                        if (targetVehicle != null)
                        {
                            Debug.Log("B");
                            if ((targetVehicle.transform.position - thisVehicle.transform.position).magnitude < allowedDistance)
                            {
                                Debug.Log("C");
                                CanShootTarget = true;
                            }
                        }
                                             
                    }
                    else
                    {
                        Debug.Log("AAAA");
                        CanShootTarget = false;
                    }
                    return CanShootTarget;
                }
                point1 = point2;
            }
        }
        return CanShootTarget;
    }

    protected void CalculateTrajectory()
    {
        Debug.DrawRay(mainCamera.transform.position, mainCamera.transform.forward * raycastLength, Color.magenta);

        if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, raycastLength, terrainLayer))
        {
            targetPos = hit.point;
        }
        else
        {
            targetPos = mainCamera.transform.position + mainCamera.transform.forward * raycastLength;
        }
        if (!inputManager.AI)
        {
            aiStates.TargetAttack = targetPos;
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OverHeatSlider : MonoBehaviour
{
    Car thisCar;
    Weapon weapon;
    CannonAndLmg cannonAndLmg;
    [SerializeField]Slider primarySlider, secondarySlider;
    [SerializeField] Image primarySliderColour, secondarySliderColour;
    public void ChangeReferencedCar(Car newCar)
    {
        thisCar = newCar;
    }
    public void Update()
    {
        if(weapon != null)
        {
            primarySlider.value = weapon.GetOverheatPrimary();
            primarySliderColour.color = Color.Lerp(Color.white, Color.red, primarySlider.value / primarySlider.maxValue);
            if(cannonAndLmg == null)
            {
                secondarySlider.value = weapon.GetOverheatSecondary();
                secondarySliderColour.color = Color.Lerp(Color.white, Color.red, secondarySlider.value / secondarySlider.maxValue);
            }
            else
            {
                
                secondarySlider.value = cannonAndLmg.WaitingTimer;
                secondarySliderColour.color = Color.Lerp(Color.white, Color.red, secondarySlider.value / secondarySlider.maxValue);
            }
        }
        else
        {
            weapon = thisCar.Weapon;
            cannonAndLmg = weapon.GetComponent<CannonAndLmg>();
            if(cannonAndLmg != null)
            {
                secondarySlider.maxValue = cannonAndLmg.WaitingTime;
            }
            else
            {
                primarySlider.maxValue = 100;
                secondarySlider.maxValue = 100;
            }
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;

public class SemiAutomatic : RangeWeapon
{
    protected List<Proiettile> listaGranate = new List<Proiettile>();
    [SerializeField] protected GameObject prefabGranata, grenadeCasingPrefab, grenadeCasingOrigin;
    List<GameObject> grenadeCasings = new List<GameObject>();
    GameObject cartellaGranate;
    [SerializeField] string nomeCartellaGranate;
    [SerializeField] AudioClip grenadeSpawn;
    public override void Start()
    {
        base.Start();
        cartellaGranate = GameObject.Find(nomeCartellaGranate);
        if (cartellaGranate == null)
        {
            Debug.LogError("Manca nella scena il gameobject pool");
        }
    }
    public override void Update()
    {
        base.Update();
        if (currentAmmo > 0)
        {
            for (int i = 0; i < listaGranate.Count; i++)
            {
                SimulateBulletPhysicsGrenade(listaGranate[i]);
                listaGranate[i].lifeTimer += Time.deltaTime;
                if (listaGranate[i].lifeTimer >= lifeTime)
                {
                    DespawnProjectile(listaGranate[i]);
                }
            }
        }
    }
    public override void Inputs()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (!isShootingSecondary)
            {
                Shoot(fireRateTimer, fireRate, canFirePrimary, 0);
                isShootingPrimary = true;                
            }
            if (!canFirePrimary)
            {
                if (!isShootingSecondary)
                {
                    overHeatPrimary -= Time.deltaTime * coolDown;
                }                   
            }
            else
            {
                if (fireRateTimer < fireRate)
                {
                    //Debug.Log("added overheating");
                    overHeatPrimary += overHeatRatePrimary;
                }
            }

        }
        else
        {
            isShootingPrimary = false;
            if (overHeatPrimary > 0)
            {
                overHeatPrimary -= Time.deltaTime * coolDown;
            }
            else
            {
                overHeatPrimary = 0;
            }
        }


        if (Input.GetKey(KeyCode.Mouse1))
        {
            if (!isShootingPrimary)
            {
                ShootGrenade();
                isShootingSecondary = true;
                if (!canFireSecondary)
                {
                    overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
                }
            }
        }
        else
        {
            isShootingSecondary = false;
            if (overHeatSecondary > 0)
            {
                overHeatSecondary -= overHeatRateSecondary * Time.deltaTime * coolDown;
            }
            else
            {
                overHeatSecondary = 0;
            }
        }
    }

    void SimulateBulletPhysicsGrenade(Proiettile proiettile)
    {
        if (proiettile.bullet != null)
        {
            Vector3 point1 = proiettile.bullet.transform.position;
            for (float step = 0; step < stepLength; step += stepsize)
            {
                if (proiettile.bulletVelocity.magnitude > bulletSpeed * 0.5)
                {
                    proiettile.bulletVelocity += Physics.gravity * stepsize * Time.deltaTime;
                    Vector3 point2 = point1 + proiettile.bulletVelocity * stepsize * Time.deltaTime;
                    proiettile.bullet.transform.LookAt(point2);
                    Ray ray = new Ray(point1, point2 - point1);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, (point2 - point1).magnitude))
                    {
                        Vector3 newDir = Vector3.Reflect(proiettile.bulletVelocity, hit.normal);
                        proiettile.bulletVelocity = newDir * 0.75f;
                    }
                    else
                    {
                        proiettile.bullet.transform.position = point1;
                    }
                    point1 = point2;
                }
                else
                {
                    Rigidbody grenadeRb = proiettile.bullet.GetComponent<Rigidbody>();
                    if (grenadeRb == null)
                    {
                        grenadeRb = proiettile.bullet.gameObject.AddComponent<Rigidbody>();
                    }
                }
            }
        }
        else
        {
            listaGranate.Remove(proiettile);
        }
    }
     void SpawnGrenades()
    {
        for (int i = 0; i < grenadeCasings.Count; i++)
        {
            if (!grenadeCasings[i].GetComponent<ParticleSystem>().isPlaying)
            {
                Destroy(grenadeCasings[i]);
                grenadeCasings.RemoveAt(i);
            }
        }
        Vector3 newDir = listaCanne[IndiceCanna].transform.forward;
        var v3 = Random.insideUnitCircle * errorAngle;
        newDir += v3.x * listaCanne[IndiceCanna].transform.right;
        newDir += v3.y * listaCanne[IndiceCanna].transform.up;
        GameObject go = LeanPool.Spawn(prefabGranata, listaCanne[IndiceCanna].position, Quaternion.Euler(newDir), cartellaGranate.transform);
        Proiettile proiettile = new Proiettile(newDir * bulletSpeed + new Vector3(0, 0, rbVehicle.velocity.z), go);
        listaGranate.Add(proiettile);
        go.transform.forward = newDir;
        IndiceCanna = (1 + IndiceCanna) % listaCanne.Count;
        currentAmmo--;
        if (isShootingPrimary)
        {
            overHeatPrimary += overHeatRatePrimary;
        }
        if (isShootingSecondary)
        {
            overHeatSecondary += overHeatRateSecondary;
        }
        if(muzzleFlash != null)
        {
            muzzleFlash[IndiceCanna].Play();
        }
        theseAudios[0].PlayOneShot(grenadeSpawn);
        forcetowheel.ApplyForce(listaCanne[IndiceCanna].position, listaCanne[IndiceCanna].forward);
        rbVehicle.AddForceAtPosition(-newDir * recoil, listaCanne[IndiceCanna].position, ForceMode.Impulse);
        if (grenadeCasingPrefab != null)
        {
            GameObject bc = Instantiate(grenadeCasingPrefab, grenadeCasingOrigin.transform);
            bulletCasings.Add(bc);
        }
    }

    public virtual void ShootGrenade()
    {
        if (canFireSecondary && fireRateTimer >= fireRate * 10 && currentAmmo > 0)
        {
            SpawnGrenades();
            fireRateTimer = 0;
        }
    }
}

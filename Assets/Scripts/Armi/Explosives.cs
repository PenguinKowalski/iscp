using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;

public class Explosives : Weapon
{
    [SerializeField] float multiplier, particleLifeTime;
    RaycastHit hit;
    Car enemyCar, thisCar;
    [SerializeField] bool isGrenade;
    [SerializeField] ParticleSystem explosion;
    AudioSource thisAudio;
    [SerializeField] List<AudioClip> explosionSounds = new List<AudioClip>();

    public Car ThisCar { get => thisCar; set => thisCar = value; }
    public AudioSource ThisAudio { get => thisAudio; set => thisAudio = value; }

    private void OnEnable()
    {
        explosion.gameObject.SetActive(false);
        StartCoroutine(Explode());
    }
    // Start is called before the first frame update
    public override void Start()
    {
        thisAudio = GetComponent<AudioSource>();
        thisAudio.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isGrenade)
        {
            if (Physics.Raycast(transform.position, transform.up, out hit, 1f))
            {
                enemyCar = hit.collider.GetComponentInParent<Car>();
                if (IsCarEnemy(enemyCar, thisVehicle))
                {
                    Explosion();
                    Destroy(this.gameObject);
                }
            }
        }
    }
    IEnumerator Explode()
    {
        yield return new WaitForSeconds(fireRate);
        Explosion();
        yield return new WaitForSeconds(particleLifeTime);
        explosion.Stop();
        explosion.gameObject.SetActive(false);
        if (!isGrenade)
        {
            Destroy(this.gameObject);
        }
        else
        {
            LeanPool.Despawn(this.gameObject);
        }
        yield return null;
    }
    void Explosion()
    {
        int soundIndex = Random.Range(0, explosionSounds.Count);
        thisAudio.PlayOneShot(explosionSounds[soundIndex]);
        explosion.gameObject.SetActive(true);
        explosion.Play();
        IDestroyable targetPart = null;
        AOEDamage(this.gameObject, targetPart, null);
    }
}

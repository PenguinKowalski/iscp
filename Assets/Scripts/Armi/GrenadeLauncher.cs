using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeLauncher : RangeWeapon
{
    [SerializeField] int projectiles;
    int tempIndice;
    [SerializeField]LayerMask noGrenadeCollision;

    public override void Start()
    {
        base.Start();
        prevRecoil = recoil;
        // hasHitDestroyable = false;
        currentAmmo = maxAmmo;
        canFirePrimary = true;
        canFireSecondary = true;
        vehicleFound = false;
        inputManager = GetComponentInParent<InputManager>();
        cartellaProiettili = GameObject.Find(nomeCartellaProiettili);
        if (cartellaProiettili == null)
        {
            Debug.LogError("Manca nella scena il gameobject pool");
        }
        thisVehicle = this.GetComponentInParent<Car>();
        //aiStates = GetComponentInParent<MediumAI>();
        aiStates = GetComponent<MediumAI>();
        forcetowheel = this.GetComponentInParent<ConvertForceToWheel>();
        rbVehicle = GetComponentInParent<Rigidbody>();
        stepsize = stepDivision / predictionStepsPerFrame;

        mainCamera = Arena._instance.PlayerCamera;
    }

    public override void SimulateBulletPhysics(Proiettile proiettile)
    {
        if(proiettile.bullet != null)
        {
            Vector3 point1 = proiettile.bullet.transform.position;
            for (float step = 0; step < stepLength; step += stepsize)
            {
                if (proiettile.bulletVelocity.magnitude > bulletSpeed * 0.5)
                {
                    proiettile.bulletVelocity += Physics.gravity * stepsize * Time.deltaTime;
                    Vector3 point2 = point1 + proiettile.bulletVelocity * stepsize * Time.deltaTime;
                    proiettile.bullet.transform.LookAt(point2);
                    Ray ray = new Ray(point1, point2 - point1);
                    IDestroyable targetPart = null;
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, (point2 - point1).magnitude, noGrenadeCollision))
                    {
                        Vector3 newDir = Vector3.Reflect(proiettile.bulletVelocity, hit.normal);
                        proiettile.bulletVelocity = newDir * 0.75f;
                    }
                    else
                    {
                        proiettile.bullet.transform.position = point1;
                    }
                    point1 = point2;
                }
                else
                {
                    Rigidbody grenadeRb = proiettile.bullet.GetComponent<Rigidbody>();
                    if(grenadeRb == null)
                    {
                        grenadeRb = proiettile.bullet.gameObject.AddComponent<Rigidbody>();
                    }
                }
            }
        }
        else
        {            
            listaProiettili.Remove(proiettile);
        }
    }

    public override void SecondaryAttack()
    {
        if (canFireSecondary)
        {
            if (canFireSecondary && fireRateTimer >= fireRate && currentAmmo > 0)
            {
                tempIndice = IndiceCanna;
                for (int i = 0; i < projectiles; i++)
                {
                    IndiceCanna = tempIndice;
                    SpawnProjectile();
                }
                fireRateTimer = 0;
            }
        }
    }
}

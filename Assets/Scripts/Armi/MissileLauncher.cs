using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLauncher : RangeWeapon
{
   // [SerializeField] Vector3 laserTarget;
    [SerializeField] float missileRotaionSpeed;
    [SerializeField] GameObject explosionPrefab;
    //public Vector3 LaserTarget { get => laserTarget; set => laserTarget = value; }

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
      
        if (!inputManager.AI)
        {
            if (!mainCamera)
            {
                mainCamera = Arena._instance.PlayerCamera;
            }
            CalculateTrajectory();
        }


        CanShoot();
        if (currentAmmo > 0)
        {
            for (int i = 0; i < listaProiettili.Count; i++)
            {
                SimulateBulletPhysics(listaProiettili[i]);
            }
            fireRateTimer += Time.deltaTime;
            if (aiStates.AIState != MediumAI.AIStates.death)
            {
                if (!inputManager.AI)
                {
                    Inputs();
                }
                PrimaryFire();
                SecondaryFire();
            }
            for (int i = 0; i < listaProiettili.Count; i++)
            {
                listaProiettili[i].lifeTimer += Time.deltaTime;
                if (listaProiettili[i].lifeTimer >= lifeTime)
                {
                    DespawnProjectile(listaProiettili[i]);
                }
            }
        }
    }



    public override bool CanShoot()
    {
        CanShootTarget = false;

        for (int i = 0; i < ListaCanne.Count; i++)
        {

            Vector3 point1 = ListaCanne[i].transform.position;
            Vector3 predictedBulletVelocity = ListaCanne[i].forward * bulletSpeed;
            float stepsize = 1f / predictionStepsPerFrame;

            if (Physics.Raycast(ListaCanne[i].transform.position, ListaCanne[i].transform.forward, out hit, raycastLength))
            {
                Car targetVehicle = hit.collider.gameObject.GetComponentInParent<Car>();
                if (IsCarEnemy(targetVehicle, thisVehicle) && targetVehicle != null && (targetVehicle.transform.position - thisVehicle.transform.position).magnitude < allowedDistance)
                {
                    CanShootTarget = true;
                }
                else
                {
                    CanShootTarget = false;
                }
                return CanShootTarget;
            }
        }
        return CanShootTarget;
    }
    public override void Shoot(float _fireRateTimer, float _fireRate, bool canFire, float? speed)
    {
        if (canFire && _fireRateTimer >= _fireRate && currentAmmo > 0)
        {
            GiveMissileRotation(speed.Value);
            fireRateTimer = 0;
        }
    }
    public override void SecondaryAttack()
    {
        Shoot(fireRateTimer, fireRate * 2, canFireSecondary, missileRotaionSpeed);
    }
    void GiveMissileRotation(float speed)
    {
        GameObject go= SpawnProjectile();
        Missile missile= go.GetComponent<Missile>();
        missile.StartVelocity(this, thisVehicle.Speed, speed);
    }
    public override void SimulateBulletPhysics(Proiettile proiettile)
    {
        IDestroyable targetPart = null;
        Ray ray = new Ray(proiettile.bullet.transform.position, proiettile.bullet.transform.forward);
        if (Physics.Raycast(ray, out hit, 0.2f))
        {
            GameObject go = Instantiate(explosionPrefab, transform);
            go.SetActive(false);
            ParticleSystem explosion = go.GetComponent<ParticleSystem>();
            go.transform.position = proiettile.bullet.transform.position;
            go.gameObject.SetActive(true);
            explosion.Play();
            if (!explosion.isPlaying)
            {
                Destroy(go);
            }
            theseAudios[0].PlayOneShot(bulletImpacts[Random.Range(0, bulletImpacts.Count)]);
            ApplyDamage(proiettile.bullet, targetPart, hit, ray);
            DespawnProjectile(proiettile);
        }
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CannonAndLmg : RangeWeapon
{
    [SerializeField] float duration, lowerRecoil = 10, waitingTime = 10f, minimumSpeed;
    float waitingTimer;
    bool isMoving, hasLowerRecoil, hasHigherRecoil;
    //[SerializeField] float distanzaStabilizzatori;
    int recoilSelection;
    //[SerializeField] GameObject stabilizzatori;
    [SerializeField] GameObject explosionPrefab;
    [SerializeField] List<Animator> listaAnimazioni = new List<Animator>();

    public float Duration { get => duration; set => duration = value; }
    public float WaitingTime { get => waitingTime; set => waitingTime = value; }
    public float WaitingTimer { get => waitingTimer; set => waitingTimer = value; }

    public override void Start()
    {
        base.Start();
        for (int i = 0; i < thisVehicle.OutRiggers.Count; i++)
        {
            thisVehicle.OutRiggers[i].gameObject.SetActive(true);
            listaAnimazioni.Add(thisVehicle.OutRiggers[i].gameObject.GetComponent<Animator>());
        }
        isMoving = false;
    }
    public override void Update()
    {
        base.Update();

        if (thisVehicle.Speed > minimumSpeed)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }
        if (!isMoving)
        {
            waitingTimer += Time.deltaTime;
        }
        else
        {
            waitingTimer = 0;
        }
        if (!isMoving && !hasLowerRecoil && waitingTimer > waitingTime)
        {
            StartCoroutine(RecoilModifier());
            hasLowerRecoil = true;
        }
        if(isMoving && hasLowerRecoil)
        {
            StartCoroutine(RecoilModifier());
            hasLowerRecoil = false;
        }

    }
    IEnumerator RecoilModifier()
    {
        yield return new WaitForSeconds(duration);
        recoilSelection = (1 + recoilSelection) % 2;
        if (recoilSelection == 1)
        {
            recoil = lowerRecoil;
            for (int i = 0; i < listaAnimazioni.Count; i++)
            { 
                listaAnimazioni[i].SetBool("outrigger", true);
            }
        }
        else
        {
            recoil = prevRecoil;
            for (int i = 0; i < listaAnimazioni.Count; i++)
            {
                listaAnimazioni[i].SetBool("outrigger", false);
            }
        }
        yield return null;
    }
    public override void Inputs()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (!isShootingSecondary)
            {
                Shoot(fireRateTimer, fireRate, canFirePrimary, 0);
                isShootingPrimary = true;
                if (!canFirePrimary)
                {
                    overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
                }
            }
        }
        else
        {
            isShootingPrimary = false;
            if (overHeatPrimary > 0)
            {
                overHeatPrimary -= overHeatRatePrimary * Time.deltaTime * coolDown;
            }
            else
            {
                overHeatPrimary = 0;
            }
        }
    }
    public override void SimulateBulletPhysics(Proiettile proiettile)
    {
        Vector3 point1 = proiettile.bullet.transform.position;
        for (float step = 0; step < stepLength; step += stepsize)
        {
            proiettile.bulletVelocity += Physics.gravity * stepsize * Time.deltaTime;
            Vector3 point2 = point1 + proiettile.bulletVelocity * stepsize * Time.deltaTime;
            proiettile.bullet.transform.LookAt(point2);
            Ray ray = new Ray(point1, point2 - point1);
            IDestroyable targetPart = null;
            int hitsnum = Physics.RaycastNonAlloc(ray, hits, (point2 - point1).magnitude);
            for (int j = 0; j < hitsnum; j++)
            {
                ApplyDamage(proiettile.bullet, targetPart, hits[j], ray);
            }
            point1 = point2;
            if (hitsnum == 0)
            {
                proiettile.bullet.transform.position = point1;
            }
            else
            {
                if(collType == collisionType.explosion)
                {
                    GameObject go = Instantiate(explosionPrefab, transform);
                    go.SetActive(false);
                    ParticleSystem explosion = go.GetComponent<ParticleSystem>();
                    go.transform.position = proiettile.bullet.transform.position;
                    go.gameObject.SetActive(true);
                    explosion.Play();
                    if (!explosion.isPlaying)
                    {
                        Destroy(go);
                    }
                }
                DespawnProjectile(proiettile);
                break;
            }
        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : RangeWeapon
{
    [SerializeField] int projectiles;
    int tempIndice;
    // Start is called before the first frame update

    public override void SecondaryAttack()
    {
        if (canFireSecondary)
        {
            if (canFireSecondary && fireRateTimer >= fireRate * 2 && currentAmmo > 0)
            {
                for (int i = 0; i < ListaCanne.Count; i++)
                {
                    for (int j = 0; j < projectiles; j++)
                    {
                        SpawnProjectile();
                    }
                    fireRateTimer = 0;
                }
            }
        }
    }
    public override void Shoot(float _fireRateTimer, float _fireRate, bool canFire, float? speed)
    {
        
        if (canFire)
        {
            if (canFire && _fireRateTimer >= _fireRate && currentAmmo > 0)
            {
                tempIndice = IndiceCanna;
                for (int i = 0; i < projectiles; i++)
                {
                    IndiceCanna = tempIndice;
                    SpawnProjectile();
                } 
                fireRateTimer = 0;
            }
        }
    }
}

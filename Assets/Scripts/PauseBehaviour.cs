using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class PauseBehaviour : MonoBehaviour
{
    public int paused = 0;
    float speed = 1;

    const float speedNormal=1f, speedFast=2f, speedVeryFast=3f;
    [SerializeField] GameObject pausedText;
    [SerializeField] Button[] speedControls;

    [SerializeField] Color selectedColor,unSelectedColor;
    //Color unSelectedColor;

    private void Start()
    {
        UpdatePausedGUI();
       // unSelectedColor= speedControls[0].colors.normalColor;
    }
    public void Pause()
    {
        paused++;
        Time.timeScale = 0;
        UpdatePausedGUI();
    }

    public void UnPause()
    {
        paused--;
        if (paused <= 0)
        {
            Time.timeScale = speed;
            paused = 0;
        }
        UpdatePausedGUI();
    }



    void UpdatePausedGUI()
    {
        ColorBlock colors;
       

        int found = 0;

        if (Mathf.Approximately(Time.timeScale, speedNormal))
        {
            pausedText.SetActive(false);
            found = 1;
        }
        else if (Mathf.Approximately(Time.timeScale, speedFast))
        {
            pausedText.SetActive(false);
            found = 2;
        }
        else if (Mathf.Approximately(Time.timeScale, speedVeryFast))
        {
            pausedText.SetActive(false);
            found = 3;
        }
        else
        {
            pausedText.SetActive(true);
        }

        for (int i = 0; i < speedControls.Length; i++)
        {
            colors = speedControls[i].colors;
            if (i == found)
            {
                colors.normalColor = selectedColor;
                colors.selectedColor = selectedColor;
            }
            else
            {
                colors.normalColor = unSelectedColor;
            }
           
           
            speedControls[i].colors = colors;
        }
    }

    public void SetSpeed(float newspeed)
    {
        if (Mathf.Approximately(Time.timeScale, 0))
        {
            UnPause();
        }
        speed = newspeed;
        Time.timeScale = speed;
        
        UpdatePausedGUI();


    }

    public void SetSpeedPause_Button()
    {
        if (paused == 0)
        {
            Pause();
        }
        else
        {
            UnPause();
        }
        UpdatePausedGUI();
    }
    public void SetSpeedNormal_Button()
    {
        SetSpeed(speedNormal);
    }
    public void SetSpeedFast_Button()
    {
        SetSpeed(speedFast);
    }
    public void SetSpeedVeryFast_Button()
    {
        SetSpeed(speedVeryFast);
    }
}

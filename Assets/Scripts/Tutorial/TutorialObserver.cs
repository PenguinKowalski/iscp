using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TutorialObserver : MonoBehaviour
{

    [SerializeField] List<Tutorial> tutorialList;
    Dictionary<string, Tutorial> hashTutorials;

    [SerializeField] GameObject canvasTutorial;
    [SerializeField] Image imageSource;
    [SerializeField] TextMeshProUGUI textNextButton;
    [SerializeField] Button nextButton, prevButton;

    string nextText = "NEXT", endText ="CONTINUE";

    int index = 0;
    Tutorial actualTutorial;
    private void Start()
    {
        hashTutorials = new Dictionary<string, Tutorial>();
        for (int i = 0; i < tutorialList.Count; i++)
            hashTutorials.Add(tutorialList[i].Id, tutorialList[i]);
    }

    public void StartTutorial(string tutId)
    {      
        if(! hashTutorials.TryGetValue(tutId, out actualTutorial))
        {
            return;
        }

        if (!actualTutorial.DoneOnce)
        {          
            OpenTutorialScreen();
        }
        UpdateButtons();
    }

    public void OpenTutorialScreen()
    {
        actualTutorial.DoneOnce = true;
        index = 0;
        OpenWorld._instance.PauseBehaviour.Pause();
        canvasTutorial.SetActive(true);

        imageSource.sprite = actualTutorial.Images[index];
    }

    public void CloseTutorialScreen()
    {
        OpenWorld._instance.PauseBehaviour.UnPause();
        canvasTutorial.SetActive(false);
        actualTutorial = null;
    }

    public void NextPage_Button()
    {
        index++;
        if (index < actualTutorial.Images.Count)
        {         
            imageSource.sprite = actualTutorial.Images[index];
        }
        else
        {
            CloseTutorialScreen();
        }

        UpdateButtons();

    }

    void UpdateButtons()
    {
     
        if (index == 0)
        {
            prevButton.interactable = false;
        }
        else
        {
            prevButton.interactable = true;
        }

        if (actualTutorial==null) return;

        if (index == actualTutorial.Images.Count - 1)
        {
            textNextButton.text = endText;
        }
        else
        {
            textNextButton.text = nextText;
        }
    }
    public void BackPage_Button()
    {
        if (index > 0)
        {
            index--;
            imageSource.sprite = actualTutorial.Images[index];
        }

        UpdateButtons();
    }

}

[System.Serializable]
public class Tutorial
{
    [SerializeField] string id;
    [SerializeField] bool doneOnce;
    [SerializeField]  List<Sprite> images;

    public string Id { get => id; set => id = value; }
    public bool DoneOnce { get => doneOnce; set => doneOnce = value; }
    public List<Sprite> Images { get => images; set => images = value; }
}


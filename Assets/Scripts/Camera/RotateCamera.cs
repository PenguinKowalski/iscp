﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    [SerializeField] GameObject pivot;
    [SerializeField] Car lookObject;
    [SerializeField] float speed, rotationXPrev, rotationYPrev;
    bool isSniper;

    [SerializeField] LayerMask Car;

    float rotationX;
    float rotationY;

    [SerializeField] GameObject targetPosition/*, mirinoReale*/;

    public GameObject TargetPosition { get => targetPosition; set => targetPosition = value; }
    public Car LookObject { get => lookObject; set => lookObject = value; }
    //public GameObject MirinoReale { get => mirinoReale; set => mirinoReale = value; }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        isSniper = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        rotationX += Input.GetAxis("Mouse Y") * -speed * Time.deltaTime;
        rotationY += Input.GetAxis("Mouse X")* speed * Time.deltaTime;

        if(!isSniper)
        {
            pivot.transform.localRotation = Quaternion.Euler(rotationX, rotationY, 0f);
        }
        else
        {
            rotationY = Mathf.Clamp(rotationY, -90, 90);
            rotationX = Mathf.Clamp(rotationX, -90, 90);
            pivot.transform.localRotation = Quaternion.Euler(rotationX, rotationY, 0f);
        }

        if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, Car))
        {
            lookObject = hit.collider.GetComponentInParent<Car>();
        }
        else
        {
            lookObject = null;
        }

    }

    public void SetSniperBool(bool setNewValue)
    {
        isSniper = setNewValue;
    }
}

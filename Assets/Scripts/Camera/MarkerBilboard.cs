using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerBilboard : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Arena._instance.PlayerCamera != null)
        {
            transform.LookAt(Arena._instance.PlayerCamera.gameObject.transform, new Vector3(0, 1, 0));
        }
    }
}

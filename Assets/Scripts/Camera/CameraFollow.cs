using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	[SerializeField] Transform target;

	[SerializeField] Camera cam;



	[SerializeField] float smoothSpeed = 0.125f;
	private Vector3 velocity = Vector3.zero;
	[SerializeField] Vector3 offset;
    [SerializeField] Vector3 camOffset= new Vector3(0,0,-7f);

	[SerializeField] LayerMask mask;
	[SerializeField] float startDistanceCam;

    bool isUsingSniper;
    Transform posizioneCanna;


    RaycastHit hit = new RaycastHit() ;

    private void Start()
    {
      //  camOffset = cam.transform.localPosition;
    }
    void LateUpdate()
	{
		Vector3 desiredPosition = target.position + offset;
	    //Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
		//transform.position = smoothedPosition;

		Vector3 camdir = cam.transform.position - this.transform.position;
        Vector3 targetPosition;
        if ( !isUsingSniper)
        {
            targetPosition = target.TransformPoint(offset);

            // Smoothly move the camera towards that target position
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothSpeed);

            //Debug.DrawRay(this.transform.position, camdir, Color.blue);

            if (Physics.Raycast(transform.position, camdir, out RaycastHit hit, startDistanceCam, mask))
            {
                cam.transform.position = hit.point - camdir * 0.05f;
            }
            else
            {
                cam.transform.localPosition = camOffset;
            }

        }
	}

    public void ResetCamera(Transform _target)
    {
        target = _target;
        cam.transform.localPosition = camOffset;
        startDistanceCam = (cam.transform.position - this.transform.position).magnitude;
        isUsingSniper = false;
    }
    public void ResetCamera(Transform _target, Vector3 _camOffset)
    {
        target = _target;
        cam.transform.localPosition = _camOffset;
        startDistanceCam = (cam.transform.position - this.transform.position).magnitude;
        isUsingSniper = false;
    }

    public void ChangeToSniperMode(GameObject zoomCamera)
    {
        zoomCamera.SetActive(true);
        cam.enabled = false;
        isUsingSniper = true;
        cam.GetComponent<RotateCamera>().SetSniperBool(true);
    }

    public void ChangeBackFromSniperMode(GameObject zoomCamera)
    {
        cam.enabled = true;
        zoomCamera.SetActive(false);
        isUsingSniper = false;
        cam.GetComponent<RotateCamera>().SetSniperBool(false);
    }
}

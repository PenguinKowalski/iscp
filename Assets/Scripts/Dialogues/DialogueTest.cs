using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using CleverCrow.Fluid.Databases;

public class DialogueTest : MonoBehaviour
{
    [SerializeField] TMP_InputField idSpace;
    [SerializeField] GameObject idSpaceCanvas;
    [SerializeField] DialogueManager manager;
    [SerializeField] TextMeshProUGUI nodeIdText;

    [SerializeField] string[] keydataKey;
    [SerializeField] string[] keydataValue;

    public GameObject IdSpaceCanvas { get => idSpaceCanvas; set => idSpaceCanvas = value; }

    public void Start()
    {

        for (int i=0; i< keydataKey.Length; i++)
        {
            GlobalDatabaseManager.Instance.Database.Strings.Set(keydataKey[i], keydataValue[i]);
        }
      

    }

    // Start is called before the first frame update
    public void AssignText()
    {
        manager.AssignDialogue(idSpace.text, ReActivateId, null,idSpaceCanvas.gameObject);
    }
    public void ReActivateId(string nodeId)
    {
        idSpace.text = "";
        idSpaceCanvas.SetActive(true);
        nodeIdText.text = nodeId;
    }

}

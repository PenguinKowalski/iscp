using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class JsonReader : MonoBehaviour
{
    Dictionary<string, string> txtAssets;

    // Start is called before the first frame update
    void Awake()
    {
        string jsonTextFile = Resources.Load<TextAsset>("dialoguesTest").ToString();
       
        txtAssets = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonTextFile);

        //Debug.Log(txtAssets["0001"]);
    }

    public string ReadJson(string id)
    {
        if (txtAssets.ContainsKey(id))
        {
            return txtAssets[id];
        }
        else
        {
            //Debug.LogWarning("non esiste nessun testo associato all'id " + id);
            return "";
        }
    }
}


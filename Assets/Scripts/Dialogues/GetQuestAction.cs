using CleverCrow.Fluid.Dialogues.Actions;
using CleverCrow.Fluid.Dialogues;
using CleverCrow.Fluid.Databases;

[CreateMenu("Custom/Get Quests")]
public class GetQuestAction : ActionDataBase
{
    
    public override void OnInit(IDialogueController dialogue)
    {
        // Run the first time the action is triggered
    }

    public override void OnStart()
    {
        // Runs when the action begins triggering
        // DialogueManager manager = FindObjectOfType<DialogueManager>();
        DialogueManager._manager.SetQuestDialogue();


        //
    }

    public override ActionStatus OnUpdate()
    {
        // Runs when the action begins triggering

        // Return continue to span multiple frames
        return ActionStatus.Success;
    }

    public override void OnExit()
    {


    }

    public override void OnReset()
    {
   
        // Runs after a node has fully run through the start, update, and exit cycle
    }
}

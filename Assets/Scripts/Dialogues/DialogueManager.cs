using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CleverCrow.Fluid.Databases;
using TMPro;
using CleverCrow.Fluid.Dialogues;
using UnityEngine.Events;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager _manager;
    [SerializeField] List<DialogueData> dialogueList = new List<DialogueData>();
    [SerializeField] DialogueUI dialogueData;

    UnityAction<string> dialogueEvent;

    public UnityAction<string> DialogueEvent { get => dialogueEvent; set => dialogueEvent = value; }
    public List<DialogueData> DialogueList { get => dialogueList; set => dialogueList = value; }
    public DialogueUI DialogueData { get => dialogueData; set => dialogueData = value; }

    private void Awake()
    {
        if (_manager == null)
        {
            _manager = this;
        }
    }

    private void Start()
    {
        for (int i = 0; i < dialogueList.Count; i++)
        {
            if(dialogueList[i].DialogueId == "")
            {
                dialogueList[i].DialogueId = dialogueList[i].RandomizeID();
            }
        }

        DialogueData = OpenWorld._instance.GetDialogueUI();
    }

   
    public bool AssignDialogue(string dialogueId, UnityAction<string> unityAction, InteractableObject interactableObject, GameObject deactivationObject=null)
    {

        Debug.Log("START DIALOGUE " +  dialogueId);

        for (int i = 0; i < dialogueList.Count; i++)
        {
            if(dialogueList[i].DialogueId.Equals(dialogueId))
            {
                //Debug.Log("Found correct ID");
                dialogueData.SetDialogue(dialogueList[i].ThisDialogue, deactivationObject, unityAction, interactableObject);
                dialogueEvent = unityAction;
               
                return true;
            }
          
        }

        Debug.LogError("There is no coinciding ID");
        return false;
        
    }

    public void SetQuestDialogue()
    {
        Debug.Log("setQuest");
       
        DialogueData.ClearChoices();
        DialogueData.ClearEndEvents();


        DialogueData.SetDialogueQuest(OpenWorld._instance.GetActualAvailableQuests(), JumpToDialogue); //TODO
    }


    void JumpToDialogue(int id)
    {
        List<QuestData> quests = OpenWorld._instance.GetActualAvailableQuests();

        //Remove Quest from interactableobject
        InteractableObject oldQuest = OpenWorld._instance.GetInteractableObjectFromID(quests[id].ObjectiveID);
         if (oldQuest)   
            oldQuest.RemoveQuest(quests[id]);

        DialogueData.ClearEndEvents();
        AssignDialogue(quests[id].DialogueID, quests[id].NextStep, null);


    }
}

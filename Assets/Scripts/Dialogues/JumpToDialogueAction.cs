using CleverCrow.Fluid.Dialogues.Actions;
using CleverCrow.Fluid.Dialogues;
using CleverCrow.Fluid.Databases;

[CreateMenu("Custom/Jump To Dialogue")]
public class JumpToDialogueAction : ActionDataBase
{
    
    public override void OnInit(IDialogueController dialogue)
    {
        // Run the first time the action is triggered
    }

    public override void OnStart()
    {
        // Runs when the action begins triggering
        // DialogueManager manager = FindObjectOfType<DialogueManager>();
        DialogueManager manager = DialogueManager._manager;

        manager.DialogueData.ClearEndEvents();
        manager.AssignDialogue(GlobalDatabaseManager.Instance.Database.Strings.Get("Dialogue"), manager.DialogueEvent , null);
        /*
        QuestTarget targ = enemyScript.GetComponent<QuestTarget>();
        QuestData quest = CarListOpenWorld.Instance.GetQuestData(targ.QuestID);

        DialogueManager._manager.AssignDialogue(quest.DialogueID, quest.NextStep, null);
        Destroy(targ);
        enemyScript.InteractableData.HasQuest = false;
        */
        //
    }

    public override ActionStatus OnUpdate()
    {
        // Runs when the action begins triggering

        // Return continue to span multiple frames
        return ActionStatus.Success;
    }

    public override void OnExit()
    {
        // Runs when the actions `OnUpdate()` returns `ActionStatus.Success`
    }

    public override void OnReset()
    {
   
        // Runs after a node has fully run through the start, update, and exit cycle
    }
}

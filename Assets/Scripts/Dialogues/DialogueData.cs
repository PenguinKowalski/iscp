using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CleverCrow.Fluid.Databases;
using CleverCrow.Fluid.Dialogues.Graphs;

[System.Serializable]
public class DialogueData
{
    [SerializeField] DialogueGraph thisDialogue;
    [SerializeField] string dialogueId;

    const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";

    public DialogueData(DialogueGraph thisDialogue, string dialogueId)
    {
        this.thisDialogue = thisDialogue;
        this.dialogueId = dialogueId;
    }

    public DialogueGraph ThisDialogue { get => thisDialogue; set => thisDialogue = value; }
    public string DialogueId { get => dialogueId; set => dialogueId = value; }

    public string RandomizeID()
    {
        string dialogueId = "";

        for (int i = 0; i < 10; i++)
        {
            dialogueId += glyphs[Random.Range(0, glyphs.Length)];
        }

        return dialogueId;
    }
}

﻿using System.Collections;
using System.Linq;
using CleverCrow.Fluid.Databases;
using CleverCrow.Fluid.Dialogues.Graphs;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CleverCrow.Fluid.Dialogues.Examples;
using UnityEngine.Events;
using System.Collections.Generic;

namespace CleverCrow.Fluid.Dialogues {
    public class DialogueUI : MonoBehaviour {
        private DialogueController _ctrl;

        string nodeId;

        public DialogueGraph dialogue;

        public GameObjectOverride[] gameObjectOverrides;

        [Header("Graphics")]
        public GameObject speakerContainer;
        public Image portrait;
        public TextMeshProUGUI lines;
        public TextMeshProUGUI actorName;

        public RectTransform choiceList;
        public ChoiceButton choicePrefab;

   
        void PlayDialogue () {

            var database = new DatabaseInstanceExtended();
           _ctrl = new DialogueController(database);

           _ctrl.Events.Speak.AddListener((actor, text) => {
               ClearChoices();
               portrait.sprite = actor.Portrait;
               lines.text = text;
               actorName.text = actor.DisplayName;


               StartCoroutine(NextDialogue());
           });

           _ctrl.Events.Choice.AddListener((actor, text, choices) => {
               ClearChoices();
               portrait.sprite = actor.Portrait;
               lines.text = text;
               actorName.text = actor.DisplayName;

               choices.ForEach(c => {
                   var choice = Instantiate(choicePrefab, choiceList);
                   choice.title.text = c.Text;
                   choice.clickEvent.AddListener(_ctrl.SelectChoice);
               });
           });


           _ctrl.Events.NodeEnter.AddListener((node) => {
               Debug.Log($"Node Enter: {node.GetType()} - {node.NodeTitle}");
               nodeId = node.NodeTitle;
           });

           

           _ctrl.Play(dialogue, gameObjectOverrides.ToArray<IGameObjectOverride>());
        }

        public void AddEvent(UnityAction<string> unityAction)
        {
            _ctrl.Events.End.AddListener(() => {
                speakerContainer.SetActive(false);
                if (OpenWorld._instance)
                OpenWorld._instance.CloseCombatLog(true);

                if (unityAction!=null)
                unityAction.Invoke(nodeId);
                _ctrl.Events.End.RemoveAllListeners();
            }); 
        }

        public void ClearChoices () {
            foreach (Transform child in choiceList) {
                Destroy(child.gameObject);
            }
        }

        public void ClearEndEvents()
        {
            _ctrl.Events.End.RemoveAllListeners();
        }



        private IEnumerator NextDialogue () {
            yield return null;

            while (!Input.GetMouseButtonDown(0)) {
                yield return null;
            }

            _ctrl.Next();
        }

        private void Update () {
            // Required to run actions that may span multiple frames
            if(speakerContainer.activeSelf)
            {
                _ctrl.Tick();
            }
        }

        public void SetDialogue(DialogueGraph chosenDialogue, GameObject deactivationObject, UnityAction<string> unityAction, InteractableObject interactableObject)
        {
            
            dialogue = chosenDialogue;
            if (deactivationObject != null)
            {
                deactivationObject.SetActive(false);
            }
            // if (OpenWorld._instance)
            //    OpenWorld._instance.OpenCombatLog(interactableObject, 5);
            speakerContainer.SetActive(true);
            PlayDialogue();
            AddEvent(unityAction);
        }

        public void SetDialogueQuest(List<QuestData> questsId, UnityAction<int> questChoice)
        {
            speakerContainer.SetActive(true);

            lines.text = "So, what do you want to talk about?";
            for (int i=0; i< questsId.Count; i++)
            {             
                var choice = Instantiate(choicePrefab, choiceList);
                choice.title.text = CarListOpenWorld.Instance.GetQuest(questsId[i].QuestID).QuestDescription;
                choice.clickEvent.AddListener(questChoice);
            }
        }

       
    }
}
